﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using Game.Loader;
using Networking.Messages;
using Networking.Sockets;
using Networking.Sockets.Events;

namespace SneakGame
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            // Verify the game can run on this system
            using (Game.Game g = new Game.Game())
            {
                if (!g.Supported)
                {
                    MessageBox.Show("This game is not supported by this system.");
                    Close(); // Close window as game will not run
                }
                else
                {
                    InitializeComponent();
                    InitailizeData();
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the player is the host.
        /// </summary>
        private bool Host { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the player is the map player.
        /// </summary>
        private bool MapPlayer { get; set; }

        /// <summary>
        /// Gets or sets the username of the player.
        /// </summary>
        private string Username { get; set; }

        /// <summary>
        /// Gets or sets the other player's username.
        /// </summary>
        private string HostUsername { get; set; }

        /// <summary>
        /// Gets or sets the level name.
        /// </summary>
        private string LevelName { get; set; }

        /// <summary>
        /// Gets or sets the TCP connector socket.
        /// </summary>
        private TCP Connector { get; set; }

        /// <summary>
        /// Initializes data and variables in the main window.
        /// </summary>
        private void InitailizeData()
        {
            // Start the TCP connector
            Connector = new TCP((int)Ports.ConnectionPort);
            Connector.StopListen();
            Connector.Connected += Connected;
            Connector.MessageReceived += MessageReceived;
            Connector.Disconnected += Disconnected;

            Host = false;
            Username = string.Empty;
            LevelName = string.Empty;
            MapPlayer = false;

            // Get local ip address to display to the user
            // https://stackoverflow.com/questions/6803073/get-local-ip-address
            IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    LocalIPBox.Text = ip.ToString();
                    break;
                }
            }

            // Get public ip address to display to user
            // https://social.msdn.microsoft.com/Forums/vstudio/en-US/b117ccb3-dcde-42d5-8df9-7fbc2ac120d3/getting-public-ip-address-of-a-machine?forum=csharpgeneral
            try
            {
                string IPAddress = new WebClient().DownloadString("http://icanhazip.com/");
                PublicIPBox.Text = IPAddress;
            }
            catch { } // Catch errors to prevent a crash if the website cannot be accessed

            // Keep track of username
            UsernameBox.TextChanged += UpdateUsername;

            // Keep track of hosting
            HostGameCheck.Checked += HostChanged;
            HostGameCheck.Unchecked += HostChanged;

            // Keep track of map player
            MapCheck.Checked += MapCheckChanged;
            MapCheck.Unchecked += MapCheckChanged;

            // Add levels to level selector
            PopulateLevelSelector();
            LevelSelector.SelectionChanged += LevelSelectionChanged;

            // Handle button presses
            StartButton.Click += StartClicked;
            ConnectButton.Click += ConnectClicked;
            OptionsButtton.Click += OptionsClicked;

            // Handle the window closing
            Closed += WindowClosed;
        }

        /// <summary>
        /// Handles the closing of the window.
        /// </summary>
        /// <param name="sender">The origin of the event.</param>
        /// <param name="e">The event arguments.</param>
        private void WindowClosed(object sender, EventArgs e)
        {
            Connector.Close();
        }

        /// <summary>
        /// Handles a connection being made.
        /// </summary>
        /// <param name="sender">The origin of the event.</param>
        /// <param name="e">The event arguments.</param>
        private void Connected(object sender, ConnectedEventArgs e)
        {
            if (Host)
            {
                SetTextBoxExternally(HostIPBox, e.RemoteIP.ToString());
                Username = Username != string.Empty ? Username : " "; // Avoid sending empty string. this will cause diconnection
                LevelName = LevelName != string.Empty ? LevelName : " "; // Avoid sending empty string. this will cause diconnection
                Connector.Send(new Message((int)Codes.Username, Username));
                Connector.Send(new Message((int)Codes.LevelName, LevelName));
                Connector.Send(new Message((int)Codes.MapPlayer, new byte[] { (byte)(MapPlayer == true ? 0 : 1) }));
            }

            SetButtonEnableExternally(StartButton, true);
            SetButtonEnableExternally(ConnectButton, false);
        }

        /// <summary>
        /// Handles a message being received.
        /// </summary>
        /// <param name="sender">The origin of the event.</param>
        /// <param name="e">The event arguments.</param>
        private void MessageReceived(object sender, DataReceivedEventArgs e)
        {
            // Parse message
            if (e.Message.Code == (int)Codes.Username)
            {
                HostUsername = Encoding.UTF8.GetString(e.Message.Data);
                SetTextBoxExternally(HostUsernameBox, HostUsername);
            }
            else if (e.Message.Code == (int)Codes.LevelName)
            {
                LevelName = Encoding.UTF8.GetString(e.Message.Data);
                SetTextBoxExternally(HostLevelSelector, LevelName);
            }
            else if (e.Message.Code == (int)Codes.MapPlayer)
            {
                MapPlayer = e.Message.Data[0] == 1 ? true : false;
                SetCheckBoxExternally(MapCheck, MapPlayer);
            }
            else if (e.Message.Code == (int)Codes.LevelData)
            {
                SaveLevelData(Encoding.UTF8.GetString(e.Message.Data));

                Dispatcher.Invoke((Action)(() => StartGame()));
            }
        }

        /// <summary>
        /// Saves level data sent by the host.
        /// </summary>
        /// <param name="levelData">The level data received.</param>
        private void SaveLevelData(string levelData)
        {
            // Check if a file exists
            if (!TrySaveFile(LevelName, levelData))
            {
                // Create a number postfix to make a unique file to create
                int attemptNo = -1;
                string levelName;
                do
                {
                    attemptNo++;
                    levelName = string.Format("{0}-{1}-{2}", LevelName, HostUsername, attemptNo);
                } while (!TrySaveFile(levelName, levelData));

                // Reset the level name variable
                LevelName = levelName;
            }
        }

        /// <summary>
        /// Trys to save a file.
        /// </summary>
        /// <param name="filename">The filename to save.</param>
        /// <param name="data">The data to save in the file.</param>
        /// <returns>Whether the file was saved.</returns>
        private bool TrySaveFile(string filename, string data)
        {
            if (!File.Exists(string.Format("Resources/Levels/{0}.xml", filename)))
            {
                // File doesn't exist. Save file and return true
                SaveLevel(filename, data);
                return true;
            }
            else if (ReadLevel(LevelName) == data)
            {
                // File identical no need to resave as it exists accurately
                return true;
            }
            else
            {
                // Conflicting name do not save
                return false;
            }
        }

        /// <summary>
        /// Saves data to a file.
        /// </summary>
        /// <param name="filename">The filename to save the data to.</param>
        /// <param name="data">The data to save.</param>
        private void SaveLevel(string filename, string data)
        {
            // Save file data
            using (StreamWriter sw = new StreamWriter(string.Format("Resources/Levels/{0}.xml", filename)))
            {
                sw.Write(data);
            }
        }

        /// <summary>
        /// Reads a level file.
        /// </summary>
        /// <param name="levelName">The name of the file.</param>
        /// <returns>The data in the level file.</returns>
        private string ReadLevel(string levelName)
        {
            string data;
            using (StreamReader sr = new StreamReader(string.Format("Resources/Levels/{0}.xml", LevelName)))
            {
                data = sr.ReadToEnd();
            }

            return data;
        }

        /// <summary>
        /// Handles a disconnection.
        /// </summary>
        /// <param name="sender">The origin of the event.</param>
        /// <param name="e">The event arguments.</param>
        private void Disconnected(object sender, DisconnectedEventArgs e)
        {
            SetTextBoxExternally(HostIPBox, string.Empty);
            SetTextBoxExternally(HostUsernameBox, string.Empty);
            SetTextBoxExternally(HostLevelSelector, string.Empty);
            SetCheckBoxExternally(MapCheck, false);
            SetButtonEnableExternally(StartButton, false);
            SetButtonEnableExternally(ConnectButton, true);
        }

        /// <summary>
        /// Allows a checkbox to have it's content changed from another thread.
        /// </summary>
        /// <param name="checkbox">The checkbox to alter.</param>
        /// <param name="value">The value to put in the checkbox.</param>
        private void SetCheckBoxExternally(CheckBox checkbox, bool value)
        {
            checkbox.Dispatcher.Invoke((Action)(() => checkbox.IsChecked = value));
        }

        /// <summary>
        /// Allows a button to be enabled from another thread.
        /// </summary>
        /// <param name="button">The button to set it's value.</param>
        /// <param name="enabled">Whether the button is enabled.</param>
        private void SetButtonEnableExternally(Button button, bool enabled)
        {
            button.Dispatcher.Invoke((Action)(() => button.IsEnabled = enabled));
        }

        /// <summary>
        /// Allows a textbox to have it's content changed from another thread.
        /// </summary>
        /// <param name="textbox">The textbox to alter.</param>
        /// <param name="text">The string to put in the textbox.</param>
        /// <remarks>https://social.msdn.microsoft.com/Forums/vstudio/en-US/5b2c9d7f-b443-4723-89b3-4820c95881f4/how-to-call-the-dispatcher-of-the-gui-thread?forum=wpf</remarks>
        private void SetTextBoxExternally(TextBox textbox, string text)
        {
            textbox.Dispatcher.Invoke((Action)(() => textbox.Text = text));
        }

        /// <summary>
        /// Populates the level selector with levels.
        /// </summary>
        private void PopulateLevelSelector()
        {
            if (Directory.Exists("Resources/Levels"))
            {
                // Get list of xml files in the level folder
                IEnumerable<string> filenames = Directory.GetFiles("Resources/Levels").Where(s => s.EndsWith(".xml"));

                // Filter files that cannot be parsed
                filenames = filenames.Where(s => LevelLoader.CanLoad(s) == string.Empty);

                // Bind items
                foreach (string filename in filenames)
                {
                    // Use substring so the level name doesn't contain ".xml"
                    LevelSelector.Items.Add(filename.Substring(17, filename.Length - 21));
                }
            }
            else
            {
                Directory.CreateDirectory("Resources/Levels");
            }
        }

        /// <summary>
        /// Updates the username.
        /// </summary>
        /// <param name="sender">The origin of the event.</param>
        /// <param name="e">The event arguments.</param>
        private void UpdateUsername(object sender, RoutedEventArgs e)
        {
            Username = UsernameBox.Text;
            Username = Username != string.Empty ? Username : " "; // Avoid sending empty string. this will cause diconnection
            Connector.Send(new Message((int)Codes.Username, Username));
        }

        /// <summary>
        /// Updates the level selected.
        /// </summary>
        /// <param name="sender">The origin of the event.</param>
        /// <param name="e">The event arguments.</param>
        private void LevelSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            LevelName = LevelSelector.SelectedItem.ToString();
            LevelName = LevelName != string.Empty ? LevelName : " "; // Avoid sending empty string. this will cause diconnection
            Connector.Send(new Message((int)Codes.LevelName, LevelName));
        }

        /// <summary>
        /// Updates data when the map player has changed.
        /// </summary>
        /// <param name="sender">The origin of the event.</param>
        /// <param name="e">The event arguments.</param>
        private void MapCheckChanged(object sender, RoutedEventArgs e)
        {
            MapPlayer = (bool)MapCheck.IsChecked;
            Connector.Send(new Message((int)Codes.MapPlayer, new byte[] { (byte)(MapPlayer ? 0 : 1) }));
        }

        /// <summary>
        /// Updates data when the host has changed.
        /// </summary>
        /// <param name="sender">The origin of the event.</param>
        /// <param name="e">The event arguments.</param>
        private void HostChanged(object sender, RoutedEventArgs e)
        {
            // Update host
            Host = (bool)HostGameCheck.IsChecked;

            // Disconnect socket
            if (Connector.IsConnected)
            {
                Connector.Disconnect();
            }

            // Update form accordingly
            if (Host)
            {
                IPLabel.Content = "Client IP Address";
                HostIPBox.IsEnabled = false;
                HostIPBox.Text = string.Empty;
                HostUsernameBox.Text = string.Empty;
                StartButton.Visibility = Visibility.Visible;
                StartButton.IsEnabled = false;
                ConnectButton.Visibility = Visibility.Hidden;
                LevelSelector.Visibility = Visibility.Visible;
                HostLevelSelector.Visibility = Visibility.Hidden;
                MapCheck.IsEnabled = true;

                Connector.Listen();
            }
            else
            {
                IPLabel.Content = "Host IP Address";
                HostIPBox.IsEnabled = true;
                HostUsernameBox.Text = string.Empty;
                ConnectButton.Visibility = Visibility.Visible;
                StartButton.Visibility = Visibility.Hidden;
                LevelSelector.Visibility = Visibility.Hidden;
                HostLevelSelector.Visibility = Visibility.Visible;
                HostLevelSelector.Text = string.Empty;
                MapCheck.IsEnabled = false;
                MapCheck.IsChecked = false;

                Connector.StopListen();
            }
        }

        /// <summary>
        /// Handles the start button being clicked.
        /// </summary>
        /// <param name="sender">The origin of the event.</param>
        /// <param name="e">The event arguments.</param>
        private void StartClicked(object sender, RoutedEventArgs e)
        {
            if (LevelName != " ")
            {
                // Start game if level is selected
                SendLevelData();

                StartGame();
            }
            else
            {
                MessageBox.Show("Select a level before starting the game!");
            }
        }

        /// <summary>
        /// Sends level data to the client.
        /// </summary>
        private void SendLevelData()
        {
            // Read the level
            string level = ReadLevel(LevelName);

            // Send level in a message
            Message levelData = new Message((int)Codes.LevelData, level);
            Connector.Send(levelData);
        }

        /// <summary>
        /// Handles the connect button being clicked.
        /// </summary>
        /// <param name="sender">The origin of the event.</param>
        /// <param name="e">The event arguments.</param>
        private void ConnectClicked(object sender, RoutedEventArgs e)
        {
            // Connect to the host
            IPAddress address;
            if (IPAddress.TryParse(HostIPBox.Text, out address))
            {
                try
                {
                    Thread t = new Thread(() => Connect(address));
                    t.Start();
                }
                catch
                {
                    MessageBox.Show("Unable to connect to that host.");
                }
            }
            else
            {
                MessageBox.Show("Please input a valid host address.");
            }
        }

        /// <summary>
        /// Connects to a host.
        /// </summary>
        /// <param name="ip">The host's IP address.</param>
        private void Connect(IPAddress ip)
        {
            Connector.Connect(new IPEndPoint(ip, (int)Ports.ConnectionPort));
            if (Connector.IsConnected)
            {
                Username = Username != string.Empty ? Username : " "; // Avoid sending empty string. this will cause diconnection
                Connector.Send(new Message((int)Codes.Username, Username));
            }
        }

        /// <summary>
        /// Handles the options button being clicked.
        /// </summary>
        /// <param name="sender">The origin of the event.</param>
        /// <param name="e">The event arguments.</param>
        /// <remarks>https://stackoverflow.com/questions/16202101/how-do-i-know-if-a-wpf-window-is-opened</remarks>
        private void OptionsClicked(object sender, RoutedEventArgs e)
        {
            // Only open window if not already one currently open
            if (!Application.Current.Windows.OfType<Options>().Any())
            {
                new Options().Show();
            }
        }

        /// <summary>
        /// Starts the game.
        /// </summary>
        private void StartGame()
        {
            // Store settings so they do not get destoryed when making window
            bool host = Host;
            string username = Username;
            string hostUsername = HostUsername;
            IPAddress hostIP = Connector.RemoteIP.Address;
            string levelName = LevelName;
            bool mapPlayer = MapPlayer;

            Dispatcher.Invoke((Action)(() =>
            {
                // Create chat window
                if (host)
                {
                    new Chat(hostIP, username, hostUsername).Show();
                }
                else
                {
                    new Chat(username, hostUsername).Show();
                }

                // Create legend if map player
                if (mapPlayer)
                {
                    new Legend().Show();
                }
            }));

            // Create game window
            new Thread(() =>
            {
                Game.Game game = new Game.Game("Sneak Game", 1024, 768, string.Format("Resources/Levels/{0}.xml", levelName), "Resources/Settings/settings.xml", mapPlayer);
                if (host)
                {
                    game.Connect(hostIP);
                }
                game.Run();
            }).Start();

            // Close connector box
            Dispatcher.Invoke((Action)(() => Close()));
        }
    }
}
