﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using OpenTK.Input;
using Game.Loader;
using Game.Loader.Intermediary;

namespace SneakGame
{
    /// <summary>
    /// Interaction logic for Options.xaml
    /// </summary>
    public partial class Options : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Options"/> class.
        /// </summary>
        public Options()
        {
            InitializeComponent();
            InitailizeData();
        }

        /// <summary>
        /// Gets or sets a value indicating whether the settings have been saved.
        /// </summary>
        private bool Saved { get; set; }

        /// <summary>
        /// Gets or sets the binding input combo boxes.
        /// </summary>
        private Dictionary<Inputs, ComboBox> BindingInputs { get; set; }

        /// <summary>
        /// Initializes data and variables in the options window.
        /// </summary>
        private void InitailizeData()
        {
            // Get list of items for dropdown
            // https://stackoverflow.com/questions/972307/can-you-loop-through-all-enum-values
            List<Key> keys = new List<Key>();
            foreach (Key k in Enum.GetValues(typeof(Key)))
            {
                // Prevent the repeated keys in the key enum
                if (k != Key.Unknown && !keys.Contains(k))
                {
                    keys.Add(k);
                }
            }

            // Bind lists
            BindingInputs = new Dictionary<Inputs, ComboBox>();
            for (int i = 0; i < 6; i++)
            {
                ComboBox bindingBox = new ComboBox();
                bindingBox.VerticalAlignment = VerticalAlignment.Top;
                bindingBox.Margin = new Thickness(133, 5 + 28 * i, 5, 0);
                bindingBox.ItemsSource = keys;
                bindingBox.SelectionChanged += InputSelectionChanged;

                // Add to combobox list and layout
                BindingInputs.Add((Inputs)i, bindingBox);
                LayoutGrid.Children.Add(bindingBox);
            }

            // Bind together the sensitivity slider and the text box
            SensitivitySlider.ValueChanged += SensitivitySlider_ValueChanged;
            SensitivitySlider.Maximum = 0.05f;
            SensitivitySlider.Minimum = 0.001f;
            SensitivityText.TextChanged += SensitivityText_TextChanged;
            SensitivityText.Text = "10";

            // Set defaults
            float sens;
            Dictionary<Inputs, Key> bindings;
            OptionLoader.Load("Resources/Settings/settings.xml", out sens, out bindings);
            BindingInputs[Inputs.Forward].SelectedItem = bindings[Inputs.Forward];
            BindingInputs[Inputs.Back].SelectedItem = bindings[Inputs.Back];
            BindingInputs[Inputs.Left].SelectedItem = bindings[Inputs.Left];
            BindingInputs[Inputs.Right].SelectedItem = bindings[Inputs.Right];
            BindingInputs[Inputs.ZoomIn].SelectedItem = bindings[Inputs.ZoomIn];
            BindingInputs[Inputs.ZoomOut].SelectedItem = bindings[Inputs.ZoomOut];
            SensitivitySlider.Value = sens;
            Saved = true;

            // Handle save button
            SaveButton.Click += SaveButton_Click;

            // Add hook to prevent closing form when unsaved options
            Closing += Options_Closing;
        }

        /// <summary>
        /// Handles the options window closing.
        /// </summary>
        /// <param name="sender">The origin of the event.</param>
        /// <param name="e">The event arguments.</param>
        private void Options_Closing(object sender, CancelEventArgs e)
        {
            if (!Saved)
            {
                MessageBoxResult result = MessageBox.Show("Some settings are unsaved. Do you wish to close without saving?", "Unsaved settings", MessageBoxButton.YesNo);
                e.Cancel = (result == MessageBoxResult.No);
            }
        }

        /// <summary>
        /// Handles an input selection being changed.
        /// </summary>
        /// <param name="sender">The origin of the event.</param>
        /// <param name="e">The event arguments.</param>
        private void InputSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Saved = false;
        }

        /// <summary>
        /// Handles the save button being clicked.
        /// </summary>
        /// <param name="sender">The origin of the event.</param>
        /// <param name="e">The event arguments.</param>
        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            // Verify form is complete and no bindings are the same
            bool complete = true;
            bool repeatedSettings = false;
            List<Key> bindings = new List<Key>();
            foreach (ComboBox c in BindingInputs.Values)
            {
                if (c.SelectedItem != null)
                {
                    if (!bindings.Contains((Key)c.SelectedItem))
                    {
                        bindings.Add((Key)c.SelectedItem);
                    }
                    else
                    {
                        repeatedSettings = true;
                        break;
                    }
                }
                else
                {
                    complete = false;
                    break;
                }
            }

            if (complete)
            { 
                if (!repeatedSettings)
                {
                    // Convert inputs to data
                    OptionData o = new OptionData();
                    o.RotationSensitivity = (float)SensitivitySlider.Value;

                    o.Forward = (Key)BindingInputs[Inputs.Forward].SelectedItem;
                    o.Back = (Key)BindingInputs[Inputs.Back].SelectedItem;
                    o.Left = (Key)BindingInputs[Inputs.Left].SelectedItem;
                    o.Right = (Key)BindingInputs[Inputs.Right].SelectedItem;
                    o.ZoomIn = (Key)BindingInputs[Inputs.ZoomIn].SelectedItem;
                    o.ZoomOut = (Key)BindingInputs[Inputs.ZoomOut].SelectedItem;

                    // Save file
                    OptionLoader.Save("/Resources/Settings/settings.xml", o);
                    MessageBox.Show("Settings saved!");
                    Saved = true;
                }
                else
                {
                    MessageBox.Show("No keybindings can be repeated!");
                }
            }
            else
            {
                MessageBox.Show("Fill in options fully to save the file!");
            }
        }

        /// <summary>
        /// Handles the sensitivity text changing.
        /// </summary>
        /// <param name="sender">The origin of the event.</param>
        /// <param name="e">The event arguments.</param>
        private void SensitivityText_TextChanged(object sender, RoutedEventArgs e)
        {
            if (sender != SensitivitySlider)
            {
                SyncSensitivity(true);
                Saved = false;
            }
        }

        /// <summary>
        /// Handles the sensitivity slider's value changing.
        /// </summary>
        /// <param name="sender">The origin of the event.</param>
        /// <param name="e">The event arguments.</param>
        private void SensitivitySlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (sender != SensitivityText)
            {
                SyncSensitivity(false);
                Saved = false;
            }
        }

        /// <summary>
        /// Syncs up the sensitivity slider and text box.
        /// </summary>
        /// <param name="textChanged">Wheter the text was the value that has been changed.</param>
        private void SyncSensitivity(bool textChanged)
        {
            if (textChanged)
            {
                // Get text and parse
                string text = SensitivityText.Text;
                float fVal;
                if (float.TryParse(text, out fVal))
                {
                    double sens = Math.Max(SensitivitySlider.Minimum, Math.Min(SensitivitySlider.Maximum, (fVal / 1000)));

                    // Reset the value
                    SensitivitySlider.Value = sens;
                    SensitivityText.Text = (sens * 1000).ToString();
                }
                else
                {
                    MessageBox.Show("Please input a valid sensitivity value!");
                }
            }
            else
            {
                // Update textbox
                double val = SensitivitySlider.Value;
                SensitivityText.Text = (val * 1000).ToString();
            }
        }
    }
}
