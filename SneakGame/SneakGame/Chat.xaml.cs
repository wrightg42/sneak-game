﻿using System;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using Networking.Messages;
using Networking.Sockets;
using Networking.Sockets.Events;

namespace SneakGame
{
    /// <summary>
    /// Interaction logic for Chat.xaml
    /// </summary>
    public partial class Chat : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Chat"/> class.
        /// </summary>
        public Chat()
        {
            InitializeComponent();
            CreateCommunication();
            InitailizeData();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Chat"/> class.
        /// </summary>
        /// <param name="ip">The IP address to chat with.</param>
        public Chat(IPAddress ip)
        {
            InitializeComponent();
            CreateCommunication();
            Communication.Connect(new IPEndPoint(ip, (int)Ports.ChatPort));
            InitailizeData();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Chat"/> class.
        /// </summary>
        /// <param name="username">The username the user is using.</param>
        /// <param name="hostUsername">The username the host is using.</param>
        public Chat(string username, string hostUsername)
        {
            InitializeComponent();
            Username = username;
            HostUsername = hostUsername;
            CreateCommunication();
            InitailizeData();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Chat"/> class.
        /// </summary>
        /// <param name="ip">The IP address to chat with.</param>
        /// <param name="username">The username the user is using.</param>
        /// <param name="hostUsername">The username the host is using.</param>
        public Chat(IPAddress ip, string username, string hostUsername)
        {
            InitializeComponent();
            Username = username;
            HostUsername = hostUsername;
            CreateCommunication();
            Communication.Connect(new IPEndPoint(ip, (int)Ports.ChatPort));
            InitailizeData();
        }

        /// <summary>
        /// The TCP communication socket to send/receive messages over.
        /// </summary>
        private TCP Communication { get; set; }

        /// <summary>
        /// The username used to send messages.
        /// </summary>
        private string Username { get; set; }
        
        /// <summary>
        /// The host's username they send messages with.
        /// </summary>
        private string HostUsername { get; set; }

        /// <summary>
        /// Creaates the communication socket.
        /// </summary>
        private void CreateCommunication()
        {
            Communication = new TCP((int)Ports.ChatPort);
            Communication.Connected += Connected;
            Communication.MessageReceived += MessageReceived;
            Communication.Disconnected += Disconnected;
        }

        /// <summary>
        /// Handles a connection being made.
        /// </summary>
        /// <param name="sender">The origin of the event.</param>
        /// <param name="e">The event arguments.</param>
        private void Connected(object sender, ConnectedEventArgs e)
        {
            AddToTextBoxExternally(MessageBox, string.Format("Connected in chat with {0}.", HostUsername));
        }

        /// <summary>
        /// Handles a message being received.
        /// </summary>
        /// <param name="sender">The origin of the event.</param>
        /// <param name="e">The event arguments.</param>
        private void MessageReceived(object sender, DataReceivedEventArgs e)
        {
            if (e.Message.Code == (int)Codes.Message)
            {
                // Add message to messagebox
                AddToTextBoxExternally(MessageBox, string.Format("{0}: {1}", HostUsername, Encoding.UTF8.GetString(e.Message.Data)));
            }
        }

        /// <summary>
        /// Handles a disconnection.
        /// </summary>
        /// <param name="sender">The origin of the event.</param>
        /// <param name="e">The event arguments.</param>
        private void Disconnected(object sender, DisconnectedEventArgs e)
        {
            AddToTextBoxExternally(MessageBox, string.Format("Disconnected from chat with {0}.", HostUsername));
            Dispatcher.Invoke(new Action(Close));
        }

        /// <summary>
        /// Initializes data and variables in the main window.
        /// </summary>
        private void InitailizeData()
        {
            // Handle button presses
            SendButton.Click += SendClicked;

            // Handle the window closing
            Closed += WindowClosed;
        }

        /// <summary>
        /// Handles the closing of the window.
        /// </summary>
        /// <param name="sender">The origin of the event.</param>
        /// <param name="e">The event arguments.</param>
        private void WindowClosed(object sender, EventArgs e)
        {
            Communication.Close();
        }

        /// <summary>
        /// Handles the send button being pressed.
        /// </summary>
        /// <param name="sender">The origin of the event.</param>
        /// <param name="e">The event arguments.</param>
        private void SendClicked(object sender, RoutedEventArgs e)
        {
            // Grab text
            string message = SendBox.Text.Trim();
            SendBox.Text = string.Empty;

            if (message != string.Empty)
            {
                // Add to message box
                AddToTextBox(MessageBox, string.Format("{0}: {1}", Username, message));

                // Send message
                Communication.Send(new Message((int)Codes.Message, message));
            }
        }

        /// <summary>
        /// Allows a textbox to have it's content added to from another thread.
        /// </summary>
        /// <param name="textbox">The textbox to alter.</param>
        /// <param name="text">The string to put in the textbox.</param>
        private void AddToTextBoxExternally(TextBox textbox, string text)
        {
            textbox.Dispatcher.Invoke(new Action<TextBox, string>(AddToTextBox), new object[] { textbox, text });
        }

        /// <summary>
        /// Adds to a textbox's text.
        /// </summary>
        /// <param name="textbox">The textbox to add to it's text.</param>
        /// <param name="text">The string to add to the textbox.</param>
        private void AddToTextBox(TextBox textbox, string text)
        {
            textbox.Text += string.Format("{0}\n", text);
        }
    }
}
