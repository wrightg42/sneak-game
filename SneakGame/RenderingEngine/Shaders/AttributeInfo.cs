﻿using OpenTK.Graphics.OpenGL;

namespace RenderingEngine.Shaders
{
    /// <summary>
    /// A class representing info about an attibute in a compiled opengGL shader.
    /// </summary>
    public class AttributeInfo
    {
        /// <summary>
        /// Initialzes a new instace of the <see cref="AttributeInfo"/> class.
        /// </summary>
        public AttributeInfo()
        {
            Name = string.Empty;
            Address = -1;
            Size = 0;
        }

        /// <summary>
        /// The name of the attribute.
        /// </summary>
        public string Name;

        /// <summary>
        /// The address of the attibute for openGL.
        /// </summary>
        public int Address;

        /// <summary>
        /// The size of the attibute in openGL.
        /// </summary>
        public int Size;

        /// <summary>
        /// The type of the attibute.
        /// </summary>
        public ActiveAttribType Type;
    }
}
