﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using OpenTK.Graphics.OpenGL;

namespace RenderingEngine.Shaders
{
    /// <summary>
    /// A class representing a shader program in openGL.
    /// </summary>
    public class ShaderProgram
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ShaderProgram"/> class.
        /// </summary>
        public ShaderProgram()
        {
            ProgramID = GL.CreateProgram();
            VertexShaderID = -1;
            FragmentShaderID = -1;
            Attributes = new Dictionary<string, AttributeInfo>();
            Uniforms = new Dictionary<string, UniformInfo>();
            Buffers = new Dictionary<string, uint>();
            Framebuffers = new List<FramebufferInfo>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ShaderProgram"/> class.
        /// </summary>
        /// <param name="vertexShader">The filename/data of the vertex shader to attach.</param>
        /// <param name="fragmentShader">The filename/data of the fragment shader to attach.</param>
        /// <param name="fromFile">Wheather the other paramiters represent filenames.</param>
        public ShaderProgram(string vertexShader, string fragmentShader, bool fromFile = true)
                : this()
            {
            if (fromFile)
            {
                LoadShaderFromFile(vertexShader, ShaderType.VertexShader);
                LoadShaderFromFile(fragmentShader, ShaderType.FragmentShader);
            }
            else
            {
                LoadShaderFromString(vertexShader, ShaderType.VertexShader);
                LoadShaderFromString(fragmentShader, ShaderType.FragmentShader);
            }

            Link();
            GenerateBuffers();
        }

        /// <summary>
        /// The shader program id.
        /// </summary>
        public int ProgramID;

        /// <summary>
        /// The vertex shader id.
        /// </summary>
        public int VertexShaderID;

        /// <summary>
        /// The fragment shader id.
        /// </summary>
        public int FragmentShaderID;

        /// <summary>
        /// The attributes attached to the shader.
        /// </summary>
        public Dictionary<string, AttributeInfo> Attributes;

        /// <summary>
        /// The uniforms attached to the shader.
        /// </summary>
        public Dictionary<string, UniformInfo> Uniforms;

        /// <summary>
        /// The buffers attached to each attibute and uniform.
        /// </summary>
        public Dictionary<string, uint> Buffers;

        /// <summary>
        /// The framebuffers generated for the shader.
        /// </summary>
        public List<FramebufferInfo> Framebuffers;

        /// <summary>
        /// Links the program to openGL and generate uniforms and attibutes.
        /// </summary>
        public void Link()
        {
            GL.LinkProgram(ProgramID);
            Console.WriteLine(GL.GetProgramInfoLog(ProgramID));

            // Get amount of uniforms and attributes
            int attributeCount;
            int uniformCount;
            GL.GetProgram(ProgramID, ProgramParameter.ActiveAttributes, out attributeCount);
            GL.GetProgram(ProgramID, ProgramParameter.ActiveUniforms, out uniformCount);

            // Generate list of uniforms and attibutes
            for (int i = 0; i < attributeCount; i++)
            {
                AttributeInfo info = new AttributeInfo();
                int length;
                StringBuilder name = new StringBuilder();
                GL.GetActiveAttrib(ProgramID, i, 256, out length, out info.Size, out info.Type, name);
                info.Name = name.ToString();
                info.Address = GL.GetAttribLocation(ProgramID, info.Name);
                Attributes.Add(info.Name, info);
            }

            for (int i = 0; i < uniformCount; i++)
            {
                UniformInfo info = new UniformInfo();
                int length;
                StringBuilder name = new StringBuilder();
                GL.GetActiveUniform(ProgramID, i, 256, out length, out info.Size, out info.Type, name);
                info.Name = name.ToString();
                info.Address = GL.GetUniformLocation(ProgramID, info.Name);
                Uniforms.Add(info.Name, info);
            }
        }

        /// <summary>
        /// Generates the buffers.
        /// </summary>
        public void GenerateBuffers()
        {
            for (int i = 0; i < Attributes.Count; i++)
            {
                uint buffer;
                GL.GenBuffers(1, out buffer);
                Buffers.Add(Attributes.Values.ElementAt(i).Name, buffer);
            }

            for (int i = 0; i < Uniforms.Count; i++)
            {
                uint buffer;
                GL.GenBuffers(1, out buffer);
                Buffers.Add(Uniforms.Values.ElementAt(i).Name, buffer);
            }
        }

        /// <summary>
        /// Enables the attibute arrays.
        /// </summary>
        public void EnableVertexAttributeArrays()
        {
            for (int i = 0; i < Attributes.Count; i++)
            {
                GL.EnableVertexAttribArray(Attributes.Values.ElementAt(i).Address);
            }
        }

        /// <summary>
        /// Disables the attibute arrays.
        /// </summary>
        public void DisableVertexAttributeArrays()
        {
            for (int i = 0; i < Attributes.Count; i++)
            {
                GL.DisableVertexAttribArray(Attributes.Values.ElementAt(i).Address);
            }
        }

        /// <summary>
        /// Gets the location of an attibute.
        /// </summary>
        /// <param name="name">The attibute name.</param>
        /// <returns>The location of the attibute.</returns>
        public int GetAttribute(string name)
        {
            return Attributes.ContainsKey(name) ? Attributes[name].Address : -1;
        }

        /// <summary>
        /// Gets the location of an uniform.
        /// </summary>
        /// <param name="name">The uniform name.</param>
        /// <returns>The location of the uniform.</returns>
        public int GetUniform(string name)
        {
            return Uniforms.ContainsKey(name) ? Uniforms[name].Address : -1;
        }

        /// <summary>
        /// Gets a buffer for an attibute of uniform.
        /// </summary>
        /// <param name="name">The buffer name.</param>
        /// <returns>The buffer.</returns>
        public uint GetBuffer(string name)
        {
            return Buffers.ContainsKey(name) ? Buffers[name] : 0;
        }

        /// <summary>
        /// Loads a shader file from a string.
        /// </summary>
        /// <param name="code">The shader code.</param>
        /// <param name="type">The type of shader the code represents.</param>
        public void LoadShaderFromString(string code, ShaderType type)
        {
            if (type == ShaderType.VertexShader)
            {
                LoadShader(code, type, out VertexShaderID);
            }
            else if (type == ShaderType.FragmentShader)
            {
                LoadShader(code, type, out FragmentShaderID);
            }
        }

        /// <summary>
        /// Loads a shader from a file.
        /// </summary>
        /// <param name="filename">The filename of the shader.</param>
        /// <param name="type">The type of shader the code represents.</param>
        public void LoadShaderFromFile(string filename, ShaderType type)
        {
            using (StreamReader sr = new StreamReader(filename))
            {
                if (type == ShaderType.VertexShader)
                {
                    LoadShader(sr.ReadToEnd(), type, out VertexShaderID);
                }
                else if (type == ShaderType.FragmentShader)
                {
                    LoadShader(sr.ReadToEnd(), type, out FragmentShaderID);
                }
            }
        }

        /// <summary>
        /// Generates frame buffers for the shader program.
        /// </summary>
        /// <param name="textures">The amount of textures the frame buffer renders to.</param>
        public void GenerateFramebuffer(int textures = 1)
        {
            Framebuffers.Add(FramebufferInfo.GenerateFrameBuffer(1024, 1024, textures));
        }

        /// <summary>
        /// Loads and compiles a shader.
        /// </summary>
        /// <param name="code">The code representing the shader.</param>
        /// <param name="type">The type of shader the code represents.</param>
        /// <param name="address">The address of the compiled shader.</param>
        private void LoadShader(string code, ShaderType type, out int address)
        {
            // Compile shader and attach to shader program
            address = GL.CreateShader(type);
            GL.ShaderSource(address, code);
            GL.CompileShader(address);
            GL.AttachShader(ProgramID, address);
            Console.WriteLine(GL.GetShaderInfoLog(address));
        }
    }
}
