﻿using OpenTK.Graphics.OpenGL;

namespace RenderingEngine.Shaders
{
    /// <summary>
    /// A class representing info about an uniform in a compiled opengGL shader.
    /// </summary>
    public class UniformInfo
    {
        /// <summary>
        /// Initialzes a new instace of the <see cref="UniformInfo"/> class.
        /// </summary>
        public UniformInfo()
        {
            Name = string.Empty;
            Address = -1;
            Size = 0;
        }

        /// <summary>
        /// The name of the uniform.
        /// </summary>
        public string Name;

        /// <summary>
        /// The address of the uniform for openGL.
        /// </summary>
        public int Address;

        /// <summary>
        /// The size of the uniform in openGL.
        /// </summary>
        public int Size;

        /// <summary>
        /// The type of the uniform.
        /// </summary>
        public ActiveUniformType Type;
    }
}
