﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenTK.Graphics.OpenGL;

namespace RenderingEngine.Shaders
{
    /// <summary>
    /// A class representing a frame buffer in openGL.
    /// </summary>
    public class FramebufferInfo
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FramebufferInfo"/> class.
        /// </summary>
        public FramebufferInfo()
        {
            FramebuffferID = -1;
            TextureIDS = new List<int>();
            FramebufferWidth = 0;
            FramebufferHeight = 0;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FramebufferInfo"/> class.
        /// </summary>
        /// <param name="framebufferID">The id of the framebuffer.</param>
        /// <param name="textureIDS">The collection of texture id's for the framebuffer.</param>
        /// <param name="width">The width of the framebuffer.</param>
        /// <param name="height">The height of the framebuffer.</param>
        public FramebufferInfo(int framebufferID, IEnumerable<int> textureIDS, int width, int height)
        {
            FramebuffferID = framebufferID;
            TextureIDS = new List<int>(textureIDS);
            FramebufferWidth = width;
            FramebufferHeight = height;
        }

        /// <summary>
        /// The id of the framebuffer.
        /// </summary>
        public int FramebuffferID;

        /// <summary>
        /// The texture id's for the framebuffer to render to.
        /// </summary>
        public List<int> TextureIDS;

        /// <summary>
        /// The framebuffer's width.
        /// </summary>
        public int FramebufferWidth;

        /// <summary>
        /// The framebuffer's height.
        /// </summary>
        public int FramebufferHeight;

        /// <summary>
        /// Sets up openGL to use the framebuffer.
        /// </summary>
        public void UseBuffer()
        {
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, FramebuffferID);
            GL.PushAttrib(AttribMask.ViewportBit);
            GL.Viewport(0, 0, FramebufferWidth, FramebufferHeight);
        }

        /// <summary>
        /// Stops openGL using the framebuffer.
        /// </summary>
        public void CloseBuffer()
        {
            GL.PopAttrib();
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
            GL.DrawBuffer(DrawBufferMode.Back);
        }

        /// <summary>
        /// Generates a framebuffer.
        /// </summary>
        /// <param name="width">The width of the frame buffer to create.</param>
        /// <param name="height">The height of the frame buffer to create.</param>
        /// <param name="textures">The amount of textures the frame buffer render's to.</param>
        /// <returns>The info the generated framebuffer.</returns>
        public static FramebufferInfo GenerateFrameBuffer(int width, int height, int textures = 1)
        {
            // Generate frame buffer
            int framebufferID;
            GL.GenFramebuffers(1, out framebufferID);
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, framebufferID);

            // Generate texture to render to
            List<int> textureIDS = new List<int>();
            for (int i = 0; i < textures; i++)
            {
                textureIDS.Add(GenerateRenderTexture(width, height));
            }

            // Generate depth buffer if required
            int depthBufferID = -1;
            GL.GenRenderbuffers(1, out depthBufferID);
            GL.BindRenderbuffer(RenderbufferTarget.Renderbuffer, depthBufferID);
            GL.RenderbufferStorage(RenderbufferTarget.Renderbuffer, RenderbufferStorage.DepthComponent32, width, height);

            // Attach texture and depth buffer
            GL.FramebufferRenderbuffer(FramebufferTarget.Framebuffer, FramebufferAttachment.DepthAttachment, RenderbufferTarget.Renderbuffer, depthBufferID);
            for (int i = 0; i < textures; i++)
            {
                GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, (FramebufferAttachment)(36064 + i), TextureTarget.Texture2D, textureIDS[i], 0);
            }

            // Configure buffer
            DrawBuffersEnum[] bufs = textureIDS.Select((int texture, int index) => (DrawBuffersEnum)(36064 + index)).ToArray();
            GL.DrawBuffers(textures, bufs);

            FramebufferInfo framebuffer = new FramebufferInfo();
            if (GL.CheckFramebufferStatus(FramebufferTarget.Framebuffer) == FramebufferErrorCode.FramebufferComplete)
            {
                framebuffer = new FramebufferInfo(framebufferID, textureIDS, width, height);
            }

            return framebuffer;
        }

        /// <summary>
        /// Generate a texture to be rendered to.
        /// </summary>
        /// <param name="width">The width of the texture.</param>
        /// <param name="height">The height of the texture.</param>
        /// <returns>The texture's id.</returns>
        private static int GenerateRenderTexture(int width, int height)
        {
            int textureID;
            GL.GenTextures(1, out textureID);
            GL.BindTexture(TextureTarget.Texture2D, textureID);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, width, height, 0, PixelFormat.Rgba, PixelType.UnsignedByte, (IntPtr)0);

            // Add filtering to texture
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Nearest);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Nearest);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.Clamp);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.Clamp);

            return textureID;
        }
    }
}