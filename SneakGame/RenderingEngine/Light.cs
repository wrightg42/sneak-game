﻿using OpenTK;

namespace RenderingEngine
{
    /// <summary>
    /// A representation of a light.
    /// </summary>
    public class Light
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Light"/> class.
        /// </summary>
        public Light()
        {
            Position = new Vector3();
            Color = new Vector3();
            Brightness = 1;
            DiffuseIntensity = 1;
            AmbientIntensity = 1;
            Type = LightType.Point;
            Direction = new Vector3(0, 0, 1);
            ConeAngle = 15;
            LinearAttenuation = 0;
            QuadraticAttenuation = 0;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Light"/> class.
        /// </summary>
        /// <param name="position">The position of the light.</param>
        /// <param name="color">The color of the light.</param>
        /// <param name="brightness">The brightness of the light.</param>
        /// <param name="diffuseIntensity">The diffuse intensity of the light.</param>
        /// <param name="ambientIntensity">The ambient intensity of the light.</param>
        /// <param name="linearAttenuation">The linear attenuation of the light.</param>
        /// <param name="quadraticAttenuation">The quadratic attenuation of the light.</param>
        public Light(Vector3 position, Vector3 color, float brightness = 1, float diffuseIntensity = 1, float ambientIntensity = 1, float linearAttenuation = 0, float quadraticAttenuation = 0)
            : this()
        {
            Position = position;
            Color = color;
            Brightness = brightness;
            DiffuseIntensity = diffuseIntensity;
            AmbientIntensity = ambientIntensity;
            LinearAttenuation = linearAttenuation;
            QuadraticAttenuation = quadraticAttenuation;
        }

        /// <summary>
        /// The position of the light.
        /// </summary>
        public Vector3 Position;

        /// <summary>
        /// The color of the light.
        /// </summary>
        private Vector3 _color;

        /// <summary>
        /// Gets or sets the color of the light.
        /// </summary>
        public Vector3 Color
        {
            get
            {
                return _brightenedColor;
            }
            set
            {
                _color = value;
                _brightenedColor = _brightness * _color;
            }
        }

        /// <summary>
        /// The brightness of the light.
        /// </summary>
        private float _brightness;

        /// <summary>
        /// The combined light color and brightness.
        /// </summary>
        private Vector3 _brightenedColor;

        /// <summary>
        /// Gets or sets the brightness of the light.
        /// </summary>
        public float Brightness
        {
            get
            {
                return _brightness;
            }
            set
            {
                _brightness = value;
                _brightenedColor = _brightness * _color;
            }
        }

        /// <summary>
        /// The diffuse intensity of the light.
        /// </summary>
        public float DiffuseIntensity;

        /// <summary>
        /// The ambient intensity of the light.
        /// </summary>
        public float AmbientIntensity;

        /// <summary>
        /// The type of light.
        /// </summary>
        public LightType Type;

        /// <summary>
        /// The direction of the light.
        /// </summary>
        /// <remarks>Used for spotlights and directional lights.</remarks>
        public Vector3 Direction;

        /// <summary>
        /// The coneangle of the light.
        /// </summary>
        /// <remarks>Used for spotlights.</remarks>
        public float ConeAngle;

        /// <summary>
        /// The linear attenuation of the light.
        /// </summary>
        public float LinearAttenuation;

        /// <summary>
        /// The quadratic attenuation of the light.
        /// </summary>
        public float QuadraticAttenuation;
    }

    /// <summary>
    /// An enum representing the different light types.
    /// </summary>
    public enum LightType { Point, Spot, Directional }
}
