﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;
using RenderingEngine.Objects;
using RenderingEngine.Objects.Collisions;
using RenderingEngine.Shaders;

namespace RenderingEngine
{
    /// <summary>
    /// A class to create a game window.
    /// </summary>
    public class GameWindow : OpenTK.GameWindow
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GameWindow"/> class.
        /// </summary>
        public GameWindow() 
            : base(512, 512, new GraphicsMode(32, 24, 0, 8))
        {
            CheckGLSupport();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GameWindow"/> class.
        /// </summary>
        /// <param name="title">The title of the game window.</param>
        /// <param name="fullscreen">Wheater the game should be loaded in fullscreen mode.</param>
        public GameWindow(string title, bool fullscreen = false)
            : base(512, 512, new GraphicsMode(32, 24, 0, 8), title, fullscreen ? GameWindowFlags.Fullscreen : GameWindowFlags.Default)
        {
            CheckGLSupport();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GameWindow"/> class.
        /// </summary>
        /// <param name="title">The title of the game window.</param>
        /// <param name="width">The width of the game window.</param>
        /// <param name="height">The height of the game window.</param>
        /// <param name="fullscreen">Wheater the game should be loaded in fullscreen mode.</param>
        public GameWindow(string title, int width, int height, bool fullscreen = false)
            : base(width, height, new GraphicsMode(32, 24, 0, 8), title, fullscreen ? GameWindowFlags.Fullscreen : GameWindowFlags.Default)
        {
            CheckGLSupport();
        }

        /// <summary>
        /// The dictionary of shader programs and their names.
        /// </summary>
        protected Dictionary<string, ShaderProgram> Shaders;

        /// <summary>
        /// The current active shader program.
        /// </summary>
        protected string ActiveShader;

        /// <summary>
        /// The index buffer for rendering with openGL.
        /// </summary>
        private int iboElementsBuffer; 

        /// <summary>
        /// The collection of <see cref="Mesh"/>es to render.
        /// </summary>
        private List<Mesh> Objects;

        /// <summary>
        /// The collection of items to render during the test program.
        /// </summary>
        private List<Mesh> Items;

        /// <summary>
        /// The collection of bounding boxes to render during the test program.
        /// </summary>
        private List<Mesh> BBS;

        /// <summary>
        /// The dictionary of texture names and their addresses.
        /// </summary>
        protected Dictionary<string, int> Textures;

        /// <summary>
        /// The dictionary of materials and their names.
        /// </summary>
        protected Dictionary<string, Material> Materials;

        /// <summary>
        /// The collection of <see cref="Light"/>s used in the game.
        /// </summary>
        protected List<Light> Lights;

        /// <summary>
        /// Gets the maximum amount of lights used by the program.
        /// </summary>
        /// <remarks>This value can be overriden but must also be set in the shaders.</remarks>
        protected virtual int MAX_LIGHTS
        {
            get
            {
                return 50;
            }
        }

        /// <summary>
        /// The camera used to view the scene.
        /// </summary>
        protected Camera Camera;

        /// <summary>
        /// The screen default/clear color.
        /// </summary>
        protected Color ClearColor;

        /// <summary>
        /// Whether the mouse should be centered.
        /// </summary>
        protected bool CenterMouse;

        /// <summary>
        /// The last mouse position.
        /// </summary>
        protected Vector2 LastMousePosition;

        /// <summary>
        /// The current state of the keyboard.
        /// </summary>
        protected KeyboardState KeyboardState;

        /// <summary>
        /// Whether the FPS should be outputted to the console.
        /// </summary>
        protected bool DispalyFPS;

        /// <summary>
        /// The time the application was started.
        /// </summary>
        protected DateTime BeginTime;

        /// <summary>
        /// The amount of time the window has been open.
        /// </summary>
        protected float Time;

        /// <summary>
        /// Time passed since the last FPS count.
        /// </summary>
        private float TimeInSec;

        /// <summary>
        /// Frames passed since the last FPS count.
        /// </summary>
        private int FramesInSec;

        /// <summary>
        /// The milliseconds per frame count.
        /// </summary>
        private double MSPF;

        /// <summary>
        /// The frames per second count.
        /// </summary>
        private double FPS;

        /// <summary>
        /// Whether the game is supported by the current hardware.
        /// </summary>
        public bool Supported;

        /// <summary>
        /// Event handler for when the game window is loaded.
        /// </summary>
        /// <param name="e">The event arguemnts</param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            InitProgram();
            GL.ClearColor(ClearColor);
            GL.PointSize(5f);
        }

        /// <summary>
        /// Event handler for when the game window renders a frame.
        /// </summary>
        /// <param name="e">The event arguments.</param>
        protected override void OnRenderFrame(FrameEventArgs e)
        {
            base.OnRenderFrame(e);
            RenderToRenderBuffers();
            RenderToScreen();
            FramesInSec++;
        }

        /// <summary>
        /// Event handler for when the game window is updated.
        /// </summary>
        /// <param name="e">The event arguments.</param>
        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            base.OnUpdateFrame(e);

            // Increment time passed and calculate FPS
            DateTime now = DateTime.Now;
            TimeSpan diff = (now - BeginTime);
            Time = (float)diff.TotalSeconds;
            TimeInSec += (float)e.Time;
            if (TimeInSec >= 1)
            {
                FPS = FramesInSec / TimeInSec;
                MSPF = 1000 / FPS;
                if (DispalyFPS)
                {
                    Console.WriteLine("ms/frame: {0}\t\tframes/s: {1}", MSPF, FPS);
                }
                FramesInSec = 0;
                TimeInSec -= 1;
            }

            // Process inputs
            if (Focused)
            {
                Vector2 movement = LastMousePosition - new Vector2(OpenTK.Input.Mouse.GetState().X, OpenTK.Input.Mouse.GetState().Y);
                if (movement != Vector2.Zero)
                {
                    TriggerProcessMouseMove(movement);
                }

                KeyboardState = OpenTK.Input.Keyboard.GetState();
                TriggerProcessKeyboard();
            }

            UpdateGame();
            SendRenderDataToGPU();
        }

        /// <summary>
        /// The event handler for when focus is on the game window.
        /// </summary>
        /// <param name="e">The event arguments.</param>
        protected override void OnFocusedChanged(EventArgs e)
        {
            base.OnFocusedChanged(e);

            // Ceneter mouse when window is in focus
            if (Focused)
            {
                ResetCursor();
            }
        }

        /// <summary>
        /// Initializes all program variables.
        /// </summary>
        protected virtual void InitializeVariables()
        {
            Camera = new Camera(Matrix4.CreatePerspectiveFieldOfView(1.3f, ClientSize.Width / ClientSize.Height, 0.001f, 40), new Vector3(0, 0, 3), new Vector3());
            //Camera = new Camera(Matrix4.CreateOrthographic(40, 40, -40, 40), new Vector3(0, 10, 3), new Vector3(0, -(float)(Math.PI / 2) + 0.001f, 0));
        }

        /// <summary>
        /// Initializes the openGL data and shaders.
        /// </summary>
        protected virtual void InitializeGL()
        {
            // Initilize shaders
            Shaders.Add("default", new ShaderProgram("Resources\\ShaderFiles\\default.vert", "Resources\\ShaderFiles\\default.frag"));
            Shaders.Add("textured", new ShaderProgram("Resources\\ShaderFiles\\textured.vert", "Resources\\ShaderFiles\\textured.frag"));
            Shaders.Add("normal", new ShaderProgram("Resources\\ShaderFiles\\normal.vert", "Resources\\ShaderFiles\\normal.frag"));
            Shaders.Add("lit", new ShaderProgram("Resources\\ShaderFiles\\lit.vert", "Resources\\ShaderFiles\\lit.frag"));
            Shaders.Add("lit_multiple", new ShaderProgram("Resources\\ShaderFiles\\lit.vert", "Resources\\ShaderFiles\\lit_multiple.frag"));
            Shaders.Add("lit_advanced", new ShaderProgram("Resources\\ShaderFiles\\lit.vert", "Resources\\ShaderFiles\\lit_advanced.frag"));
            Shaders.Add("normal_mapped", new ShaderProgram("Resources\\ShaderFiles\\lit.vert", "Resources\\ShaderFiles\\normal_mapped.frag"));
            ActiveShader = "normal_mapped";
        }

        /// <summary>
        /// Initializes resources for the game.
        /// </summary>
        /// <remarks>When overriding DO NOT run base method - this is full of objects and data for testing purposes.</remarks>
        protected virtual void InitResoruces()
        {
        }

        /// <summary>
        /// Sets up the scene for the game.
        /// </summary>
        /// <remarks>When overriding DO NOT run base method - this is full of objects and data for testing purposes.</remarks>
        protected virtual void SetupScene()
        {
            /*/ Object loading tests
            Directory.SetCurrentDirectory("Resources\\Assets\\");
            Mesh obj = ObjectLoader.LoadMeshFromFile("teapot.obj", ref Materials);
            Mesh obj2 = ObjectLoader.LoadMeshFromFile("cow.obj", ref Materials);
            List<Mesh> objMulti = ObjectLoader.LoadMeshesFromFile("multipleobjects.obj", ref Materials);
            LoadMaterialTextures();
            Directory.SetCurrentDirectory("..\\..\\");

            // Move to prevent overlap of objects
            obj2.Position += new Vector3(0, 0, 5);
            objMulti[0].Position += new Vector3(0, 5, 0);
            objMulti[1].Position -= new Vector3(0, 5, 5);

            Objects = new List<Mesh>() { obj, obj2, objMulti[0], objMulti[1] };*/

            /*/ Binding Tests
            Directory.SetCurrentDirectory("Resources\\Assets\\");
            Mesh obj = ObjectLoader.LoadMeshFromFile("teapot.obj", ref Materials);
            Mesh obj2 = ObjectLoader.LoadMeshFromFile("teapot.obj", ref Materials); // Reload seperate mesh
            Mesh obj3 = ObjectLoader.LoadMeshFromFile("teapot.obj", ref Materials); // Reload seperate mesh
            LoadMaterialTextures();
            Directory.SetCurrentDirectory("..\\..\\");
            Items = new List<Mesh>() { obj, obj2 };
            BindObjects(Items);

            Items[1].Position += new Vector3(0, 0, 5); // Test 1
            Items[1].Scale = new Vector3(); // Test 2
            Items.RemoveAt(1); 
            obj3.Position += new Vector3(0, 5, 0);
            Items.Add(obj3); // Test 3*/

            // Create objects
            Directory.SetCurrentDirectory("Resources\\Assets\\");
            Mesh obj = ObjectLoader.LoadMeshFromFile("cow.obj", ref Materials);
            Mesh obj2 = ObjectLoader.LoadMeshFromFile("teapot.obj", ref Materials);
            Mesh obj3 = ObjectLoader.LoadMeshFromFile("earth.obj", ref Materials);
            Mesh obj4 = ObjectLoader.LoadMeshFromFile("brickwall.obj", ref Materials);
            LoadMaterialTextures();
            Directory.SetCurrentDirectory("..\\..\\");
            obj.Position += new Vector3(7, 2, -5);
            obj2.Position += new Vector3(5, 2, 0);
            obj3.Position += new Vector3(1f, 1f, -2f);
            obj4.Position += new Vector3(-5, 4, 3);
            obj4.Rotation += new Vector3(0, 1, 0);

            Mesh c = new Cube();
            Mesh c2 = new Cube();
            c2.Position += new Vector3(-4, -6, 2);

            Mesh tc = new TexturedCube();
            tc.Position += new Vector3(-15, -2, -2);
            tc.Material = Materials["opentk1"];
            Mesh tc2 = new TexturedCube();
            tc2.Material = Materials["opentk2"];
            tc2.Position += new Vector3(-10, 1, 1);

            TexturedCube floor = new TexturedCube();
            floor.Material = Materials["opentk1"];
            floor.Scale = new Vector3(20, 0.1f, 20);
            floor.Position = new Vector3(0, -2, 0);
            TexturedCube backwall = new TexturedCube();
            backwall.Scale = new Vector3(20, 20, 0.1f);
            backwall.Position += new Vector3(0, 8, -10);
            backwall.Material = Materials["opentk1"];

            Items = new List<Mesh>() { tc, tc2, obj4 };
            //Items = new List<Mesh>() { tc, tc2, obj4, obj, obj2, obj3 };
            //Camera.BeginFollowing(tc, new Vector3(0, 0.1f, 0));
            BBS = new List<Mesh>();
            for (int i = 0; i < 3; i++)
            {
                Items[i].GenerateBoundingBox();
                BoundingBox b = Items[i].OrientedBoundingBox;
                Mesh m = b.ToMesh();
                m.CalculateNormals();
                BBS.Add(m);
            }
            List<Mesh> Floors = new List<Mesh>() { floor, backwall };

            /*/ Static Collision Tests
            tc = new TexturedCube();
            tc.Position += new Vector3(0, 5, 0);
            tc2 = new TexturedCube();
            tc2.Position += new Vector3(0, 7, 0);
            Mesh tc3 = new TexturedCube();
            tc3.Position += new Vector3(0, 5.5f, -1.5f);
            Directory.SetCurrentDirectory("Resources\\Assets\\");
            obj = ObjectLoader.LoadMeshFromFile("brickwall.obj", ref Materials);
            obj2 = ObjectLoader.LoadMeshFromFile("earth.obj", ref Materials);
            obj2.Position += new Vector3(1, 5.7f, 1.7f);

            Items = new List<Mesh>() { tc, tc2, tc3, obj, obj2 };

            tc.GenerateBoundingBox();
            tc2.GenerateBoundingBox();
            tc3.GenerateBoundingBox();
            obj.GenerateBoundingBox();
            obj2.GenerateBoundingBox();

            if (tc.OrientedBoundingBox.Collision(obj.OrientedBoundingBox))
            {
                Console.WriteLine("Box 1 collides with wall");
            }
            if (tc2.OrientedBoundingBox.Collision(obj.OrientedBoundingBox))
            {
                Console.WriteLine("Box 2 collides with wall");
            }
            if (tc3.OrientedBoundingBox.Collision(obj.OrientedBoundingBox))
            {
                Console.WriteLine("Box 3 collides with wall");
            }
            if (obj2.OrientedBoundingBox.Collision(obj.OrientedBoundingBox))
            {
                Console.WriteLine("Earth collides with wall");
            }*/

            // Add objects to render list
            BindObjects(Items);
            //BindObjects(Items, BBS, Floors);*/

            // Create lights
            Light sun = new Light(new Vector3(-5, 10, 10), new Vector3(0.7f, 0.7f, 0.7f));
            sun.Type = LightType.Directional;
            //sun.Direction = new Vector3(0, 1, 0);
            sun.Direction = (sun.Position - floor.Position).Normalized();
            Light spotLight = new Light(new Vector3(2, 7, 0), new Vector3(0.7f, 0.2f, 0.2f));
            spotLight.Type = LightType.Spot;
            spotLight.Direction = (spotLight.Position - obj3.Position).Normalized();
            spotLight.ConeAngle =  (float)(5 * Math.PI / 36);
            Light spotLight2 = new Light(new Vector3(2, 0, 3), new Vector3(0.2f, 0.6f, 0.25f));
            spotLight2.Type = LightType.Spot;
            spotLight2.Direction = (spotLight2.Position - obj3.Position).Normalized();
            spotLight2.ConeAngle = (float)(Math.PI / 6);
            Light spotLight3 = new Light(new Vector3(6, 4, 0), new Vector3(0.2f, 0.25f, 0.6f));
            spotLight3.Type = LightType.Spot;
            spotLight3.Direction = (spotLight3.Position - obj3.Position).Normalized();
            spotLight3.ConeAngle = (float)(Math.PI / 4);
            Light spotLight4 = new Light(new Vector3(10, 5, 0), new Vector3(0.7f, 0.7f, 0.7f));
            spotLight4.Type = LightType.Spot;
            spotLight4.Direction = (spotLight4.Position - obj2.Position).Normalized();
            spotLight4.ConeAngle = (float)(Math.PI / 9);
            Light pointLight = new Light(new Vector3(2, 7, 0), new Vector3(1.5f, 0.2f, 0.2f), quadraticAttenuation: 0.05f);
            Light pointLight2 = new Light(new Vector3(2, 0, 3), new Vector3(0.2f, 1f, 0.25f), quadraticAttenuation: 0.05f);
            Light pointLight3 = new Light(new Vector3(6, 4, 0), new Vector3(0.2f, 0.25f, 1.5f), quadraticAttenuation: 0.05f);

            // Add lights
            Lights.Add(sun);
            Lights.Add(pointLight);
            Lights.Add(pointLight2);
            Lights.Add(pointLight3);
            Lights.Add(spotLight);
            Lights.Add(spotLight2);
            Lights.Add(spotLight3);
            Lights.Add(spotLight4);
        }

        /// <summary>
        /// Finishes up the setup for the game.
        /// </summary>
        protected virtual void Finalizer()
        {
        }

        /// <summary>
        /// Updates the game window.
        /// </summary>
        /// <remarks>When overriding DO NOT run base method - this is full of objects and data for testing purposes.</remarks>
        protected virtual void UpdateGame()
        {
            // Move objects due to time
            Objects[0].Position = new Vector3(-15 + Time, -0.5f + (float)Math.Sin(Time), -0.0f);
            Objects[0].Rotation = new Vector3(0.55f * Time, 0.25f * Time, 0);
            Objects[0].Scale = new Vector3(0.5f, 0.5f, 0.5f);
            Objects[1].Position = new Vector3(-10f + Time, 0.5f + (float)Math.Cos(Time), 1.0f);
            Objects[1].Rotation = new Vector3(-0.25f * Time, -0.35f * Time, 0);
            Objects[1].Scale = new Vector3(0.5f, 0.5f, 0.5f);

            if (Objects[2].OrientedBoundingBox.Collision(Objects[0].OrientedBoundingBox))
            {
                Console.WriteLine("1 collided with wall!");
                Contact c = new Contact(Objects[0], Objects[2].OrientedBoundingBox);
                Console.WriteLine("Normal: {0}, Depth: {1}", c.Normal.ToString(), c.Depth.ToString());
            }
            if (Objects[2].OrientedBoundingBox.Collision(Objects[1].OrientedBoundingBox))
            {
                Console.WriteLine("2 collided with wall!");
                Contact c = new Contact(Objects[1], Objects[2].OrientedBoundingBox);
                Console.WriteLine("Normal: {0}, Depth: {1}", c.Normal.ToString(), c.Depth.ToString());
            }
        }

        /// <summary>
        /// Processes mouse movements.
        /// </summary>
        /// <param name="movement">A <see cref="Vector2"/> representing the mouse movement.</param>
        protected virtual void ProcessMouseMove(Vector2 movement)
        {
            Camera.Rotate(movement.X, movement.Y);
        }

        /// <summary>
        /// Processes the current keyboard state.
        /// </summary>
        /// <remarks>The method would have been called after the keyboard state has been recieved - use this variable to process the current held keys.</remarks>
        protected virtual void ProcessKeyboard()
        { 
            if (KeyboardState[Key.W])
            {
                Camera.Move(0f, 0f, 0.1f);
            }
            if (KeyboardState[Key.A])
            {
                Camera.Move(-0.1f, 0f, 0f);
            }
            if (KeyboardState[Key.S])
            {
                Camera.Move(0f, 0f, -0.1f);
            }
            if (KeyboardState[Key.D])
            {
                Camera.Move(0.1f, 0f, 0f);
            }
            if (KeyboardState[Key.Left])
            {
                Camera.Orientation.Z -= 0.05f;
            }
            if (KeyboardState[Key.Right])
            {
                Camera.Orientation.Z += 0.05f;
            }
            if (KeyboardState[Key.Q])
            {
                Camera.Move(0f, 0.1f, 0f);
            }
            if (KeyboardState[Key.E])
            {
                Camera.Move(0f, -0.1f, 0f);
            }
        }

        /// <summary>
        /// Loads materials from a file.
        /// </summary>
        /// <param name="filename">The filename to load the materials from.</param>
        public void LoadMaterials(string filename)
        {
            // Load materials and populate dictionary
            foreach (KeyValuePair<string, Material> material in Material.LoadFromFile(filename))
            {
                if (!Materials.ContainsKey(material.Key))
                {
                    Materials.Add(material.Key, material.Value);
                }
            }

            LoadMaterialTextures();
        }

        /// <summary>
        /// Loads textures used by materials.
        /// </summary>
        public void LoadMaterialTextures()
        {
            foreach (Material material in Materials.Values)
            {
                if (File.Exists(material.AmbientMap) && !Textures.ContainsKey(material.AmbientMap))
                {
                    Textures.Add(material.AmbientMap, LoadImage(material.AmbientMap));
                }

                if (File.Exists(material.DiffuseMap) && !Textures.ContainsKey(material.DiffuseMap))
                {
                    Textures.Add(material.DiffuseMap, LoadImage(material.DiffuseMap));
                }

                if (File.Exists(material.SpecularMap) && !Textures.ContainsKey(material.SpecularMap))
                {
                    Textures.Add(material.SpecularMap, LoadImage(material.SpecularMap));
                }

                if (File.Exists(material.NormalMap) && !Textures.ContainsKey(material.NormalMap))
                {
                    Textures.Add(material.NormalMap, LoadImage(material.NormalMap));
                }
            }
        }

        /// <summary>
        /// Loads an image from a file.
        /// </summary>
        /// <param name="filename">The filename of the image to load.</param>
        /// <returns>The address of the image texture.</returns>
        public int LoadImage(string filename)
        {
            try
            {
                Bitmap file = new Bitmap(filename);
                file.RotateFlip(RotateFlipType.RotateNoneFlipY); // Flip so it fits openGL rendering of textures where y is inverted
                return LoadImage(file);
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("File not found: {0}", filename);
            }
            catch (DirectoryNotFoundException)
            {
                Console.WriteLine("File not found: {0}", filename);
            }
            catch (Exception)
            {
                Console.WriteLine("Error loading image: {0}", filename);
            }

            return -1;
        }

        /// <summary>
        /// Loads an image from <see cref="Bitmap"/> data.
        /// </summary>
        /// <param name="image">The image data.</param>
        /// <returns>The address of the image texture.</returns>
        public int LoadImage(Bitmap image)
        {
            // Bind image and send data to GPU
            int textureID = GL.GenTexture();
            GL.BindTexture(TextureTarget.Texture2D, textureID);
            BitmapData bitData = image.LockBits(new Rectangle(0, 0, image.Width, image.Height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, bitData.Width, bitData.Height, 0, OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, bitData.Scan0);
            image.UnlockBits(bitData);

            // Genereate a mipmap of image
            GL.GenerateMipmap(GenerateMipmapTarget.Texture2D);

            return textureID;
        }

        /// <summary>
        /// Binds a <see cref="List{T}"/> of <see cref="Mesh"/>es to be rendered.
        /// </summary>
        /// <param name="meshLists">The collections to be added to the render list.</param>
        protected void BindObjects(params IEnumerable<Mesh>[] meshLists)
        {
            // Add meshes
            Objects = new List<Mesh>();
            for (int i = 0; i < meshLists.Length; i++)
            {
                Objects = Objects.Concat(meshLists[i]).ToList();
            }
        }

        /// <summary>
        /// Checks if the program is supported by the current hardware.
        /// </summary>
        /// <remarks>The result is stored in the variable <see cref="Supported"/>.</remarks>
        private void CheckGLSupport()
        {
            if (int.Parse(GL.GetString(StringName.Version).Split('.')[0]) < 3)
            {
                Supported = false;
            }
            else
            {
                Supported = true;
            }
        }

        /// <summary>
        /// Initializes the program.
        /// </summary>
        private void InitProgram()
        {
            // Get start time
            BeginTime = DateTime.Now;

            // Output current system information on GPU
            Console.WriteLine("Graphics vendor: {0}", GL.GetString(StringName.Vendor));
            Console.WriteLine("Graphics card: {0}", GL.GetString(StringName.Renderer));
            Console.WriteLine("OpenGL version: {0}", GL.GetString(StringName.Version));
            Console.WriteLine("GLSL version: {0}", GL.GetString(StringName.ShadingLanguageVersion));

            // Initalize core data
            Shaders = new Dictionary<string, ShaderProgram>();
            ActiveShader = string.Empty;
            GL.GenBuffers(1, out iboElementsBuffer);
            Objects = new List<Mesh>();
            Textures = new Dictionary<string, int>();
            Materials = new Dictionary<string, Material>();
            Lights = new List<Light>();
            ClearColor = Color.CornflowerBlue;
            CenterMouse = true;
            LastMousePosition = new Vector2();
            DispalyFPS = true;
            Time = 0;
            TimeInSec = 0;
            FramesInSec = 0;
            MSPF = 0;
            FPS = 0;

            // Initialize game
            InitializeVariables();
            InitializeGL();
            InitResoruces();
            SetupScene();
            Finalizer();
        }

        /// <summary>
        /// Update mouse positions then fire the <see cref="ProcessMouseMove(Vector2)"/> method.
        /// </summary>
        /// <param name="movement">A <see cref="Vector2"/> representing the mouse movement.</param>
        private void TriggerProcessMouseMove(Vector2 movement)
        {
            if (CenterMouse)
            {
                ResetCursor();
            }
            else
            {
                LastMousePosition += movement;
            }

            ProcessMouseMove(movement);
        }

        /// <summary>
        /// Fire the <see cref="ProcessKeyboard()"/> method and process inputs to close the window.
        /// </summary>
        private void TriggerProcessKeyboard()
        {
            ProcessKeyboard();

            if ((KeyboardState[Key.AltLeft] || KeyboardState[Key.AltRight]) && KeyboardState[Key.F4])
            {
                Close();
            }
        }

        /// <summary>
        /// Resets the cursor position.
        /// </summary>
        private void ResetCursor()
        {
            OpenTK.Input.Mouse.SetPosition(Bounds.Left + Bounds.Width / 2, Bounds.Top + Bounds.Height / 2);
            LastMousePosition = new Vector2(OpenTK.Input.Mouse.GetState().X, OpenTK.Input.Mouse.GetState().Y);
        }

        /// <summary>
        /// Sets up openGL ready to render.
        /// </summary>
        private void SetupGLRender()
        {
            GL.Viewport(0, 0, Width, Height);
            GL.Enable(EnableCap.DepthTest);
            GL.Enable(EnableCap.CullFace); // Shouldn't be used with transparent objects
            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);

            // Enable drawing buffers
            GL.UseProgram(Shaders[ActiveShader].ProgramID);
            Shaders[ActiveShader].EnableVertexAttributeArrays();
        }

        /// <summary>
        /// Cleans up openGL data after rendering.
        /// </summary>
        private void CleanGLRender()
        {
            Shaders[ActiveShader].DisableVertexAttributeArrays();
            GL.Flush();
        }

        /// <summary>
        /// Renders data to the window.
        /// </summary>
        private void RenderToScreen()
        {
            SetupGLRender();
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);

            // Clear background and depth buffer
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            int indiceAt = 0;
            foreach (Mesh o in Objects)
            {
                RenderMesh(indiceAt, o);
                indiceAt += o.IndicieCount;
            }

            CleanGLRender();
            SwapBuffers();
        }

        /// <summary>
        /// Renders data to the shader's frame buffers.
        /// </summary>
        private void RenderToRenderBuffers()
        {
            if (Shaders[ActiveShader].Framebuffers.Count > 0)
            {
                SetupGLRender();

                for (int i = 0; i < Shaders[ActiveShader].Framebuffers.Count; i++)
                {
                    Shaders[ActiveShader].Framebuffers[i].UseBuffer();

                    // Clear background and depth buffer
                    GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

                    int indiceAt = 0;
                    foreach (Mesh o in Objects)
                    {
                        RenderMesh(indiceAt, o);
                        indiceAt += o.IndicieCount;
                    }

                    Shaders[ActiveShader].Framebuffers[i].CloseBuffer();
                }

                CleanGLRender();
            }
        }

        /// <summary>
        /// Renders a <see cref="Mesh"/>.
        /// </summary>
        /// <param name="indiceAt">The last index sent to the GPU.</param>
        /// <param name="o">The <see cref="Mesh"/> to render.</param>
        private void RenderMesh(int indiceAt, Mesh o)
        {
            // Recaluclate matricies for the model
            o.CalculateModelViewProjectionMatrix(Camera.GetProjectionMatrix());

            // Apply texture if supported
            GL.ActiveTexture(TextureUnit.Texture0);
            int textureID = o.TextureID;
            if (textureID == -1 && (o.Material.DiffuseMap != string.Empty || o.Material.AmbientMap != string.Empty))
            {
                textureID = o.Material.DiffuseMap != string.Empty ? Textures[o.Material.DiffuseMap] : Textures[o.Material.AmbientMap];
            }
            GL.BindTexture(TextureTarget.Texture2D, textureID);
            if (Shaders[ActiveShader].GetAttribute("maintexture") != -1)
            {
                GL.Uniform1(Shaders[ActiveShader].GetAttribute("maintexture"), o.Material.DiffuseMap != string.Empty ? Textures[o.Material.DiffuseMap] : o.TextureID);
            }

            // Send view and model matrix if needed
            if (Shaders[ActiveShader].GetUniform("modelview") != -1)
            {
                GL.UniformMatrix4(Shaders[ActiveShader].GetUniform("modelview"), false, ref o.ModelViewProjectionMatrix);
            }

            if (Shaders[ActiveShader].GetUniform("model") != -1)
            {
                GL.UniformMatrix4(Shaders[ActiveShader].GetUniform("model"), false, ref o.ModelMatrix);
            }

            if (Shaders[ActiveShader].GetUniform("normalmatrix") != -1)
            {
                Matrix3 normalMatrix = new Matrix3(o.ModelMatrix.Inverted());
                normalMatrix.Transpose();
                GL.UniformMatrix3(Shaders[ActiveShader].GetUniform("normalmatrix"), false, ref normalMatrix);
            }

            // Apply material data if supported
            if (Shaders[ActiveShader].GetUniform("material_ambient") != -1)
            {
                GL.Uniform3(Shaders[ActiveShader].GetUniform("material_ambient"), ref o.Material.AmbientColor);
            }
            
            if (Shaders[ActiveShader].GetUniform("material_ambientMap") != -1)
            {
                // Check object has ambient map
                if (o.Material.AmbientMap != string.Empty)
                {
                    GL.ActiveTexture((TextureUnit)((int)TextureUnit.Texture0 + Textures[o.Material.AmbientMap]));
                    GL.BindTexture(TextureTarget.Texture2D, Textures[o.Material.AmbientMap]);
                    GL.Uniform1(Shaders[ActiveShader].GetUniform("material_ambientMap"), Textures[o.Material.AmbientMap]);
                    GL.Uniform1(Shaders[ActiveShader].GetUniform("material_hasAmbientMap"), 1);
                }
                else
                {
                    GL.Uniform1(Shaders[ActiveShader].GetUniform("material_hasAmbientMap"), 0);
                }
            }

            if (Shaders[ActiveShader].GetUniform("material_diffuse") != -1)
            {
                GL.Uniform3(Shaders[ActiveShader].GetUniform("material_diffuse"), ref o.Material.DiffuseColor);
            }

            if (Shaders[ActiveShader].GetUniform("material_diffuseMap") != -1)
            {
                // Check object has diffuse map
                if (o.Material.DiffuseMap != string.Empty)
                {
                    GL.ActiveTexture((TextureUnit)((int)TextureUnit.Texture0 + Textures[o.Material.DiffuseMap]));
                    GL.BindTexture(TextureTarget.Texture2D, Textures[o.Material.DiffuseMap]);
                    GL.Uniform1(Shaders[ActiveShader].GetUniform("material_diffuseMap"), Textures[o.Material.DiffuseMap]);
                    GL.Uniform1(Shaders[ActiveShader].GetUniform("material_hasDiffuseMap"), 1);
                }
                else
                {
                    GL.Uniform1(Shaders[ActiveShader].GetUniform("material_hasDiffuseMap"), 0);
                }
            }

            if (Shaders[ActiveShader].GetUniform("material_specular") != -1)
            {
                GL.Uniform3(Shaders[ActiveShader].GetUniform("material_specular"), ref o.Material.SpecularColor);
            }

            if (Shaders[ActiveShader].GetUniform("material_specularMap") != -1)
            {
                // Check object has specular map
                if (o.Material.SpecularMap != string.Empty)
                {
                    GL.ActiveTexture((TextureUnit)((int)TextureUnit.Texture0 + Textures[o.Material.SpecularMap]));
                    GL.BindTexture(TextureTarget.Texture2D, Textures[o.Material.SpecularMap]);
                    GL.Uniform1(Shaders[ActiveShader].GetUniform("material_specularMap"), Textures[o.Material.SpecularMap]);
                    GL.Uniform1(Shaders[ActiveShader].GetUniform("material_hasSpecularMap"), 1);
                }
                else
                {
                    GL.Uniform1(Shaders[ActiveShader].GetUniform("material_hasSpecularMap"), 0);
                }
            }

            if (Shaders[ActiveShader].GetUniform("material_specExponent") != -1)
            {
                GL.Uniform1(Shaders[ActiveShader].GetUniform("material_specExponent"), o.Material.SpecularExponent);
            }
            if (Shaders[ActiveShader].GetUniform("material_opacity") != -1)
            {
                GL.Uniform1(Shaders[ActiveShader].GetUniform("material_opacity"), o.Material.Opacity);
            }

            // Apply normal map if supported
            if (Shaders[ActiveShader].GetUniform("material_normalMap") != -1)
            {
                // Check object has normal map
                if (o.Material.NormalMap != string.Empty)
                {
                    GL.ActiveTexture((TextureUnit)((int)TextureUnit.Texture0 + Textures[o.Material.NormalMap]));
                    GL.BindTexture(TextureTarget.Texture2D, Textures[o.Material.NormalMap]);
                    GL.Uniform1(Shaders[ActiveShader].GetUniform("material_normalMap"), Textures[o.Material.NormalMap]);
                    GL.Uniform1(Shaders[ActiveShader].GetUniform("material_hasNormalMap"), 1);
                }
                else
                {
                    GL.Uniform1(Shaders[ActiveShader].GetUniform("material_hasNormalMap"), 0);
                }
            }

            foreach (Face f in o.Faces)
            {
                GL.DrawElements(f.DrawType, f.Verticies.Count, DrawElementsType.UnsignedInt, indiceAt * sizeof(uint));
                indiceAt += f.Verticies.Count;
            }
        }

        /// <summary>
        /// Sends data to the GPU ready to render.
        /// </summary>
        private void SendRenderDataToGPU()
        {
            // Get all data about objects into lists
            List<Vector3> verts = new List<Vector3>();
            List<int> inds = new List<int>();
            List<Vector3> colors = new List<Vector3>();
            List<Vector2> textureCoords = new List<Vector2>();
            List<Vector3> normals = new List<Vector3>();
            int vertexCount = 0;
            foreach (Mesh o in Objects)
            {
                verts.AddRange(o.Verticies);
                inds.AddRange(o.GetIndices(vertexCount));
                colors.AddRange(o.Colors);
                textureCoords.AddRange(o.TextureCoords);
                normals.AddRange(o.Normals);
                vertexCount += o.Verticies.Length;
            }

            Vector3[] vertexData = verts.ToArray();
            int[] indiceData = inds.ToArray();
            Vector3[] colourData = colors.ToArray();
            Vector2[] textureCoordsData = textureCoords.ToArray();
            Vector3[] normalData = normals.ToArray();

            // Send position data to GPU
            GL.BindBuffer(BufferTarget.ArrayBuffer, Shaders[ActiveShader].GetBuffer("vPosition"));
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(vertexData.Length * Vector3.SizeInBytes), vertexData, BufferUsageHint.StaticDraw);
            GL.VertexAttribPointer(Shaders[ActiveShader].GetAttribute("vPosition"), 3, VertexAttribPointerType.Float, false, 0, 0);

            // Send color data to GPU if the shader supports it
            if (Shaders[ActiveShader].GetAttribute("vColor") != -1)
            {
                GL.BindBuffer(BufferTarget.ArrayBuffer, Shaders[ActiveShader].GetBuffer("vColor"));
                GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(colourData.Length * Vector3.SizeInBytes), colourData, BufferUsageHint.StaticDraw);
                GL.VertexAttribPointer(Shaders[ActiveShader].GetAttribute("vColor"), 3, VertexAttribPointerType.Float, true, 0, 0);
            }

            // Send texture data to GPU if the shader supports it
            if (Shaders[ActiveShader].GetAttribute("texcoord") != -1)
            {
                GL.BindBuffer(BufferTarget.ArrayBuffer, Shaders[ActiveShader].GetBuffer("texcoord"));
                GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(textureCoordsData.Length * Vector2.SizeInBytes), textureCoordsData, BufferUsageHint.StreamDraw);
                GL.VertexAttribPointer(Shaders[ActiveShader].GetAttribute("texcoord"), 2, VertexAttribPointerType.Float, true, 0, 0);
            }

            // Send normal data to GPU if the shader supports it
            if (Shaders[ActiveShader].GetAttribute("vNormal") != -1)
            {
                GL.BindBuffer(BufferTarget.ArrayBuffer, Shaders[ActiveShader].GetBuffer("vNormal"));
                GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(normalData.Length * Vector3.SizeInBytes), normalData, BufferUsageHint.StaticDraw);
                GL.VertexAttribPointer(Shaders[ActiveShader].GetAttribute("vNormal"), 3, VertexAttribPointerType.Float, true, 0, 0);
            }

            // Bind shader program to be used
            GL.UseProgram(Shaders[ActiveShader].ProgramID);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);

            // Send list of indecies to GPU
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, iboElementsBuffer);
            GL.BufferData(BufferTarget.ElementArrayBuffer, (IntPtr)(indiceData.Length * sizeof(int)), indiceData, BufferUsageHint.StaticDraw);

            // Send the inverted view matrix if required by the shader
            if (Shaders[ActiveShader].GetUniform("invertedview") != -1)
            {
                Matrix4 invertedView = Camera.GetViewMatrix().Inverted();
                GL.UniformMatrix4(Shaders[ActiveShader].GetUniform("invertedview"), false, ref invertedView);
            }

            // Apply single light data if supported by GPU
            if (Shaders[ActiveShader].GetUniform("light_position") != -1)
            {
                GL.Uniform3(Shaders[ActiveShader].GetUniform("light_position"), ref Lights[0].Position);
            }

            if (Shaders[ActiveShader].GetUniform("light_color") != -1)
            {
                Vector3 color = Lights[0].Color;
                GL.Uniform3(Shaders[ActiveShader].GetUniform("light_color"), ref color);
            }

            if (Shaders[ActiveShader].GetUniform("light_ambientIntensity") != -1)
            {
                GL.Uniform1(Shaders[ActiveShader].GetUniform("light_ambientIntensity"), Lights[0].AmbientIntensity);
            }

            if (Shaders[ActiveShader].GetUniform("light_diffuseIntensity") != -1)
            {
                GL.Uniform1(Shaders[ActiveShader].GetUniform("light_diffuseIntensity"), Lights[0].DiffuseIntensity);
            }

            // Apply multiple light data if supported by GPU
            for (int i = 0; i < Math.Min(Lights.Count, MAX_LIGHTS); i++)
            {
                if (Shaders[ActiveShader].GetUniform("lights[" + i + "].Position") != -1)
                {
                    GL.Uniform3(Shaders[ActiveShader].GetUniform("lights[" + i + "].Position"), ref Lights[i].Position);
                }

                if (Shaders[ActiveShader].GetUniform("lights[" + i + "].Color") != -1)
                {
                    Vector3 color = Lights[i].Color;
                    GL.Uniform3(Shaders[ActiveShader].GetUniform("lights[" + i + "].Color"), ref color);
                }

                if (Shaders[ActiveShader].GetUniform("lights[" + i + "].DiffuseIntensity") != -1)
                {
                    GL.Uniform1(Shaders[ActiveShader].GetUniform("lights[" + i + "].DiffuseIntensity"), Lights[i].AmbientIntensity);
                }

                if (Shaders[ActiveShader].GetUniform("lights[" + i + "].AmbientIntensity") != -1)
                {
                    GL.Uniform1(Shaders[ActiveShader].GetUniform("lights[" + i + "].AmbientIntensity"), Lights[i].AmbientIntensity);
                }

                if (Shaders[ActiveShader].GetUniform("lights[" + i + "].Direction") != -1)
                {
                    GL.Uniform3(Shaders[ActiveShader].GetUniform("lights[" + i + "].Direction"), ref Lights[i].Direction);
                }

                if (Shaders[ActiveShader].GetUniform("lights[" + i + "].Type") != -1)
                {
                    GL.Uniform1(Shaders[ActiveShader].GetUniform("lights[" + i + "].Type"), (int)Lights[i].Type);
                }

                if (Shaders[ActiveShader].GetUniform("lights[" + i + "].ConeAngle") != -1)
                {
                    GL.Uniform1(Shaders[ActiveShader].GetUniform("lights[" + i + "].ConeAngle"), Lights[i].ConeAngle);
                }

                if (Shaders[ActiveShader].GetUniform("lights[" + i + "].LinearAttenuation") != -1)
                {
                    GL.Uniform1(Shaders[ActiveShader].GetUniform("lights[" + i + "].LinearAttenuation"), Lights[i].LinearAttenuation);
                }

                if (Shaders[ActiveShader].GetUniform("lights[" + i + "].QuadraticAttenuation") != -1)
                {
                    GL.Uniform1(Shaders[ActiveShader].GetUniform("lights[" + i + "].QuadraticAttenuation"), Lights[i].QuadraticAttenuation);
                }
            }
        }
    }
}
