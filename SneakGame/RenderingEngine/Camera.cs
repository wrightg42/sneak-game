﻿using System;
using OpenTK;
using RenderingEngine.Objects;

namespace RenderingEngine
{
    /// <summary>
    /// A representation of a camera.
    /// </summary>
    public class Camera
    {
        /// <summary>
        /// Initializes a new instace of the <see cref="Camera"/> class.
        /// </summary>
        public Camera()
        {
            Position = Vector3.Zero;
            Orientation = new Vector3((float)Math.PI, 0, 0);
            FieldOfViewMatrix = new Matrix4();
            MoveSpeed = 0.2f;
            RotationSensitivity = 0.01f;
            ObjectFollowing = null;
            OffsetFromObject = Vector3.Zero;
            FollowObjectOrientation = false;
            ObjectToLookat = null;
        }

        /// <summary>
        /// Initializes a new instace of the <see cref="Camera"/> class.
        /// </summary>
        /// <param name="fieldOfViewMatrix">The field of view matrix used by the camera.</param>
        public Camera(Matrix4 fieldOfViewMatrix)
            : this()
        {
            FieldOfViewMatrix = fieldOfViewMatrix;
        }

        /// <summary>
        /// Initializes a new instace of the <see cref="Camera"/> class.
        /// </summary>
        /// <param name="fieldOfViewMatrix">The field of view matrix used by the camera.</param>
        /// <param name="position">The position of the camera.</param>
        /// <param name="orientation">The orientation of the camera.</param>
        public Camera(Matrix4 fieldOfViewMatrix, Vector3 position, Vector3 orientation)
            : this(fieldOfViewMatrix)
        {
            Position = position;
            Orientation = orientation;
        }

        /// <summary>
        /// Initializes a new instace of the <see cref="Camera"/> class.
        /// </summary>
        /// <param name="fieldOfViewMatrix">The field of view matrix used by the camera.</param>
        /// <param name="objectToFollow">The <see cref="Mesh"/> the camera is to follow.</param>
        /// <param name="offsetFromObject">The offset the camera has from the <see cref="Mesh"/> it is following.</param>
        /// <param name="followObjectOrientation">Whether the orientation of the camera must match the <see cref="Mesh"/>'s orientation.</param>
        public Camera(Matrix4 fieldOfViewMatrix, Mesh objectToFollow, Vector3 offsetFromObject, bool followObjectOrientation = false)
            : this(fieldOfViewMatrix)
        {
            BeginFollowing(objectToFollow, offsetFromObject, followObjectOrientation);
        }

        /// <summary>
        /// Initializes a new instace of the <see cref="Camera"/> class.
        /// </summary>
        /// <param name="fieldOfViewMatrix">The field of view matrix used by the camera.</param>
        /// <param name="position">The position of the camera.</param>
        /// <param name="orientation">The orientation of the camera.</param>
        /// <param name="objectToFollow">The <see cref="Mesh"/> the camera is to follow.</param>
        /// <param name="offsetFromObject">The offset the camera has from the <see cref="Mesh"/> it is following.</param>
        /// <param name="followObjectOrientation">Whether the orientation of the camera must match the <see cref="Mesh"/>'s orientation.</param>
        public Camera(Matrix4 fieldOfViewMatrix, Vector3 position, Vector3 orientation, Mesh objectToFollow, Vector3 offsetFromObject, bool followObjectOrientation = false)
            : this(fieldOfViewMatrix, position, orientation)
        {
            BeginFollowing(objectToFollow, offsetFromObject, followObjectOrientation);
        }

        /// <summary>
        /// Initializes a new instace of the <see cref="Camera"/> class.
        /// </summary>
        /// <param name="fieldOfViewMatrix">The field of view matrix used by the camera.</param>
        /// <param name="position">The position of the camera.</param>
        /// <param name="orientation">The orientation of the camera.</param>
        /// <param name="moveSpeed">The movespeed of the camera.</param>
        /// <param name="rotationSensitivity">The rotation sensitivity of the camera.</param>
        /// <param name="objectToFollow">The <see cref="Mesh"/> the camera is to follow.</param>
        /// <param name="offsetFromObject">The offset the camera has from the <see cref="Mesh"/> it is following.</param>
        /// <param name="followObjectOrientation">Whether the orientation of the camera must match the <see cref="Mesh"/>'s orientation.</param>
        public Camera(Matrix4 fieldOfViewMatrix, Vector3 position, Vector3 orientation, float moveSpeed, float rotationSensitivity, Mesh objectToFollow, Vector3 offsetFromObject, bool followObjectOrientation = false)
            : this(fieldOfViewMatrix, position, orientation, objectToFollow, offsetFromObject, followObjectOrientation)
        {
            MoveSpeed = moveSpeed;
            RotationSensitivity = rotationSensitivity;
        }

        /// <summary>
        /// Initializes a new instace of the <see cref="Camera"/> class.
        /// </summary>
        /// <param name="fieldOfViewMatrix">The field of view matrix used by the camera.</param>
        /// <param name="objectToLookat">The <see cref="Mesh"/> the camera is looking at.</param>
        public Camera(Matrix4 fieldOfViewMatrix, Mesh objectToLookat)
            : this(fieldOfViewMatrix)
        {
            ObjectToLookat = objectToLookat;
        }

        /// <summary>
        /// Initializes a new instace of the <see cref="Camera"/> class.
        /// </summary>
        /// <param name="fieldOfViewMatrix">The field of view matrix used by the camera.</param>
        /// <param name="position">The position of the camera.</param>
        /// <param name="orientation">The orientation of the camera.</param>
        /// <param name="objectToLookat">The <see cref="Mesh"/> the camera is looking at.</param>
        public Camera(Matrix4 fieldOfViewMatrix, Vector3 position, Vector3 orientation, Mesh objectToLookat)
            : this(fieldOfViewMatrix, position, orientation)
        {
            ObjectToLookat = objectToLookat;
        }

        /// <summary>
        /// Initializes a new instace of the <see cref="Camera"/> class.
        /// </summary>
        /// <param name="fieldOfViewMatrix">The field of view matrix used by the camera.</param>
        /// <param name="objectToLookat">The <see cref="Mesh"/> the camera is looking at.</param>
        /// <param name="objectToFollow">The <see cref="Mesh"/> the camera is to follow.</param>
        /// <param name="offsetFromObject">The offset the camera has from the <see cref="Mesh"/> it is following.</param>
        /// <param name="followObjectOrientation">Whether the orientation of the camera must match the <see cref="Mesh"/>'s orientation.</param>
        public Camera(Matrix4 fieldOfViewMatrix, Mesh objectToLookat, Mesh objectToFollow, Vector3 offsetFromObject, bool followObjectOrientation = false)
            : this(fieldOfViewMatrix, objectToFollow, offsetFromObject, followObjectOrientation)
        {
            ObjectToLookat = objectToLookat;
        }

        /// <summary>
        /// Initializes a new instace of the <see cref="Camera"/> class.
        /// </summary>
        /// <param name="fieldOfViewMatrix">The field of view matrix used by the camera.</param>
        /// <param name="position">The position of the camera.</param>
        /// <param name="orientation">The orientation of the camera.</param>
        /// <param name="objectToLookat">The <see cref="Mesh"/> the camera is looking at.</param>
        /// <param name="objectToFollow">The <see cref="Mesh"/> the camera is to follow.</param>
        /// <param name="offsetFromObject">The offset the camera has from the <see cref="Mesh"/> it is following.</param>
        /// <param name="followObjectOrientation">Whether the orientation of the camera must match the <see cref="Mesh"/>'s orientation.</param>
        public Camera(Matrix4 fieldOfViewMatrix, Vector3 position, Vector3 orientation, Mesh objectToLookat, Mesh objectToFollow, Vector3 offsetFromObject, bool followObjectOrientation = false)
            : this(fieldOfViewMatrix, position, orientation, objectToFollow, offsetFromObject, followObjectOrientation)
        {
            ObjectToLookat = objectToLookat;
        }

        /// <summary>
        /// Initializes a new instace of the <see cref="Camera"/> class.
        /// </summary>
        /// <param name="fieldOfViewMatrix">The field of view matrix used by the camera.</param>
        /// <param name="position">The position of the camera.</param>
        /// <param name="orientation">The orientation of the camera.</param>
        /// <param name="moveSpeed">The movespeed of the camera.</param>
        /// <param name="rotationSensitivity">The rotation sensitivity of the camera.</param>
        /// <param name="objectToLookat">The <see cref="Mesh"/> the camera is looking at.</param>
        /// <param name="objectToFollow">The <see cref="Mesh"/> the camera is to follow.</param>
        /// <param name="offsetFromObject">The offset the camera has from the <see cref="Mesh"/> it is following.</param>
        /// <param name="followObjectOrientation">Whether the orientation of the camera must match the <see cref="Mesh"/>'s orientation.</param>
        public Camera(Matrix4 fieldOfViewMatrix, Vector3 position, Vector3 orientation, float moveSpeed, float rotationSensitivity, Mesh objectToLookat, Mesh objectToFollow, Vector3 offsetFromObject, bool followObjectOrientation = false)
            : this(fieldOfViewMatrix, position, orientation, moveSpeed, rotationSensitivity, objectToFollow, offsetFromObject, followObjectOrientation)
        {
            ObjectToLookat = objectToLookat;
        }

        /// <summary>
        /// The position of the camera.
        /// </summary>
        public Vector3 Position;

        /// <summary>
        /// The orientation of the camera.
        /// </summary>
        public Vector3 Orientation;

        /// <summary>
        /// The field of view matrix of the camera.
        /// </summary>
        public Matrix4 FieldOfViewMatrix;

        /// <summary>
        /// The movespeed of the camera.
        /// </summary>
        public float MoveSpeed;

        /// <summary>
        /// The rotation sensitivity of the camera.
        /// </summary>
        public float RotationSensitivity;

        /// <summary>
        /// The <see cref="Mesh"/> the camera is following.
        /// </summary>
        public Mesh ObjectFollowing;

        /// <summary>
        /// The offset from the <see cref="Mesh"/> the camera is following.
        /// </summary>
        public Vector3 OffsetFromObject;

        /// <summary>
        /// Whether the orientation of the camera must match that of the <see cref="Mesh"/> it is following.
        /// </summary>
        public bool FollowObjectOrientation;

        /// <summary>
        /// The <see cref="Mesh"/> the camera is looking at.
        /// </summary>
        public Mesh ObjectToLookat;

        /// <summary>
        /// Calculates the view matrix of the camera.
        /// </summary>
        /// <returns>The calculated view matrix.</returns>
        public virtual Matrix4 GetViewMatrix()
        {
            CalculateLocation();

            if (ObjectToLookat == null)
            {
                // Get view matrix from forward and up vectors
                return Matrix4.LookAt(Position, Position + CalculateForwardVector(), CalculateUpVector());
            }
            else
            {
                // Turn where we are looking into view matrix
                return Matrix4.LookAt(Position, ObjectToLookat.Position, CalculateUpVector());
            }
        }

        /// <summary>
        /// Calculates the projection matrix of the camera.
        /// </summary>
        /// <returns></returns>
        public Matrix4 GetProjectionMatrix()
        {
            return GetViewMatrix() * FieldOfViewMatrix;
        }

        /// <summary>
        /// Calculates the location of the camera.
        /// </summary>
        /// <remarks>This is for when an object is being followed.</remarks>
        public void CalculateLocation()
        {
            // Change position to follow object if following is active
            if (ObjectFollowing != null)
            {
                Position = ObjectFollowing.Position + OffsetFromObject;
                if (FollowObjectOrientation)
                {
                    Orientation = ObjectFollowing.Rotation;
                }
            }
        }

        /// <summary>
        /// Move the camera.
        /// </summary>
        /// <param name="x">The x movement of the camera.</param>
        /// <param name="y">The y movement of the camera.</param>
        /// <param name="z">The z movement of the camera.</param>
        public virtual void Move(float x, float y, float z)
        {
            if (ObjectFollowing == null)
            {
                // Get forward, up, and right vectors to calculate the offset
                Vector3 forward = CalculateForwardVector();
                Vector3 up = CalculateUpVector();
                Vector3 right = Vector3.Cross(forward, up);

                // Calculate offset
                Vector3 offset = new Vector3();
                offset += x * right;
                offset += z * forward;
                offset += y * up;

                // Make values accurate to the move speed of the camera
                offset.NormalizeFast();
                offset = Vector3.Multiply(offset, MoveSpeed);

                Position += offset;
            }
        }

        /// <summary>
        /// Rotate the camera.
        /// </summary>
        /// <param name="rotation">The <see cref="Vector3"/> representing the rotation.</param>
        public void Rotate(Vector3 rotation)
        {
            if (ObjectToLookat == null)
            {
                Orientation.X = (float)((Orientation.X + rotation.X) % (Math.PI * 2));
                Orientation.Y = (float)((Orientation.Y + rotation.Y) % (Math.PI * 2));
                Orientation.Z = (float)((Orientation.Z + rotation.Z) % (Math.PI * 2));
            }
        }

        /// <summary>
        /// Rotate the camera based off mouse movemnt.
        /// </summary>
        /// <param name="x">The x movement of the mouse.</param>
        /// <param name="y">The y movement of the mouse.</param>
        public void Rotate(float x, float y)
        {
            if (ObjectToLookat == null)
            {
                // Adjust rotation to the sensitivity
                x *= RotationSensitivity;
                y *= RotationSensitivity;

                Orientation.Y = (float)((Orientation.Y + x) % (Math.PI * 2));
                Orientation.X = (float)((Orientation.X + y) % (Math.PI * 2));

                // Limit how much we can look up/down
                Orientation.X = (float)Math.Max(Math.Min(Orientation.X, Math.PI / 2 - 0.01), -Math.PI / 2 + 0.01);
           } 
        }

        /// <summary>
        /// Begin following a <see cref="Mesh"/>.
        /// </summary>
        /// <param name="objectToTrack">The <see cref="Mesh"/> to follow.</param>
        /// <param name="offsetFromObject">The offset from the <see cref="Mesh"/>.</param>
        /// <param name="trackObjectOrientation">Whether the orientation of the camera must match the <see cref="Mesh"/>'s orientation.</param>
        public void BeginFollowing(Mesh objectToTrack, Vector3 offsetFromObject, bool trackObjectOrientation = false)
        {
            ObjectFollowing = objectToTrack;
            OffsetFromObject = offsetFromObject;
            FollowObjectOrientation = trackObjectOrientation;
            CalculateLocation();
        }

        /// <summary>
        /// Stop following a <see cref="Mesh"/>.
        /// </summary>
        public void StopFollowing()
        {
            CalculateLocation();
            ObjectFollowing = null;
        }

        /// <summary>
        /// Centers the camera with the rotation of the <see cref="Mesh"/> being followed.
        /// </summary>
        /// <param name="rotateZ">Whether the z axis rotation sould be copied.</param>
        public void CenterCameraWithFollowObject(bool rotateZ = false)
        {
            if (ObjectFollowing != null)
            {
                Orientation = ObjectFollowing.Rotation;
            }
        }

        /// <summary>
        /// Being looking at a particular <see cref="Mesh"/>.
        /// </summary>
        /// <param name="objectToLookat">The <see cref="Mesh"/> to look at.</param>
        public void BeginLookat(Mesh objectToLookat)
        {
            ObjectToLookat = objectToLookat;
        }

        /// <summary>
        /// Stop looking at a <see cref="Mesh"/>.
        /// </summary>
        public void StopLookat()
        {
            ObjectToLookat = null;
        }

        /// <summary>
        /// Calculates the directional vector that the camera is pointing in.
        /// </summary>
        /// <returns>The forward vector.</returns>
        private Vector3 CalculateForwardVector()
        {
            if (ObjectToLookat == null)
            {
                return new Vector3((float)Math.Sin(Orientation.Y), (float)Math.Sin(Orientation.X), (float)Math.Cos(Orientation.Y));
            }
            else
            {
                return ObjectToLookat.Position - Position;
            }
        }
        
        /// <summary>
        /// Calculates the directional vector that points up from the camera's perspective.
        /// </summary>
        /// <returns>The up vector.</returns>
        private Vector3 CalculateUpVector()
        {
            return new Vector3((float)Math.Sin(Orientation.Z), (float)Math.Cos(Orientation.Z), 0);
        }
    }
}
