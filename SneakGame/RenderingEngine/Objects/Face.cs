﻿using System.Collections.Generic;
using System.Linq;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace RenderingEngine.Objects
{
    /// <summary>
    /// A class representing a face in a <see cref="Mesh"/>.
    /// </summary>
    public class Face
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Face"/> class.
        /// </summary>
        public Face()
        {
            Verticies = new List<Vertex>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Face"/> class.
        /// </summary>
        /// <param name="verticies">An array of vertex locations to make the face.</param>
        public Face(params Vector3[] verticies)
            : this()
        {
            Verticies = new List<Vertex>(verticies.Select(v => new Vertex(v)));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Face"/> class.
        /// </summary>
        /// <param name="verticies">An array of verticies to make the face.</param>
        public Face(params Vertex[] verticies)
            : this()
        {
            Verticies = new List<Vertex>(verticies);
        }

        /// <summary>
        /// The list of verticies that make up the face.
        /// </summary>
        public List<Vertex> Verticies;

        /// <summary>
        /// Gets the drawing type needed to be used when rendering the face.
        /// </summary>
        public BeginMode DrawType
        {
            get
            {
                if (Verticies.Count == 1)
                {
                    return BeginMode.Points;
                }
                else if (Verticies.Count == 2)
                {
                    return BeginMode.Lines;
                }
                else if (Verticies.Count == 3)
                {
                    return BeginMode.Triangles;
                }
                else if (Verticies.Count == 4)
                {
                    return BeginMode.Quads;
                }
                else
                {
                    return BeginMode.Polygon;
                }
            }
        }

        /// <summary>
        /// Gets the midpoint of the face.
        /// </summary>
        public Vector3 MidPoint
        {
            get
            {
                Vector3 mid = new Vector3();
                foreach (Vertex v in Verticies)
                {
                    mid += v.Position;
                }

                return mid / Verticies.Count;
            }
        }
    }
}
