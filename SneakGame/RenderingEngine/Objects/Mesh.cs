﻿using System.Collections.Generic;
using System.Linq;
using OpenTK;
using RenderingEngine.Objects.Collisions;

namespace RenderingEngine.Objects
{
    /// <summary>
    /// A class representing a renderable mesh.
    /// </summary>
    public class Mesh
    {
        /// <summary>
        /// Initializes a new instace of the <see cref="Mesh"/> class.
        /// </summary>
        public Mesh()
        {
            Position = Vector3.Zero;
            Rotation = Vector3.Zero;
            Scale = Vector3.One;
            TextureID = -1;
            Material = new Material();
            Faces = new List<Face>();
            Verticies = new Vector3[0];
            Colors = new Vector3[0];
            Normals = new Vector3[0];
            TextureCoords = new Vector2[0];
            IndicieCount = 0;
            CalculateModelMatrix();
            ModelViewProjectionMatrix = new Matrix4();
            Name = string.Empty;
            AxisAlignedBoundingBox = null;
            OrientedBoundingBox = null;
            AxisAlignedBoundingBoxGenerated = false;
            OrientedBoundingBoxGenerated = false;
        }

        /// <summary>
        /// Initializes a new instace of the <see cref="Mesh"/> class.
        /// </summary>
        /// <param name="position">The position of the mesh.</param>
        /// <param name="rotation">The rotation applied to the mesh.</param>
        /// <param name="scale">The scale of the mesh.</param>
        public Mesh(Vector3 position, Vector3 rotation, Vector3 scale)
            : this()
        {
            Position = position;
            Rotation = rotation;
            Scale = scale;
        }

        /// <summary>
        /// Initializes a new instace of the <see cref="Mesh"/> class.
        /// </summary>
        /// <param name="faces">The collection of faces building up the mesh.</param>
        public Mesh(IEnumerable<Face> faces)
            : this()
        {
            Faces = new List<Face>(faces);
        }

        /// <summary>
        /// Initializes a new instace of the <see cref="Mesh"/> class.
        /// </summary>
        /// <param name="material">The material of the mesh.</param>
        public Mesh(Material material)
            : this()
        {
            Material = material;
        }

        /// <summary>
        /// Initializes a new instace of the <see cref="Mesh"/> class.
        /// </summary>
        /// <param name="position">The position of the mesh.</param>
        /// <param name="rotation">The rotation applied to the mesh.</param>
        /// <param name="scale">The scale of the mesh.</param>
        /// <param name="faces">The collection of faces building up the mesh.</param>
        public Mesh(Vector3 position, Vector3 rotation, Vector3 scale, IEnumerable<Face> faces)
            : this(position, rotation, scale)
        {
            Faces = new List<Face>(faces);
        }

        /// <summary>
        /// Initializes a new instace of the <see cref="Mesh"/> class.
        /// </summary>
        /// <param name="faces">The collection of faces building up the mesh.</param>
        /// <param name="material">The material of the mesh.</param>
        public Mesh(IEnumerable<Face> faces, Material material)
            : this(faces)
        {
            Material = material;
        }

        /// <summary>
        /// Initializes a new instace of the <see cref="Mesh"/> class.
        /// </summary>
        /// <param name="position">The position of the mesh.</param>
        /// <param name="rotation">The rotation applied to the mesh.</param>
        /// <param name="scale">The scale of the mesh.</param>
        /// <param name="material">The material of the mesh.</param>
        public Mesh(Vector3 position, Vector3 rotation, Vector3 scale, Material material)
            : this(position, rotation, scale)
        {
            Material = material;
        }

        /// <summary>
        /// Initializes a new instace of the <see cref="Mesh"/> class.
        /// </summary>
        /// <param name="position">The position of the mesh.</param>
        /// <param name="rotation">The rotation applied to the mesh.</param>
        /// <param name="scale">The scale of the mesh.</param>
        /// <param name="faces">The collection of faces building up the mesh.</param>
        /// <param name="material">The material of the mesh.</param>
        public Mesh(Vector3 position, Vector3 rotation, Vector3 scale, IEnumerable<Face> faces, Material material)
            : this(position, rotation, scale, faces)
        {
            Material = material;
        }

        /// <summary>
        /// A private store of the position of the mesh.
        /// </summary>
        private Vector3 _position;

        /// <summary>
        /// Gets or sets the position of the mesh.
        /// </summary>
        public Vector3 Position
        {
            get
            {
                return _position;
            }
            set
            {
                Vector3 diff = value - _position;
                if (AxisAlignedBoundingBoxGenerated)
                {
                    AxisAlignedBoundingBox.Center += diff;
                }

                if (OrientedBoundingBoxGenerated)
                {
                    OrientedBoundingBox.Center += diff;
                }

                _position = value;
                CalculateModelMatrix();
            }
        }

        /// <summary>
        /// A private store of the rotation of the mesh.
        /// </summary>
        private Vector3 _rotation;

        /// <summary>
        /// Get or sets the rotation applied to the mesh.
        /// </summary>
        public Vector3 Rotation
        {
            get
            {
                return _rotation;
            }
            set
            {
                if (AxisAlignedBoundingBoxGenerated)
                {
                    AxisAlignedBoundingBox = new AxisAlignedBoundingBox(this);
                }

                if (OrientedBoundingBoxGenerated)
                { 
                    Vector3 diff = value - _rotation;
                    OrientedBoundingBox.Orientation += diff;
                }

                _rotation = value;
                CalculateModelMatrix();
            }
        }

        /// <summary>
        /// A private store of the scale of the mesh.
        /// </summary>
        private Vector3 _scale;

        /// <summary>
        /// Gets or sets the scale of the mesh.
        /// </summary>
        public virtual Vector3 Scale
        {
            get
            {
                return _scale;
            }
            set
            {
                if (AxisAlignedBoundingBoxGenerated)
                {
                    float x = AxisAlignedBoundingBox.Extents.X * value.X / _scale.X;
                    float y = AxisAlignedBoundingBox.Extents.Y * value.Y / _scale.Y;
                    float z = AxisAlignedBoundingBox.Extents.Z * value.Z / _scale.Z;
                    AxisAlignedBoundingBox.Extents = new Vector3(x, y, z);
                }

                if (OrientedBoundingBoxGenerated)
                { 
                    float x = OrientedBoundingBox.Extents.X * value.X / _scale.X;
                    float y = OrientedBoundingBox.Extents.Y * value.Y / _scale.Y;
                    float z = OrientedBoundingBox.Extents.Z * value.Z / _scale.Z;
                    OrientedBoundingBox.Extents = new Vector3(x, y, z);
                }

                _scale = value;
                CalculateModelMatrix();
            }
        }

        /// <summary>
        /// The texture appled to the mesh.
        /// </summary>
        /// <remarks>Get's overridden by ambient/diffuse textures.</remarks>
        public int TextureID;

        /// <summary>
        /// The material of the mesh.
        /// </summary>
        public Material Material;

        /// <summary>
        /// A private list of the faces making the mesh.
        /// </summary>
        private List<Face> _faces;

        /// <summary>
        /// Gets or sets the faces of the mesh.
        /// </summary>
        public List<Face> Faces
        {
            get
            {
                return _faces;
            }
            set
            {
                if (AxisAlignedBoundingBoxGenerated)
                {
                    AxisAlignedBoundingBox = new AxisAlignedBoundingBox(this);
                }

                if (OrientedBoundingBoxGenerated)
                { 
                    OrientedBoundingBox = new OrientedBoundingBox(this);
                }

                _faces = value;
                CalculateVertexData();
            }
        }

        /// <summary>
        /// Gets or sets the verticies of the mesh.
        /// </summary>
        public Vector3[] Verticies { get; protected set; }

        /// <summary>
        /// Gets or sets the colors of the mesh at each vertex.
        /// </summary>
        public Vector3[] Colors { get; protected set; }

        /// <summary>
        /// Gets or sets the normals of the mesh at each vertex.
        /// </summary>
        public Vector3[] Normals { get; protected set; }

        /// <summary>
        /// Gets or sets the texture coordinates at each vertex.
        /// </summary>
        public Vector2[] TextureCoords { get; protected set; }

        /// <summary>
        /// Gets or sets how many verticies there are in the mesh.
        /// </summary>
        public int IndicieCount { get; protected set; }

        /// <summary>
        /// The matrix representing the worldspace transformations applied to the mesh.
        /// </summary>
        public Matrix4 ModelMatrix;

        /// <summary>
        /// The matrix representing the cameraspace transformations applied to the mesh.
        /// </summary>
        public Matrix4 ModelViewProjectionMatrix;

        /// <summary>
        /// The name of the mesh.
        /// </summary>
        public string Name;

        /// <summary>
        /// Gets or sets the <see cref="AxisAlignedBoundingBox"/> box for the current mesh.
        /// </summary>
        public AxisAlignedBoundingBox AxisAlignedBoundingBox { get; set; }

        /// <summary>
        /// Gets or sets the <see cref="OrientedBoundingBox"/> box for the current mesh.
        /// </summary>
        public OrientedBoundingBox OrientedBoundingBox { get; set; }

        /// <summary>
        /// Gets or sets a value indicating wheather an axis aligned bounding box has been generated.
        /// </summary>
        private bool AxisAlignedBoundingBoxGenerated { get; set; }

        /// <summary>
        /// Gets or sets a value indicating wheather a oriented bounding box has been generated.
        /// </summary>
        private bool OrientedBoundingBoxGenerated { get; set; }

        /// <summary>
        /// Calculates the <see cref="ModelMatrix"/>.
        /// </summary>
        public void CalculateModelMatrix()
        {
            ModelMatrix = Matrix4.CreateScale(Scale) * Matrix4.CreateRotationX(Rotation.X) * Matrix4.CreateRotationY(Rotation.Y) * Matrix4.CreateRotationZ(Rotation.Z) * Matrix4.CreateTranslation(Position);
        }

        /// <summary>
        /// Calculates the <see cref="ModelViewProjectionMatrix"/>.
        /// </summary>
        /// <param name="viewProjectionMatrix">The view projection matrix of the camera viewing the model.</param>
        public void CalculateModelViewProjectionMatrix(Matrix4 viewProjectionMatrix)
        {
            ModelViewProjectionMatrix = ModelMatrix * viewProjectionMatrix;
        }

        /// <summary>
        /// Gets an array of the indicies of the model that have been offset.
        /// </summary>
        /// <param name="offset">How much to offset the indicies by.</param>
        /// <returns>The array of offset indicies of the mesh.</returns>
        public int[] GetIndices(int offset = 0)
        {
            return Enumerable.Range(offset, IndicieCount).ToArray();
        }

        /// <summary>
        /// Calculates the normals at each vertex in the mesh.
        /// </summary>
        public virtual void CalculateNormals()
        {
            // Edit normals for each face
            foreach (Face f in Faces)
            {
                // Only add normal if we can (more than 3 points on face)
                if (f.Verticies.Count >= 3)
                {
                    for (int i = 0; i < f.Verticies.Count; i++)
                    {
                        // Calculate normal as the cross product of lines for that point
                        Vertex[] jointPoints = new Vertex[] { f.Verticies[(f.Verticies.Count + i - 1) % f.Verticies.Count], f.Verticies[i], f.Verticies[(i + 1) % f.Verticies.Count] };
                        Vector3[] edges = new Vector3[] { jointPoints[2].Position - jointPoints[1].Position, jointPoints[0].Position - jointPoints[1].Position };
                        Vector3 normal = Vector3.Cross(edges[0], edges[1]);
                        f.Verticies[i].Normal = normal;

                        Vector2[] textureChange = new Vector2[] { jointPoints[2].TextureCoord - jointPoints[1].TextureCoord, jointPoints[0].TextureCoord - jointPoints[1].TextureCoord };
                    }
                }
            }

            // Reset useable normal array
            Normals = Faces.SelectMany((Face f) => f.Verticies).Select((Vertex v) => v.Normal).ToArray();
        }

        /// <summary>
        /// Calcualtes data about each vertex to store in their lists.
        /// </summary>
        public void CalculateVertexData()
        {
            // Turn data in faces into the arrays to be used by program
            Verticies = Faces.SelectMany((Face f) => f.Verticies).Select((Vertex v) => v.Position).ToArray();
            Colors = Faces.SelectMany((Face f) => f.Verticies).Select((Vertex v) => v.Color).ToArray();
            Normals = Faces.SelectMany((Face f) => f.Verticies).Select((Vertex v) => v.Normal).ToArray();
            TextureCoords = Faces.SelectMany((Face f) => f.Verticies).Select((Vertex v) => v.TextureCoord).ToArray();
            IndicieCount = Faces.Sum((Face f) => f.Verticies.Count);
        }

        /// <summary>
        /// Generates the <see cref="BoundingBox"/>s for the current <see cref="Mesh"/>.
        /// </summary>
        /// <param name="axisAligned">Whether an axis aligned bounding box should be generaeted.</param>
        /// <param name="oriented">Whether a oriented bounding box should be generated.</param>
        /// <remarks>The bounding box is kept updated after it has been generated.</remarks>
        public void GenerateBoundingBox(bool axisAligned = false, bool oriented = true)
        {
            if (axisAligned)
            {
                AxisAlignedBoundingBox = new AxisAlignedBoundingBox(this);
                AxisAlignedBoundingBoxGenerated = true;
            }

            if (oriented)
            {
                OrientedBoundingBox = new OrientedBoundingBox(this);
                OrientedBoundingBoxGenerated = true;
            }
        }

        /// <summary>
        /// Destroys the <see cref="BoundingBox"/>s for the current <see cref="Mesh"/>.
        /// </summary>
        /// <param name="axisAligned">Whether to destroy the axis aligned bounding box.</param>
        /// <param name="oriented">Wheather to destory the oriented bounding box.</param>
        public void DestroyBoundingBox(bool axisAligned = false, bool oriented = true)
        {
            if (axisAligned)
            {
                AxisAlignedBoundingBox = null;
                AxisAlignedBoundingBoxGenerated = false;
            }

            if (oriented)
            {
                OrientedBoundingBox = null;
                OrientedBoundingBoxGenerated = false;
            }
        }

        /// <summary>
        /// Clones a mesh's geometry and material data.
        /// </summary>
        /// <param name="m">The mesh to clone.</param>
        public void CloneMeshData(Mesh m)
        {
            TextureID = m.TextureID;
            Material = new Material(m.Material.AmbientColor, m.Material.DiffuseColor, m.Material.SpecularColor, m.Material.SpecularExponent, m.Material.Opacity);
            Material.AmbientMap = m.Material.AmbientMap;
            Material.DiffuseMap = m.Material.DiffuseMap;
            Material.SpecularMap = m.Material.SpecularMap;
            Material.NormalMap = m.Material.NormalMap;
            Faces = m.Faces;
            CalculateVertexData();
        }

        /// <summary>
        /// Sets the colour of the mesh.
        /// </summary>
        /// <param name="color">The colour to turn the mesh.</param>
        public void SetMeshColor(Vector3 color)
        {
            Material.AmbientColor = color;
            Material.DiffuseColor = color;
            Material.SpecularColor = color;
        }
    }
}
