﻿using OpenTK;

namespace RenderingEngine.Objects
{
    /// <summary>
    /// A class representing a vertex in a <see cref="Face"/>.
    /// </summary>
    public class Vertex
    {
        /// <summary>
        /// Initializes a new instace of the <see cref="Vertex"/> class.
        /// </summary>
        public Vertex()
        {
            Position = new Vector3();
            Normal = new Vector3();
            TextureCoord = new Vector2();
            Color = new Vector3();
        }

        /// <summary>
        /// Initializes a new instace of the <see cref="Vertex"/> class.
        /// </summary>
        /// <param name="position">The position of the vertex.</param>
        public Vertex(Vector3 position)
        {
            Position = position;
            Normal = new Vector3();
            TextureCoord = new Vector2();
            Color = new Vector3();
        }

        /// <summary>
        /// Initializes a new instace of the <see cref="Vertex"/> class.
        /// </summary>
        /// <param name="position">The position of the vertex.</param>
        /// <param name="normal">The normal from the vertex.</param>
        /// <param name="textureCoord">The texture coordinate of the vertex.</param>
        public Vertex(Vector3 position, Vector3 normal, Vector2 textureCoord)
        {
            Position = position;
            Normal = normal;
            TextureCoord = textureCoord;
            Color = new Vector3();
        }

        /// <summary>
        /// Initializes a new instace of the <see cref="Vertex"/> class.
        /// </summary>
        /// <param name="position">The position of the vertex.</param>
        /// <param name="normal">The normal from the vertex.</param>
        /// <param name="textureCoord">The texture coordinate of the vertex.</param>
        /// <param name="color">The color of the vertex.</param>
        public Vertex(Vector3 position, Vector3 normal, Vector2 textureCoord, Vector3 color)
        {
            Position = position;
            Normal = normal;
            TextureCoord = textureCoord;
            Color = color;
        }

        /// <summary>
        /// The position of the vertex. 
        /// </summary>
        public Vector3 Position;

        /// <summary>
        /// The normal from the vertex.
        /// </summary>
        public Vector3 Normal;

        /// <summary>
        /// The texture coordinate of the vertex.
        /// </summary>
        public Vector2 TextureCoord;

        /// <summary>
        /// The color of the vertex.
        /// </summary>
        public Vector3 Color;
    }
}
