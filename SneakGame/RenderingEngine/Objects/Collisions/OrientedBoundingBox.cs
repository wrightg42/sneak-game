﻿using System.Collections.Generic;
using System.Linq;
using OpenTK;

namespace RenderingEngine.Objects.Collisions
{
    /// <summary>
    /// A class representing an oriented bounding box.
    /// </summary>
    public class OrientedBoundingBox : BoundingBox
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OrientedBoundingBox"/> class.
        /// </summary>
        /// <param name="mesh">The mesh to create a bounding box for.</param>
        /// <remarks>This bounding box assumes the identity of the object can accurately be modeled by an AABB.</remarks>
        public OrientedBoundingBox(Mesh mesh)
        {
            // Get list of the mesh's verticies and apply the object's transformation
            List<Vector3> verticies = mesh.Verticies.ToList();
            verticies = verticies.Select(v => Vector3.Transform(v, Matrix4.CreateScale(mesh.Scale) * Matrix4.CreateTranslation(mesh.Position))).ToList();

            // Calculate center and extents of the bounding box
            GenerateBoundingBox(verticies);

            // Set orientation
            Orientation = mesh.Rotation;
        }

        /// <summary>
        /// Use seperating axis theorem to check if oriented bounding boxes have collided.
        /// </summary>
        /// <param name="b">The box to check if there has been a collision with.</param>
        /// <returns>Whether there has been a collision.</returns>
        public bool Collision(BoundingBox b)
        {
            // Collect normals of each box
            List<Vector3> axes = new List<Vector3>();
            if (!UpdatedNormals)
            {
                GenerateNormals();
            }
            if (!b.UpdatedNormals)
            {
                b.GenerateNormals();
            }
            axes.AddRange(Normals);
            axes.AddRange(b.Normals);

            // Check if there is a gap along any of the axes
            foreach (Vector3 axis in axes)
            {
                if (GapAlongAxis(axis, this, b))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Use seperating axis theorem to check if a oriented bounding box has collided with a mesh.
        /// </summary>
        /// <param name="m">The mesh that has potentially collided.</param>
        /// <returns>Whether there has been a collision.</returns>
        /// <remarks>The method creates a convex hull around the mesh so may produce some false positives.</remarks>
        public bool Collision(Mesh m)
        {
            // Collect normals of the box
            List<Vector3> axes = new List<Vector3>();
            if (!UpdatedNormals)
            {
                GenerateNormals();
            }
            axes.AddRange(Normals);

            // Collect unqiue normals of the mesh
            Matrix4 tranformation = Matrix4.CreateScale(m.Scale) * Matrix4.CreateRotationX(m.Rotation.X) * Matrix4.CreateRotationY(m.Rotation.Y) * Matrix4.CreateRotationZ(m.Rotation.Z) * Matrix4.CreateTranslation(m.Position);
            List<Vector3> meshNormals = m.Normals.Select(n => Vector3.TransformNormal(n, tranformation)).ToList();
            meshNormals = meshNormals.GroupBy(n => n).Select(g => g.First()).ToList();
            axes.AddRange(meshNormals);

            // Check if there is a gap along any of the axes
            foreach (Vector3 axis in axes)
            {
                if (GapAlongAxis(axis, this, m))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Checks if there is a gap between a bounding boxes along an axis.
        /// </summary>
        /// <param name="axis">The axis to check for a gap.</param>
        /// <param name="b">The first bounding box.</param>
        /// <param name="m">The second bounding box.</param>
        /// <returns>Whether there was a seperating gap between the bounding boxes.</returns>
        private static bool GapAlongAxis(Vector3 axis, BoundingBox b1, BoundingBox b2)
        {
            // Get min/max of each mesh along the axis
            Vector2 m1MinMax = GetMinMaxAlongAxis(axis, b1.Verticies);
            Vector2 m2MinMax = GetMinMaxAlongAxis(axis, b2.Verticies);

            // Calculate if the meshes are seperated by the axis
            if (m1MinMax.Y <= m2MinMax.X || m1MinMax.X >= m2MinMax.Y)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Checks if there is a gap between a bounding box and a mesh along an axis.
        /// </summary>
        /// <param name="axis">The axis to check for a gap.</param>
        /// <param name="b">The bounding box.</param>
        /// <param name="m">The mesh.</param>
        /// <returns>Whether there was a seperating gap between the bounding box and mesh.</returns>
        private static bool GapAlongAxis(Vector3 axis, BoundingBox b, Mesh m)
        {
            // Get min/max of each mesh along the axis
            Vector2 m1MinMax = GetMinMaxAlongAxis(axis, b.Verticies);
            Vector2 m2MinMax = GetMinMaxAlongAxis(axis, m);

            // Calculate if the meshes are seperated by the axis
            if (m1MinMax.Y < m2MinMax.X || m1MinMax.X > m2MinMax.Y)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
