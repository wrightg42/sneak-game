﻿using System;
using System.Collections.Generic;
using System.IO;
using OpenTK;

namespace RenderingEngine.Objects.Collisions
{
    /// <summary>
    /// A class used to perform raycast checks. Raycasts are implemented as horizontal at a specified height.
    /// </summary>
    public class Raycast
    {
        /// <summary>
        /// Gets or sets the raycast mesh.
        /// </summary>
        /// <remarks>This is so that a raycast can be performed without loading the raycast mesh.
        private static Mesh RaycastMesh { get; set; }

        /// <summary>
        /// Initializes the raycast mesh used to check raycasts..
        /// </summary>
        static Raycast()
        {
            // Set to destination folder and import raycast mesh
            Directory.SetCurrentDirectory("Resources\\Assets\\");
            Dictionary<string, Material> materials = new Dictionary<string, Material>();
            RaycastMesh = ObjectLoader.LoadMeshFromFile("raycast.obj", ref materials);
            Directory.SetCurrentDirectory("..\\..\\");
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Raycast"/> class.
        /// </summary>
        /// <param name="start">The start coordinate of the ray.</param>
        /// <param name="end">The end coordinate of the ray.</param>
        /// <param name="height">The height of the ray.</param>
        public Raycast(Vector2 start, Vector2 end, float height = 0)
        {
            // Clone mesh data
            Ray = new Mesh();
            Ray.CloneMeshData(RaycastMesh);
            Ray.Name = "Raycast";

            // Set up the bounding box data
            Ray.GenerateBoundingBox();

            // Calculate the direction vector from start to end
            Vector2 direction = end - start;

            // Set position and length to be accurate
            Vector2 position = start + (direction / 2);
            Ray.Position = new Vector3(position.X, height, position.Y);
            Ray.Scale = new Vector3(direction.Length / 2, 1, 1);

            // Calculate orientation
            float rotationY = (float)Math.Atan(direction.Y / direction.X);
            Ray.Rotation = new Vector3(0, rotationY, 0);
        }

        /// <summary>
        /// The ray represented as a mesh.
        /// </summary>
        private Mesh Ray { get; set; }

        /// <summary>
        /// Calculates the intersections the raycast makes with meshes.
        /// </summary>
        /// <param name="meshes">The meshes that could be penetrated.</param>
        /// <returns>The meshes that the raycast intersects with.</returns>
        public List<Mesh> Intersections(IEnumerable<Mesh> meshes)
        {
            // Get list of meshes the raycast intersects
            List<Mesh> intersections = new List<Mesh>();
            foreach (Mesh m in meshes)
            {
                if (Ray.OrientedBoundingBox.Collision(m.OrientedBoundingBox))
                {
                    intersections.Add(m);
                }
            }

            return intersections;
        }
    }
}
