﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenTK;

namespace RenderingEngine.Objects.Collisions
{
    /// <summary>
    /// A class representing a penetration point of a collision.
    /// </summary>
    public class Contact
    {
        /// <summary>
        /// Initializes a new instace of the <see cref="Contact"/> class.
        /// </summary>
        /// <param name="penetrating">The mesh creating a contact.</param>
        /// <param name="box">The box being collided with.</param>
        public Contact(Mesh penetrating, BoundingBox box)
            :this(ClosestFace(penetrating, box.Center).Verticies.Select(v => v.Position), box)
        {
        }

        /// <summary>
        /// Initializes a new instace of the <see cref="Contact"/> class.
        /// </summary>
        /// <param name="point">The point creating a contact.</param>
        /// <param name="box">The box being collided with.</param>
        public Contact(Vector3 point, BoundingBox box)
        {
            // Convert point to localspace of the box
            Matrix4 transformation = Matrix4.CreateTranslation(-box.Center) * Matrix4.CreateRotationZ(-box.Orientation.Z) * Matrix4.CreateRotationY(-box.Orientation.Y) * Matrix4.CreateRotationX(-box.Orientation.X);
            Vector3 relPoint = Vector3.Transform(point, transformation);

            // Make sure normals are up to date
            box.GenerateNormals();

            // Determine axis of penetration, as well as depth
            Contact c = CalculateContact(box, relPoint);

            // Penetration occured, store data in variables
            Depth = c.Depth;
            Normal = c.Normal;
        }

        /// <summary>
        /// Initializes a new instace of the <see cref="Contact"/> class.
        /// </summary>
        /// <param name="points">The points creating a contact.</param>
        /// <param name="box">The box being collided with.</param>
        public Contact(IEnumerable<Vector3> points, BoundingBox box)
        {
            Depth = 0;
            Normal = new Vector3();

            // Iterate over all points to test for best overlap
            foreach (Vector3 p in points)
            {
                Contact c = new Contact(p, box);
                if (c.Depth > Depth)
                {
                    Depth = c.Depth;
                    Normal = c.Normal;
                }
            }
        }

        /// <summary>
        /// Initializes a new instace of the <see cref="Contact"/> class.
        /// </summary>
        /// <param name="depth">The depth of the contact.</param>
        /// <param name="normal">The normal of the contact.</param>
        private Contact(float depth, Vector3 normal)
        {
            Depth = depth;
            Normal = normal;
        }

        /// <summary>
        /// Gets or sets the normal of the face that is being penetrated.
        /// </summary>
        public Vector3 Normal { get; set; }

        /// <summary>
        /// Gets or sets the depth of the penetration.
        /// </summary>
        public float Depth { get; set; }

        /// <summary>
        /// Calcualtes the closest point from a mesh's geometry to a specific point in space.
        /// </summary>
        /// <param name="penetrating">The mesh to work out the cloest point of.</param>
        /// <param name="point">The point to be tested against.</param>
        /// <returns>The closest point from the mesh's geometry to the specified point.</returns>
        public static Vector3 ClosestPoint(Mesh penetrating, Vector3 point)
        {
            float minDist = float.MaxValue;
            Vector3 minPoint = new Vector3();
            Matrix4 transformation = Matrix4.CreateScale(penetrating.Scale) * Matrix4.CreateRotationX(penetrating.Rotation.X) * Matrix4.CreateRotationY(penetrating.Rotation.Y) * Matrix4.CreateRotationZ(penetrating.Rotation.Z);
            Vector3 adjustment = penetrating.Position - point; // Repeated calcualtion used to evaluate distance

            // Calcualte the distance of each point in the mesh
            foreach (Vector3 p in penetrating.Verticies)
            {
                Vector3 transP = Vector3.Transform(p, transformation);
                float dist = (transP + adjustment).Length;
                if (dist < minDist)
                {
                    minDist = dist;
                    minPoint = transP + penetrating.Position;
                }
            }

            return minPoint;
        }

        /// <summary>
        /// Calcualtes the closest point from a mesh's geometry to a specific point in space.
        /// </summary>
        /// <param name="penetrating">The mesh to work out the cloest point of.</param>
        /// <param name="point">The point to be tested against.</param>
        /// <returns>The closest point from the mesh's geometry to the specified point.</returns>
        public static Face ClosestFace(Mesh penetrating, Vector3 point)
        {
            float minDist = float.MaxValue;
            Face minFace = new Face();
            Matrix4 transformation = Matrix4.CreateScale(penetrating.Scale) * Matrix4.CreateRotationX(penetrating.Rotation.X) * Matrix4.CreateRotationY(penetrating.Rotation.Y) * Matrix4.CreateRotationZ(penetrating.Rotation.Z);
            Vector3 adjustment = penetrating.Position - point; // Repeated calcualtion used to evaluate distance

            // Calcualte the distance of each face in the mesh
            foreach (Face f in penetrating.Faces)
            {
                Vector3 p = f.MidPoint;
                Vector3 transP = Vector3.Transform(p, transformation);
                float dist = (transP + adjustment).Length;
                if (dist < minDist)
                {
                    minDist = dist;
                    minFace = f;
                }
            }

            // Get list of verticies and include the face midpoint
            List<Vertex> verticies = new List<Vertex>(minFace.Verticies);
            verticies.Add(new Vertex(minFace.MidPoint));

            // Adjust face to be in world space
            minFace = new Face(verticies.Select(v => Vector3.Transform(v.Position, transformation) + penetrating.Position).ToArray());

            return minFace;
        }

        /// <summary>
        /// Calculates data about a contact from the box and the point relative to the box's local space.
        /// </summary>
        /// <param name="box">The box to work out the contact for.</param>
        /// <param name="relativePoint">The point to calcualte from.</param>
        /// <returns>The contact generated from the overlap.</returns>
        private Contact CalculateContact(BoundingBox box, Vector3 relativePoint)
        {
            // Check x penetration
            float minDepth = box.Extents.X - Math.Abs(relativePoint.X);
            Vector3 normal = new Vector3();
            if (minDepth < 0)
            {
                // No penetration 
                return new Contact(0, new Vector3());
            }
            else
            {
                // Get axis of penetrtion
                normal = box.Normals[0] * (relativePoint.X < 0 ? -1 : 1);
            }

            // Check y penetration
            float depth = box.Extents.Y - Math.Abs(relativePoint.Y);
            if (depth < 0)
            {
                return new Contact(0, new Vector3());
            }
            else if (depth < minDepth)
            {
                minDepth = depth;
                normal = box.Normals[1] * (relativePoint.Y < 0 ? -1 : 1);
            }

            // Check z penetration
            depth = box.Extents.Z - Math.Abs(relativePoint.Z);
            if (depth < 0)
            {
                return new Contact(0, new Vector3());
            }
            else if (depth < minDepth)
            {
                minDepth = depth;
                normal = box.Normals[2] * (relativePoint.Z < 0 ? -1 : 1);
            }

            return new Contact(minDepth, normal);
        }
    }
}
