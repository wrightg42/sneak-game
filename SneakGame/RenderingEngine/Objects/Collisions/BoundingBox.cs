﻿using System.Collections.Generic;
using System.Linq;
using OpenTK;

namespace RenderingEngine.Objects.Collisions
{
    /// <summary>
    /// A base class for bounding boxes.
    /// </summary>
    public abstract class BoundingBox
    {
        /// <summary>
        /// Gets or sets the center of the bounding box.
        /// </summary>
        public Vector3 Center { get; set; }

        /// <summary>
        /// Gets or sets the half extents of each axis inside the bounding box.
        /// </summary>
        public Vector3 Extents { get; set; }

        /// <summary>
        /// A private store of the orientation of the mesh.
        /// </summary>
        private Vector3 _orientation;

        /// <summary>
        /// Gets The orientation of the bounding box.
        /// </summary>
        public virtual Vector3 Orientation
        {
            get
            {
                return _orientation;
            }
            set
            {
                _orientation = value;
                UpdatedNormals = false;
            }
        }

        /// <summary>
        /// Gets or sets the main normals of the bounding box.
        /// </summary>
        protected internal Vector3[] Normals { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the stored normals are updated and relavent.
        /// </summary>
        protected internal bool UpdatedNormals { get; set; }

        /// <summary>
        /// Gets a list of verticies representing the corners of the bounding box.
        /// </summary>
        /// <remarks>The format of the list is such that right = 1, top = 2, front = 4. Add the location values together to get the index in the list.</remarks>
        public Vector3[] Verticies
        {
            get
            {
                // Create locations
                float left = -Extents.X;
                float right = Extents.X;
                float bottom = -Extents.Y;
                float top = Extents.Y;
                float back = -Extents.Z;
                float front = Extents.Z;

                // Create verticies
                Vector3 leftBottomBack = new Vector3(left, bottom, back);
                Vector3 rightBottomBack = new Vector3(right, bottom, back);
                Vector3 leftTopBack = new Vector3(left, top, back);
                Vector3 rightTopBack = new Vector3(right, top, back);
                Vector3 leftBottomFront = new Vector3(left, bottom, front);
                Vector3 rightBottomFront = new Vector3(right, bottom, front);
                Vector3 leftTopFront = new Vector3(left, top, front);
                Vector3 rightTopFront = new Vector3(right, top, front);

                // Apply rotation and translation of the box
                Matrix4 transformation = Matrix4.CreateRotationX(Orientation.X) * Matrix4.CreateRotationY(Orientation.Y) * Matrix4.CreateRotationZ(Orientation.Z) * Matrix4.CreateTranslation(Center);
                Vector3[] verticies = new Vector3[] { leftBottomBack, rightBottomBack, leftTopBack, rightTopBack, leftBottomFront, rightBottomFront, leftTopFront, rightTopFront };
                verticies = verticies.Select(v => Vector3.Transform(v, transformation)).ToArray();
                return verticies;
            }
        }

        /// <summary>
        /// Converts the current <see cref="BoundingBox"/> into a <see cref="Mesh"/>.
        /// </summary>
        /// <returns>The <see cref="Mesh"/> representing the <see cref="BoundingBox"/>.</returns>
        public Mesh ToMesh()
        {
            Vector3[] v = Verticies;

            // Create faces
            Face[] faces = new Face[6] {
                new Face(v[2], v[0], v[4], v[6]),
                new Face(v[3], v[7], v[5], v[1]),
                new Face(v[2], v[6], v[7], v[3]),
                new Face(v[0], v[1], v[5], v[4]),
                new Face(v[2], v[3], v[1], v[0]),
                new Face(v[4], v[5], v[7], v[6])};

            // Return the mesh
            return new Mesh(faces);
        }

        /// <summary>
        /// Generates a bounding box given a list of verticies to include.
        /// </summary>
        /// <param name="verticies">The verticies to include in the bounding box.</param>
        /// <remarks>The box is aligned with the global axis - make sure the verticies are also aligned this way for best fit.</remarks>
        protected void GenerateBoundingBox(IEnumerable<Vector3> verticies)
        {
            // Generate the min and max of each axis
            Vector2[] axisMinMax = new Vector2[3];
            axisMinMax[0] = GetMinMaxAlongAxis(new Vector3(1, 0, 0), verticies);
            axisMinMax[1] = GetMinMaxAlongAxis(new Vector3(0, 1, 0), verticies);
            axisMinMax[2] = GetMinMaxAlongAxis(new Vector3(0, 0, 1), verticies);

            // Convert this data into the center and extents of the bounding box
            float[] centerCoords = new float[3];
            float[] extents = new float[3];
            for (int i = 0; i < 3; i++)
            {
                centerCoords[i] = (axisMinMax[i].X + axisMinMax[i].Y) / 2;
                extents[i] = (axisMinMax[i].Y - axisMinMax[i].X) / 2;
            }

            Center = new Vector3(centerCoords[0], centerCoords[1], centerCoords[2]);
            Extents = new Vector3(extents[0], extents[1], extents[2]);
            Orientation = new Vector3();
        }

        /// <summary>
        /// Gets the normals of a bounding box.
        /// </summary>
        protected internal void GenerateNormals()
        {
            // Generate normals from mesh
            Mesh m = ToMesh();
            m.CalculateNormals();
            List<Vector3> normals = new List<Vector3>();
            for (int i = 0; i < 24; i += 8)
            {
                normals.Add(m.Normals[i]);
            }

            Normals = normals.ToArray();
            UpdatedNormals = true;
        }

        /// <summary>
        /// Gets the minimum and maximum point of a set of verticies along a specified axis.
        /// </summary>
        /// <param name="axis">The axis the set of verticies is to be projected along.</param>
        /// <param name="verticies">The set of verticies to get the minimum/maximum point of.</param>
        /// <returns>The minimum then maximum point of the verticies along the axis.</returns>
        protected static Vector2 GetMinMaxAlongAxis(Vector3 axis, IEnumerable<Vector3> verticies)
        {
            Vector2 minmax = new Vector2(float.MaxValue, float.MinValue);
            foreach (Vector3 vertex in verticies)
            {
                // Project the point along the axis
                float dotProduct = Vector3.Dot(vertex, axis);

                // Update min/max values as necessary 
                if (minmax.X > dotProduct)
                {
                    minmax.X = dotProduct;
                }
                if (minmax.Y < dotProduct)
                {
                    minmax.Y = dotProduct;
                }
            }

            return minmax;
        }

        /// <summary>
        /// Gets the minimum and maximum point of a mesh along a specified axis.
        /// </summary>
        /// <param name="axis">The axis the mesh is to be projected along.</param>
        /// <param name="m">The mesh to get the minimum/maximum point of.</param>
        /// <returns>The minimum then maximum point of the mesh along the axis.</returns>
        protected static Vector2 GetMinMaxAlongAxis(Vector3 axis, Mesh m)
        {
            Vector2 minmax = new Vector2(float.MaxValue, float.MinValue);
            foreach (Vector3 vertex in m.Verticies.Select(v => Vector3.Transform(v, m.ModelMatrix)))
            {
                float dotProduct = Vector3.Dot(vertex, axis);
                if (minmax.X > dotProduct)
                {
                    minmax.X = dotProduct;
                }
                if (minmax.Y < dotProduct)
                {
                    minmax.Y = dotProduct;
                }
            }

            return minmax;
        }
    }
}
