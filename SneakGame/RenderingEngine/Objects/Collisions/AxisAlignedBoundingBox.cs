﻿using System.Collections.Generic;
using System.Linq;
using OpenTK;

namespace RenderingEngine.Objects.Collisions
{
    /// <summary>
    /// A class representing an axis aligned bounding box.
    /// </summary>
    public class AxisAlignedBoundingBox : BoundingBox
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AxisAlignedBoundingBox"/> class.
        /// </summary>
        protected AxisAlignedBoundingBox()
        {
            Center = new Vector3();
            Extents = new Vector3();
            Orientation = new Vector3();
            Normals = new Vector3[0];
            UpdatedNormals = false;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AxisAlignedBoundingBox"/> class.
        /// </summary>
        /// <param name="mesh">The mesh to create a bounding box for.</param>
        public AxisAlignedBoundingBox(Mesh mesh)
            : this()
        {
            // Get list of the mesh's verticies and apply the object's transformation
            List<Vector3> verticies = mesh.Verticies.ToList();
            verticies = verticies.Select(v => Vector3.Transform(v, mesh.ModelMatrix)).ToList();

            // Calculate center and extents of the bounding box
            GenerateBoundingBox(verticies);
        }

        /// <summary>
        /// Gets The orientation of the bounding box.
        /// </summary>
        public override Vector3 Orientation
        {
            get
            {
                return new Vector3();
            }
        }

        /// <summary>
        /// Check if two axis aligned bounding boxes have collided.
        /// </summary>
        /// <param name="box">The box to check if there has been a collision with.</param>
        /// <returns>Whether there was a collision or not.</returns>
        public virtual bool Collision(AxisAlignedBoundingBox box)
        {
            // Check x overlap
            if (!(Center.X + Extents.X <= box.Center.X - box.Extents.X || Center.X - Extents.X >= box.Center.X + box.Extents.X))
            {
                // Check y overlap
                if (!(Center.Y + Extents.Y <= box.Center.Y - box.Extents.Y || Center.Y - Extents.Y >= box.Center.Y + box.Extents.Y))
                {
                    // Check z overlap
                    if (!(Center.Z + Extents.Z <= box.Center.Z - box.Extents.Z || Center.Z - Extents.Z >= box.Center.Z + box.Extents.Z))
                    {
                        // Overlap on all axis so there is a collision
                        return true;
                    }
                }
            }

            return false;
        }
    }
}
