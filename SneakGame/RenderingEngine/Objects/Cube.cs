﻿using System.Collections.Generic;
using OpenTK;

namespace RenderingEngine.Objects
{
    /// <summary>
    /// A class to represent a cube.
    /// </summary>
    public class Cube : Mesh
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Cube"/> class.
        /// </summary>
        public Cube()
            : base()
        {
            // Calculate verticies
            Vertex[] verticies = new Vertex[]
            {
                new Vertex(new Vector3(-0.5f, -0.5f,  -0.5f), new Vector3(), new Vector2(),  new Vector3( 1f, 0f, 0f)),
                new Vertex(new Vector3(0.5f, -0.5f,  -0.5f), new Vector3(), new Vector2(),  new Vector3( 0f, 0f, 1f)),
                new Vertex(new Vector3(0.5f, 0.5f,  -0.5f), new Vector3(), new Vector2(),  new Vector3( 0f, 1f, 0f)),
                new Vertex(new Vector3(-0.5f, 0.5f,  -0.5f), new Vector3(), new Vector2(),  new Vector3( 1f, 0f, 0f)),
                new Vertex(new Vector3(-0.5f, -0.5f,  0.5f), new Vector3(), new Vector2(),  new Vector3( 0f, 0f, 1f)),
                new Vertex(new Vector3(0.5f, -0.5f,  0.5f), new Vector3(), new Vector2(),  new Vector3( 0f, 1f, 0f)),
                new Vertex(new Vector3(0.5f, 0.5f,  0.5f), new Vector3(), new Vector2(),  new Vector3( 1f, 0f, 0f)),
                new Vertex(new Vector3(-0.5f, 0.5f,  0.5f), new Vector3(), new Vector2(),  new Vector3( 0f, 0f, 0f)),
            };

            // Create face structure
            Faces = new List<Face>
            {
                new Face(verticies[3], verticies[2], verticies[1], verticies[0]), 
                new Face(verticies[1], verticies[2], verticies[6], verticies[5]), 
                new Face(verticies[4], verticies[5], verticies[6], verticies[7]), 
                new Face(verticies[7], verticies[6], verticies[2], verticies[3]), 
                new Face(verticies[4], verticies[7], verticies[3], verticies[0]), 
                new Face(verticies[0], verticies[1], verticies[5], verticies[4]) 
            };
            
            CalculateNormals();

            Name = "Cube";
        }
    }
}
