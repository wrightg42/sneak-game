﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using OpenTK;

namespace RenderingEngine.Objects
{
    /// <summary>
    /// A static class to load a <see cref="Mesh"/>.
    /// </summary>
    public static class ObjectLoader
    {
        /// <summary>
        /// Loads a <see cref="Mesh"/> from a file.
        /// </summary>
        /// <param name="filename">The file location.</param>
        /// <param name="materials">The dictionary to store <see cref="Material"/>s.</param>
        /// <returns>The loaded <see cref="Mesh"/>.</returns>
        public static Mesh LoadMeshFromFile(string filename, ref Dictionary<string, Material> materials)
        {
            Mesh obj = new Mesh();
            try
            {
                using (StreamReader reader = new StreamReader(filename))
                {
                    string objCode = reader.ReadToEnd();
                    ProcessMaterialRequests(objCode, ref materials);
                    obj = LoadFromString(objCode, materials);
                }
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("File not found: {0}", filename);
            }
            catch (DirectoryNotFoundException)
            {
                Console.WriteLine("File not found: {0}", filename);
            }
            catch (Exception)
            {
                Console.WriteLine("Error loading file: {0}\n", filename);
            }

            return obj;
        }

        /// <summary>
        /// Loads many <see cref="Mesh"/>es from a file.
        /// </summary>
        /// <param name="filename">The filename of the file.</param>
        /// <param name="materials">The dictionary to store <see cref="Material"/>s.</param>
        /// <returns>The loaded <see cref="Mesh"/>es.</returns>
        public static List<Mesh> LoadMeshesFromFile(string filename, ref Dictionary<string, Material> materials)
        {
            List<Mesh> objs = new List<Mesh>();
            try
            {
                using (StreamReader reader = new StreamReader(filename))
                {
                    string objCode = reader.ReadToEnd();
                    ProcessMaterialRequests(objCode, ref materials);
                    objs = LoadManyFromString(objCode, materials);
                }
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("File not found: {0}", filename);
            }
            catch (DirectoryNotFoundException)
            {
                Console.WriteLine("File not found: {0}", filename);
            }
            catch (Exception)
            {
                Console.WriteLine("Error loading file: {0}\n", filename);
            }

            return objs;
        }

        /// <summary>
        /// Loads a <see cref="Mesh"/> from a string.
        /// </summary>
        /// <param name="objCode">The string representing a mesh.</param>
        /// <param name="materials">The dictionary of loaded <see cref="Material"/>s.</param>
        /// <returns>The loaded <see cref="Mesh"/>.</returns>
        public static Mesh LoadFromString(string objCode, Dictionary<string, Material> materials)
        {
            // Split lines and make sure it is lower case
            string[] lines = objCode.Split('\n').Select((string s) => s = s.Trim()).ToArray();

            // To store model data
            List<Vector3> verticies = new List<Vector3>();
            List<Vector3> normals = new List<Vector3>();
            List<Vector2> textureCoords = new List<Vector2>();
            List<Face> faces = new List<Face>();
            string name = string.Empty;
            string materialInUse = string.Empty;

            // Add base values so we dont need to account for 1 based indexing later
            verticies.Add(new Vector3());
            textureCoords.Add(new Vector2());
            normals.Add(new Vector3());

            // Read line by line and process data
            foreach (string line in lines)
            {
                if (line.StartsWith("usemtl "))
                {
                    materialInUse = line.Substring(7).Trim();
                }
                else if (line.StartsWith("v "))
                {
                    verticies.Add(ParseVector3(line, "vertex"));
                }
                else if (line.StartsWith("vt "))
                {
                    textureCoords.Add(ParseVector2(line, "texture corordinate"));
                }
                else if (line.StartsWith("vn "))
                {
                    normals.Add(ParseVector3(line, "normal vector"));
                }
                else if (line.StartsWith("f "))
                {
                    faces.Add(ParseFace(line, verticies, normals, textureCoords));
                }
                else if (line.StartsWith("o "))
                {
                    name = line.Substring(2);
                }
            }

            // Create model
            return GenerateMesh(faces, materials, materialInUse, name, calculateNormals: (normals.Count == 1));
        }

        /// <summary>
        /// Loads multiple <see cref="Mesh"/>es from a string.
        /// </summary>
        /// <param name="objCode">The string representing a mesh.</param>
        /// <param name="materials">The dictioary of loaded <see cref="Material"/>.</param>
        /// <returns>The loaded <see cref="Mesh"/>es.</returns>
        public static List<Mesh> LoadManyFromString(string objCode, Dictionary<string, Material> materials)
        {
            // Split lines and make sure it is lower case
            string[] lines = objCode.Split('\n').Select((string s) => s = s.Trim()).ToArray();

            // To store model data
            List<Mesh> objs = new List<Mesh>();
            List<Vector3> verticies = new List<Vector3>();
            List<Vector3> normals = new List<Vector3>();
            List<Vector2> textureCoords = new List<Vector2>();
            List<Face> faces = new List<Face>();
            string name = string.Empty;
            string group = string.Empty;
            string materialInUse = string.Empty;

            // Add base values so we dont need to account for 1 based indexing later
            verticies.Add(new Vector3());
            textureCoords.Add(new Vector2());
            normals.Add(new Vector3());

            // Read line by line and process data
            foreach (string line in lines)
            {
                if (line.StartsWith("usemtl "))
                {
                    materialInUse = line.Substring(7).Trim();
                }
                else if (line.StartsWith("v "))
                {
                    verticies.Add(ParseVector3(line, "vertex"));
                }
                else if (line.StartsWith("vt "))
                {
                    textureCoords.Add(ParseVector2(line, "texture corordinate"));
                }
                else if (line.StartsWith("vn "))
                {
                    normals.Add(ParseVector3(line, "normal vector"));
                }
                else if (line.StartsWith("f "))
                {
                    faces.Add(ParseFace(line, verticies, normals, textureCoords));
                }
                else if (line.StartsWith("o ") || line.StartsWith("g "))
                {
                    // If new name was stated, previous polygon has been parsed
                    if (faces.Count > 0)
                    {
                        objs.Add(GenerateMesh(faces, materials, materialInUse, name, group, normals.Count == 1));
                    }

                    // Clear faces for next polygon
                    faces = new List<Face>();

                    // Parse new object name/group
                    if (line.StartsWith("o"))
                    {
                        name = line.Substring(2);
                        group = string.Empty;
                    }
                    else
                    {
                        group = line.Substring(2);
                    }
                }
            }

            // Create final polygon
            objs.Add(GenerateMesh(faces, materials, materialInUse, name, group, normals.Count == 1));
            return objs;
        }

        /// <summary>
        /// Generates a <see cref="Mesh"/> from given data.
        /// </summary>
        /// <param name="faces">The <see cref="Mesh"/>'s <see cref="Face"/>s.</param>
        /// <param name="materials">The dictionary of loaded <see cref="Material"/>s.</param>
        /// <param name="material">The name of the <see cref="Mesh"/>'s material.</param>
        /// <param name="name">The <see cref="Mesh"/>'s name.</param>
        /// <param name="group">The <see cref="Mesh"/>'s group name.</param>
        /// <param name="calculateNormals">Wheater the normals should be calculated on the new <see cref="Mesh"/>.</param>
        /// <returns>The generated <see cref="Mesh"/>.</returns>
        private static Mesh GenerateMesh(IEnumerable<Face> faces, Dictionary<string, Material> materials, string material, string name, string group = "", bool calculateNormals = false)
        {
            Mesh obj = new Mesh(faces);
            obj.Name = name;
            if (group != "")
            {
                obj.Name += "." + group;
            }

            if (calculateNormals)
            {
                obj.CalculateNormals();
            }

            if (material != string.Empty)
            {
                obj.Material = materials[material];
            }

            return obj;
        }

        /// <summary>
        /// Processes a material request.
        /// </summary>
        /// <param name="objCode">A string representing a file containing a <see cref="Mesh"/>.</param>
        /// <param name="materials">The <see cref="Material"/> dictionary to be populated from requests in the mesh.</param>
        private static void ProcessMaterialRequests(string objCode, ref Dictionary<string, Material> materials)
        {
            IEnumerable<string> materialLibs = objCode.Split('\n').Select((string s) => s = s.Trim()).Where((string s) => s.StartsWith("mtllib "));
            foreach (string materialLib in materialLibs)
            {
                Dictionary<string, Material> newMaterials = Material.LoadFromFile(materialLib.Substring(7).Trim());
                foreach (KeyValuePair<string, Material> newMaterial in newMaterials)
                {
                    if (!materials.ContainsKey(newMaterial.Key))
                    {
                        materials.Add(newMaterial.Key, newMaterial.Value);
                    }
                }
            }
        }

        /// <summary>
        /// Parses a line into a <see cref="Vector3"/>.
        /// </summary>
        /// <param name="line">A string representing a <see cref="Vector3"/>.</param>
        /// <param name="dataType">The origins of the data being parsed.</param>
        /// <returns>The prased <see cref="Vector3"/>.</returns>
        private static Vector3 ParseVector3(string line, string dataType = "line")
        {
            // Cut off beginning of line
            string data = line.Substring(2);

            Vector3 vector = new Vector3();

            // Check if there's enough elements for a vector 3
            if (data.Trim().Count((char c) => c == ' ') == 2) 
            {
                try
                {
                    // Parse the data if valid length
                    string[] splitData = data.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    vector = new Vector3(float.Parse(splitData[0]), float.Parse(splitData[1]), float.Parse(splitData[2]));
                }
                catch
                {
                    Console.WriteLine("Error parsing {0}: {1}", dataType, line);
                }
            }
            else
            {
                Console.WriteLine("Invalid amout of data to parse {0}: {1}", dataType, line);
            }

            return vector;
        }

        /// <summary>
        /// Parses a line into a <see cref="Vector2"/>.
        /// </summary>
        /// <param name="line">A string representing a <see cref="Vector2"/>.</param>
        /// <param name="dataType">The origins of the data being parsed.</param>
        /// <returns>The prased <see cref="Vector2"/>.</returns>
        private static Vector2 ParseVector2(string line, string dataType = "line")
        {
            // Cut off beginning of line
            string data = line.Substring(2);

            Vector2 vector = new Vector2();

            // Check if there's enough elements for a vector 2
            if (data.Trim().Count((char c) => c == ' ') == 1)
            {
                try
                {
                    // Parse the data if valid length
                    string[] splitData = data.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    vector = new Vector2(float.Parse(splitData[0]), float.Parse(splitData[1]));
                }
                catch
                {
                    Console.WriteLine("Error parsing {0}: {1}", dataType, line);
                }
            }
            else
            {
                Console.WriteLine("Invalid amout of data to parse {0}: {1}", dataType, line);
            }

            return vector;
        }

        /// <summary>
        /// Parses a line into a <see cref="Face"/>.
        /// </summary>
        /// <param name="line">A string representing a <see cref="Face"/>.</param>
        /// <param name="verticies">The collection of currently processed verticies.</param>
        /// <param name="normals">The collection of currently processed normals.</param>
        /// <param name="textureCoords">The collection of currently processed texture coordinates..</param>
        /// <returns>The prased <see cref="Face"/>.</returns>
        private static Face ParseFace(string line, IEnumerable<Vector3> verticies, IEnumerable<Vector3> normals, IEnumerable<Vector2> textureCoords)
        {
            // Cut definition of line 
            string data = line.Substring(2);

            Face face = new Face();

            // Validate enough data to parse correctly
            if (data.Trim().Count((char c) => c == ' ') >= 2)
            {
                try
                {
                    // Parse data
                    string[] splitData = data.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                    // Parse verticies
                    List<Vertex> faceVerticies = new List<Vertex>();
                    foreach (string vecData in splitData)
                    {
                        // Get refrances to lines
                        int vertexRef = int.Parse(vecData.Split('/')[0]);
                        int textureCoordRef;
                        int normalRef;
                        if (splitData[0].Count((char c) => c == '/') >= 2)
                        {
                            // Incase there was no normal/texture coords included in the definition
                            if (!int.TryParse(vecData.Split('/')[1], out textureCoordRef))
                            {
                                textureCoordRef = 0;
                            }

                            if (!int.TryParse(vecData.Split('/')[2], out normalRef))
                            {
                                normalRef = 0;
                            }
                        }
                        else
                        {
                            // If not enough data provided to generate, assume their positions are the same in each list, or there isn't any data
                            if (textureCoords.Count() > vertexRef)
                            {
                                textureCoordRef = vertexRef;
                            }
                            else
                            {
                                textureCoordRef = 0;
                            }

                            if (normals.Count() > vertexRef)
                            {
                                normalRef = vertexRef;
                            }
                            else
                            {
                                normalRef = 0;
                            }
                        }

                        faceVerticies.Add(new Vertex(verticies.ElementAt(vertexRef), normals.ElementAt(normalRef), textureCoords.ElementAt(textureCoordRef)));
                    }

                    // Generate face
                    foreach (Vertex faceVertex in faceVerticies)
                    {
                        face.Verticies.Add(faceVertex);
                    }
                }
                catch (Exception)
                {
                    Console.WriteLine("Error parsing face: {0}", line);
                }
            }
            else
            {
                Console.WriteLine("Invalid amount of data to parse face: {0}", line);
            }

            return face;
        }
    }
}
