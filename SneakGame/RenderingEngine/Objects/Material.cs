﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using OpenTK;

namespace RenderingEngine.Objects
{
    /// <summary>
    /// A class representing a material for a <see cref="Mesh"/>.
    /// </summary>
    public class Material
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Material"/> class.
        /// </summary>
        public Material()
        {
            AmbientColor = new Vector3();
            DiffuseColor = new Vector3();
            SpecularColor = new Vector3();
            SpecularExponent = 1;
            Opacity = 1;
            AmbientMap = string.Empty;
            DiffuseMap = string.Empty;
            SpecularMap = string.Empty;
            NormalMap = string.Empty;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Material"/> class.
        /// </summary>
        /// <param name="ambientColor">The ambient color of the material.</param>
        /// <param name="diffuseColor">The diffuse color of the material.</param>
        /// <param name="specularColor">The specular color of the material.</param>
        /// <param name="specularExponent">The sepcular exponent of the material.</param>
        /// <param name="opacity">The opacity of the material.</param>
        public Material(Vector3 ambientColor, Vector3 diffuseColor, Vector3 specularColor, float specularExponent = 1, float opacity = 1)
            : this()
        {
            AmbientColor = ambientColor;
            DiffuseColor = diffuseColor;
            SpecularColor = specularColor;
            SpecularExponent = specularExponent;
            Opacity = opacity;
        }

        /// <summary>
        /// The ambient color of the material.
        /// </summary>
        public Vector3 AmbientColor;

        /// <summary>
        /// The diffuse color of the material.
        /// </summary>
        public Vector3 DiffuseColor;

        /// <summary>
        /// The specular color of the material.
        /// </summary>
        public Vector3 SpecularColor;

        /// <summary>
        /// The specular exponent of the material.
        /// </summary>
        public float SpecularExponent;
        
        /// <summary>
        /// The opacity of the material.
        /// </summary>
        public float Opacity;

        /// <summary>
        /// The filename of the ambient color map.
        /// </summary>
        public string AmbientMap;

        /// <summary>
        /// The filename of the diffuse color map.
        /// </summary>
        public string DiffuseMap;

        /// <summary>
        /// The filename of the specular map.
        /// </summary>
        public string SpecularMap;

        /// <summary>
        /// The filename of the normal map.
        /// </summary>
        public string NormalMap;

        /// <summary>
        /// Loads a set of materials from a file.
        /// </summary>
        /// <param name="filename">The filename of the material file.</param>
        /// <returns>A dictionary of material names and their properties.</returns>
        public static Dictionary<string, Material> LoadFromFile(string filename)
        {
            // List of materials read
            Dictionary<string, Material> materials = new Dictionary<string, Material>();

            try
            {
                // Read over file to get data
                List<string> materialStrings = new List<string>();
                using (StreamReader sr = new StreamReader(filename))
                {
                    string currentMaterialString = string.Empty;
                    string currentLine;
                    while ((currentLine = sr.ReadLine()) != null)
                    {
                        if (!currentLine.StartsWith("newmtl"))
                        {
                            // Add new line to material string if it is not the end of the current material
                            if (currentMaterialString.StartsWith("newmtl"))
                            {
                                currentMaterialString += currentLine + "\n";
                            }
                        }
                        else
                        {
                            // If new material store the previous one and reset the read string
                            if (currentMaterialString.Length > 0)
                            {
                                materialStrings.Add(currentMaterialString);
                            }

                            currentMaterialString = currentLine + "\n";
                        }
                    }

                    // Add last material
                    materialStrings.Add(currentMaterialString);
                }

                // Parse materials
                foreach (string materialString in materialStrings)
                {
                    if (materialString.Count((char c) => c == '\n') > 0)
                    {
                        Material newMaterial = new Material();
                        string materialName = string.Empty;
                        newMaterial = LoadFromString(materialString, out materialName);
                        materials.Add(materialName, newMaterial);
                    }
                }
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("File not found: {0}", filename);
            }
            catch (DirectoryNotFoundException)
            {
                Console.WriteLine("File not found: {0}", filename);
            }
            catch (Exception)
            {
                Console.WriteLine("Error loading file: {0}", filename);
            }

            return materials;
        }

        /// <summary>
        /// Prases a string containing material data.
        /// </summary>
        /// <param name="material">The string representing a material</param>
        /// <param name="name">The name of the parsed material.</param>
        /// <returns>The parsed material.</returns>
        public static Material LoadFromString(string material, out string name)
        {
            Material output = new Material();
            name = "";

            List<string> lines = material.Split('\n').ToList();

            // Skip until the material definition starts
            lines = lines.SkipWhile(s => !s.StartsWith("newmtl ")).ToList();

            // Make sure an actual material was included
            if (lines.Count != 0)
            {
                // Get name from first line
                name = lines[0].Substring("newmtl ".Length);
            }

            // Remove leading whitespace
            lines = lines.Select((string s) => s.Trim()).ToList();

            // Read material properties
            foreach (string line in lines)
            {
                // Skip comments and blank lines
                if (line.Length < 3 || line.StartsWith("//") || line.StartsWith("#"))
                {
                    continue;
                }

                // Parse ambient color
                if (line.StartsWith("Ka"))
                {
                    string[] colorParts = line.Substring(3).Split(' ');

                    // Check that all vector fields are present
                    if (colorParts.Length < 3)
                    {
                        throw new ArgumentException("Invalid color data");
                    }

                    try
                    {
                        output.AmbientColor = new Vector3(float.Parse(colorParts[0]), float.Parse(colorParts[1]), float.Parse(colorParts[2]));
                    }
                    catch
                    {
                        Console.WriteLine("Error parsing ambient color: {0}", line);
                    }
                }

                // Parse diffuse color
                if (line.StartsWith("Kd"))
                {
                    string[] colorParts = line.Substring(3).Split(' ');

                    // Check that all vector fields are present
                    if (colorParts.Length < 3)
                    {
                        throw new ArgumentException("Invalid color data");
                    }

                    try
                    {
                        output.DiffuseColor = new Vector3(float.Parse(colorParts[0]), float.Parse(colorParts[1]), float.Parse(colorParts[2]));
                    }
                    catch
                    {
                        Console.WriteLine("Error parsing diffuse color: {0}", line);
                    }
                }

                // Parse specular color
                if (line.StartsWith("Ks"))
                {
                    string[] colorParts = line.Substring(3).Split(' ');

                    // Check that all vector fields are present
                    if (colorParts.Length < 3)
                    {
                        throw new ArgumentException("Invalid color data");
                    }

                    try
                    {
                        output.SpecularColor = new Vector3(float.Parse(colorParts[0]), float.Parse(colorParts[1]), float.Parse(colorParts[2]));
                    }
                    catch
                    {
                        Console.WriteLine("Error parsing specular color: {0}", line);
                    }
                }

                // Parse specular exponent
                if (line.StartsWith("Ns"))
                {
                    try
                    {
                        output.SpecularExponent = float.Parse(line.Substring(3));
                    }
                    catch
                    {
                        Console.WriteLine("Error parsing specular exponent: {0}", line);
                    }
                }

                // Parse opacity
                if (line.StartsWith("d"))
                {
                    try
                    {
                        output.Opacity = float.Parse(line.Substring(2).Trim());
                    }
                    catch
                    {
                        Console.WriteLine("Error parsing opacity: {0}", line);
                    }
                }

                // Parse ambient map
                if (line.StartsWith("map_Ka"))
                {
                    // Check that file name is present
                    if (line.Length >= "map_Ka".Length + 6)
                    {
                        output.AmbientMap = line.Substring("map_Ka".Length + 1);
                    }
                }

                // Parse diffuse map
                if (line.StartsWith("map_Kd"))
                {
                    // Check that file name is present
                    if (line.Length >= "map_Kd".Length + 6)
                    {
                        output.DiffuseMap = line.Substring("map_Kd".Length + 1);
                    }
                }

                // Parse specular map
                if (line.StartsWith("map_Ks"))
                {
                    // Check that file name is present
                    if (line.Length >= "map_Ks".Length + 6)
                    {
                        output.SpecularMap = line.Substring("map_Ks".Length + 1);
                    }
                }

                // Parse normal map
                if (line.StartsWith("map_bump"))
                {
                    // Check that file name is present
                    if (line.Length >= "map_bump".Length + 6)
                    {
                        output.NormalMap = line.Substring("map_bump".Length + 1);
                    }
                }
            }

            return output;
        }
    }
}
