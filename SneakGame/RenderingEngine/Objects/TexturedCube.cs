﻿using System.Collections.Generic;
using OpenTK;

namespace RenderingEngine.Objects
{
    /// <summary>
    /// A class to represent a textured cube.
    /// </summary>
    public class TexturedCube : Cube
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TexturedCube"/> class.
        /// </summary>
        public TexturedCube()
            : base()
        {
            // Calculate verticies
            Vertex[] verticies = new Vertex[]
            {
                new Vertex(new Vector3(-0.5f, -0.5f, -0.5f), new Vector3(), new Vector2(0, 0),  new Vector3(1f, 0f, 0f)),
                new Vertex(new Vector3(0.5f, 0.5f, -0.5f), new Vector3(), new Vector2(-1, 1),  new Vector3(0f, 0f, 1f)),
                new Vertex(new Vector3(0.5f, -0.5f, -0.5f), new Vector3(), new Vector2(-1, 0),  new Vector3(0f, 1f, 0f)),
                new Vertex(new Vector3(-0.5f, 0.5f, -0.5f), new Vector3(), new Vector2(0, 1),  new Vector3(1f, 0f, 0f)),
                new Vertex(new Vector3(0.5f, -0.5f, -0.5f), new Vector3(), new Vector2(0, 0),  new Vector3(0f, 0f, 1f)),
                new Vertex(new Vector3(0.5f, 0.5f, -0.5f), new Vector3(), new Vector2(0, 1),  new Vector3(0f, 1f, 0f)),
                new Vertex(new Vector3(0.5f, 0.5f, 0.5f), new Vector3(), new Vector2(-1, 1),  new Vector3(1f, 0f, 0f)),
                new Vertex(new Vector3(0.5f, -0.5f, 0.5f), new Vector3(), new Vector2(-1, 0),  new Vector3(0f, 1f, 0f)),
                new Vertex(new Vector3(-0.5f, -0.5f, 0.5f), new Vector3(), new Vector2(-1, 0),  new Vector3(0f, 0f, 1f)),
                new Vertex(new Vector3(0.5f, -0.5f, 0.5f), new Vector3(), new Vector2(0, 0),  new Vector3(0f, 1f, 0f)),
                new Vertex(new Vector3(0.5f, 0.5f, 0.5f), new Vector3(), new Vector2(0, 1),  new Vector3(1f, 0f, 0f)),
                new Vertex(new Vector3(-0.5f, 0.5f, 0.5f), new Vector3(), new Vector2(-1, 1),  new Vector3(0f, 0f, 0f)),
                new Vertex(new Vector3(-0.5f, 0.5f, -0.5f), new Vector3(), new Vector2(0, 1),  new Vector3(1f, 0f, 0f)),
                new Vertex(new Vector3(0.5f, 0.5f, -0.5f), new Vector3(), new Vector2(0, 0),  new Vector3(0f, 1f, 0f)),
                new Vertex(new Vector3(0.5f, 0.5f, 0.5f), new Vector3(), new Vector2(-1, 0),  new Vector3(1f, 0f, 0f)),
                new Vertex(new Vector3(-0.5f, 0.5f, 0.5f), new Vector3(), new Vector2(-1, 1),  new Vector3(0f, 0f, 0f)),
                new Vertex(new Vector3(-0.5f, -0.5f, -0.5f), new Vector3(), new Vector2(0, 0),  new Vector3(1f, 0f, 0f)),
                new Vertex(new Vector3(-0.5f, 0.5f, -0.5f), new Vector3(), new Vector2(0, 1),  new Vector3(1f, 0f, 0f)),
                new Vertex(new Vector3(-0.5f, 0.5f, 0.5f), new Vector3(), new Vector2(1, 1),  new Vector3(0f, 0f, 0f)),
                new Vertex(new Vector3(-0.5f, -0.5f, 0.5f), new Vector3(), new Vector2(1, 0),  new Vector3(0f, 0f, 1f)),
                new Vertex(new Vector3(-0.5f, -0.5f, -0.5f), new Vector3(), new Vector2(0, 0),  new Vector3(1f, 0f, 0f)),
                new Vertex(new Vector3(0.5f, -0.5f, -0.5f), new Vector3(), new Vector2(0, 1),  new Vector3(0f, 0f, 1f)),
                new Vertex(new Vector3(0.5f, -0.5f, 0.5f), new Vector3(), new Vector2(-1, 1),  new Vector3(0f, 1f, 0f)),
                new Vertex(new Vector3(-0.5f, -0.5f, 0.5f), new Vector3(), new Vector2(-1, 0),  new Vector3(0f, 0f, 1f))
            };

            // Create face structure
            Faces = new List<Face>
            {
                new Face(verticies[3], verticies[1], verticies[2], verticies[0]), // back 
                new Face(verticies[4], verticies[5], verticies[6], verticies[7]), // right
                new Face(verticies[8], verticies[9], verticies[10], verticies[11]), // front
                new Face(verticies[15], verticies[14], verticies[13], verticies[12]), // top
                new Face(verticies[19], verticies[18], verticies[17], verticies[16]), // left 
                new Face(verticies[20], verticies[21], verticies[22], verticies[23]) // bottom 
            };

            CalculateNormals();

            Name = "TexturedCube";
        }
    }
}
