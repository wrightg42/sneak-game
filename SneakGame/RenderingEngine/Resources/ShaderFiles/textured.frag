﻿#version 130

in vec2 f_texcoord;
out vec4 outputColor;

uniform sampler2D mainTexture;

void main()
{
	outputColor = texture2D(mainTexture, f_texcoord);
}