﻿#version 130

in vec3 vPosition;
in vec3 vNormal;
in vec2 texcoord;

out vec3 v_norm;
out vec3 v_pos;
out vec2 f_texcoord;

uniform mat4 modelview;
uniform mat4 model;
uniform mat4 view;
uniform mat3 normalmatrix;

void main()
{
	// Output position of vertex
	gl_Position = modelview * vec4(vPosition, 1.0);

	// Position of vertex in worldspace
	v_pos = (model * vec4(vPosition, 1.0)).xyz;

	// Texture coord stays same
	f_texcoord = texcoord;

	// Calculate normal of the model
	v_norm = normalmatrix * vNormal;
}