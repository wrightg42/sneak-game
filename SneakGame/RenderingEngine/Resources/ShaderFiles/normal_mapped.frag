﻿#version 130

// Holds light info
struct Light 
{
	vec3 Position;
	vec3 Color;
	float AmbientIntensity;
	float DiffuseIntensity;

	int Type;
	vec3 Direction;
	float ConeAngle;

	float LinearAttenuation;
	float QuadraticAttenuation;
};

// Input and output data from shader
in vec3 v_norm;
in vec3 v_pos;
in vec2 f_texcoord;
out vec4 outputColor;

// Texture information
uniform sampler2D maintexture;
uniform bool material_hasAmbientMap;
uniform sampler2D material_ambientMap;
uniform bool material_hasDiffuseMap;
uniform sampler2D material_diffuseMap;
uniform bool material_hasSpecularMap;
uniform sampler2D material_specularMap;
uniform bool material_hasNormalMap;
uniform sampler2D material_normalMap;

uniform mat4 invertedview;

// Material information
uniform vec3 material_ambient;
uniform vec3 material_diffuse;
uniform vec3 material_specular;
uniform float material_specExponent;
uniform float material_opacity;

// Array of lights used in the shader
const int MAX_LIGHTS = 50;
uniform Light lights[MAX_LIGHTS];

void main()
{	
	// Texture information
	vec4 texcolor = texture2D(maintexture, f_texcoord);
	outputColor = vec4(0, 0, 0, material_opacity);

	// Normalize the normal vector
	vec3 n = normalize(v_norm);
	if (material_hasNormalMap)
	{
		n = normalize(texture2D(material_normalMap, f_texcoord).rgb * 2 - 1);
	}

	// Recalculate material properties if map provided
	vec3 ambientColor = material_ambient;
	vec3 diffuseColor = material_diffuse;
	if (material_hasAmbientMap)
	{
		ambientColor = ambientColor * texture2D(material_ambientMap, f_texcoord).rgb;
	}

	if (material_hasDiffuseMap)
	{
		diffuseColor = diffuseColor * texture2D(material_diffuseMap, f_texcoord).rgb;
	}

	// Calculate view vector
	vec3 viewvec = normalize(vec3(invertedview * vec4(0, 0, 0, 1)) - v_pos);  

	// Loop through lights, adding their lighting to the shader data
	for (int i = 0; i < MAX_LIGHTS; i++)
	{
		// Skip lights with 0 affect
		if (lights[i].Color == vec3(0, 0, 0))
		{
			continue;
		}

		vec3 lightvec = normalize(lights[i].Position - v_pos);
		vec4 lightcolor = vec4(0, 0, 0, material_opacity);

		// Check spotlight angle
		bool inCone = false;
		if (lights[i].Type == 1 && acos(dot(lightvec, lights[i].Direction)) < lights[i].ConeAngle)
		{
			inCone = true;
		}

		// Directional lighting
		if (lights[i].Type == 2)
		{
			lightvec = lights[i].Direction;
		}

		// Colors
		vec4 light_ambient = lights[i].AmbientIntensity * vec4(lights[i].Color, 0.0);
		vec4 light_diffuse = lights[i].DiffuseIntensity * vec4(lights[i].Color, 0.0);

		// Ambient lighting
		lightcolor = lightcolor + texcolor * light_ambient * vec4(ambientColor, 0.0);

		// Diffuse lighting
		float lambertmaterial_diffuse = max(dot(n, lightvec), 0.0);

		// Spotlight, limit light to specific angle
		if (lights[i].Type != 1 || inCone)
		{
			lightcolor = lightcolor + (texcolor * light_diffuse * vec4(diffuseColor, 0.0)) * lambertmaterial_diffuse;
		}

		// Specular lighting
		vec3 reflectionvec = normalize(reflect(-lightvec, v_norm));
		float material_specularreflection = max(dot(v_norm, lightvec), 0.0) * pow(max(dot(reflectionvec, viewvec), 0.0), material_specExponent);

		// Specular map
		if (material_hasSpecularMap)
		{
			material_specularreflection = material_specularreflection * texture2D(material_specularMap, f_texcoord).r;
		}

		// Spotlight, specular reflections are also limited by angle
		if (lights[i].Type != 1 || inCone)
		{
			lightcolor = lightcolor + vec4(material_specular * lights[i].Color, 0.0) * material_specularreflection;
		}

		// Attenuation
		float distancefactor = distance(lights[i].Position, v_pos);
		float attenuation = 1 / (1 + (distancefactor * lights[i].LinearAttenuation) + (distancefactor * distancefactor * lights[i].QuadraticAttenuation));
		outputColor = outputColor + lightcolor * attenuation;
	}
}