﻿#version 130

// Holds light info
struct Light 
{
	vec3 Position;
	vec3 Color;
	float AmbientIntensity;
	float DiffuseIntensity;
};

// Input and output data from shader
in vec3 v_norm;
in vec3 v_pos;
in vec2 f_texcoord;
out vec4 outputColor;

// Texture information
uniform sampler2D maintexture;
uniform bool hasSpecularMap;
uniform sampler2D map_specular;

uniform mat4 invertedview;

// Material information
uniform vec3 material_ambient;
uniform vec3 material_diffuse;
uniform vec3 material_specular;
uniform float material_specExponent;
uniform float material_opacity;

// Array of lights used in the shader
const int MAX_LIGHTS = 50;
uniform Light lights[MAX_LIGHTS];

void main()
{	
	outputColor = vec4(0, 0, 0, material_opacity);

	// Texture information
	vec4 texcolor = texture2D(maintexture, f_texcoord);

	// Normalize the normal vector
	vec3 n = normalize(v_norm);

	// Calculate view vector
	vec3 viewvec = normalize(vec3(invertedview * vec4(0, 0, 0, 1)) - v_pos); 

	// Loop through lights, adding their lighting to the shader data
	for (int i = 0; i < MAX_LIGHTS; i++)
	{
		// Skip lights with 0 affect
		if (lights[i].Color == vec3(0, 0, 0))
		{
			continue;
		}

		vec3 lightvec = normalize(lights[i].Position - v_pos);

		// Colors
		vec4 light_ambient = lights[i].AmbientIntensity * vec4(lights[i].Color, 0.0);
		vec4 light_diffuse = lights[i].DiffuseIntensity * vec4(lights[i].Color, 0.0);

		// Ambient lighting
		outputColor = outputColor + texcolor  * light_ambient * vec4(material_ambient, 0.0);

		// Diffuse lighting
		float lambertmaterial_diffuse = max(dot(n, lightvec), 0.0);

		outputColor = outputColor + (light_diffuse * texcolor * vec4(material_diffuse, 0.0)) * lambertmaterial_diffuse;

		// Specular lighting
		vec3 reflectionvec = normalize(reflect(-lightvec, v_norm));
		float material_specularreflection = max(dot(v_norm, lightvec), 0.0) * pow(max(dot(reflectionvec, viewvec), 0.0), material_specExponent);

		outputColor = outputColor + vec4(material_specular * lights[i].Color, 0.0) * material_specularreflection;
	}
}