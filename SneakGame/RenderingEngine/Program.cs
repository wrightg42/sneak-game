﻿using System;

namespace RenderingEngine
{
    /// <summary>
    /// An application entry point.
    /// </summary>
    public class Program
    {
        /// <summary>
        /// The main entry point for the program.
        /// </summary>
        /// <param name="args">Any arguments the program is run with.</param>
        public static void Main(string[] args)
        {
            using (GameWindow g = new GameWindow("Rendering Prototype", 1024, 768))
            {
                if (g.Supported)
                {
                    g.Run();
                }
                else
                {
                    Console.Write("OpenGL version is not up to date enough to support this application.\nTry updating your graphics card.\nPress any key to continue...");
                    Console.ReadKey(true);
                }
            }
        }
    }
}
