﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Windows.Forms;
using OpenTK;
using Game.Items;
using Game.Items.Enemies;
using Game.Items.Pickups;
using Game.Items.Walls;
using Game.Loader;
using Game.Loader.Intermediary;
using Networking.Messages;
using Networking.Sockets;
using Networking.Sockets.Events;
using RenderingEngine;
using RenderingEngine.Objects;

namespace Game
{
    /// <summary>
    /// A class to create an instance of the game.
    /// </summary>
    public class Game : RenderingEngine.GameWindow
    {
        /// <summary>
        /// The minimum map scale for the map view of the level.
        /// </summary>
        private const float MinMapScale = 10;

        /// <summary>
        /// The maximum map scale for the map view of the level.
        /// </summary>
        private const float MaxMapScale = 100;

        /// <summary>
        /// Initializes a new instance of the <see cref="Game"/> class.
        /// </summary>
        public Game() 
            : base()
        {
            RemoteIP = null;
            LevelFilepath = string.Empty;
            OptionsFilepath = string.Empty;
            MapPlayer = false;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Game"/> class.
        /// </summary>
        /// <param name="title">The title of the game window.</param>
        /// <param name="levelFilepath">The filepath of the level to load.</param>
        /// <param name="mapPlayer">Whether the game is from the perspective of the map player.</param>
        /// <param name="fullscreen">Wheater the game should be loaded in fullscreen mode.</param>
        public Game(string title, string levelFilepath, bool mapPlayer, bool fullscreen = false)
            : base(title, fullscreen)
        {
            LevelFilepath = levelFilepath;
            OptionsFilepath = string.Empty;
            MapPlayer = mapPlayer;
            CreateSocket();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Game"/> class.
        /// </summary>
        /// <param name="title">The title of the game window.</param>
        /// <param name="width">The width of the game window.</param>
        /// <param name="height">The height of the game window.</param>
        /// <param name="levelFilepath">The filepath of the level to load.</param>
        /// <param name="mapPlayer">Whether the game is from the perspective of the map player.</param>
        /// <param name="fullscreen">Wheater the game should be loaded in fullscreen mode.</param>
        public Game(string title, int width, int height, string levelFilepath, bool mapPlayer, bool fullscreen = false)
            : base(title, width, height, fullscreen)
        {
            LevelFilepath = levelFilepath;
            OptionsFilepath = string.Empty;
            MapPlayer = mapPlayer;
            CreateSocket();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Game"/> class.
        /// </summary>
        /// <param name="title">The title of the game window.</param>
        /// <param name="levelFilepath">The filepath of the level to load.</param>
        /// <param name="optionFilepath">The filepath of the options to load.</param>
        /// <param name="mapPlayer">Whether the game is from the perspective of the map player.</param>
        /// <param name="fullscreen">Wheater the game should be loaded in fullscreen mode.</param>
        public Game(string title, string levelFilepath, string optionFilepath, bool mapPlayer, bool fullscreen = false)
            : base(title, fullscreen)
        {
            LevelFilepath = levelFilepath;
            OptionsFilepath = optionFilepath;
            MapPlayer = mapPlayer;
            CreateSocket();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Game"/> class.
        /// </summary>
        /// <param name="title">The title of the game window.</param>
        /// <param name="width">The width of the game window.</param>
        /// <param name="height">The height of the game window.</param>
        /// <param name="levelFilepath">The filepath of the level to load.</param>
        /// <param name="optionFilepath">The filepath of the options to load.</param>
        /// <param name="mapPlayer">Whether the game is from the perspective of the map player.</param>
        /// <param name="fullscreen">Wheater the game should be loaded in fullscreen mode.</param>
        public Game(string title, int width, int height, string levelFilepath, string optionFilepath, bool mapPlayer, bool fullscreen = false)
            : base(title, width, height, fullscreen)
        {
            LevelFilepath = levelFilepath;
            OptionsFilepath = optionFilepath;
            MapPlayer = mapPlayer;
            CreateSocket();
        }

        /// <summary>
        /// Gets the maximum amount of lights used by the program.
        /// </summary>
        protected override int MAX_LIGHTS
        {
            get
            {
                return 10;
            }
        }

        /// <summary>
        /// The player in the game.
        /// </summary>
        public Player Player;

        /// <summary>
        /// The walls in the game.
        /// </summary>
        public List<Wall> Walls;

        /// <summary>
        /// The items to be picked up within the game.
        /// </summary>
        public List<Pickup> Pickups;

        /// <summary>
        /// The enemy units in the game.
        /// </summary>
        public List<Enemy> Enemies;

        /// <summary>
        /// The filepath of the level to laod.
        /// </summary>
        public string LevelFilepath;

        /// <summary>
        /// The filepath of the options to load.
        /// </summary>
        public string OptionsFilepath;

        /// <summary>
        /// The rotation speed of the camera.
        /// </summary>
        public float RotationSensitivity;

        /// <summary>
        /// The input dictionary between actions and keys.
        /// </summary>
        public Dictionary<Inputs, OpenTK.Input.Key> InputKeys;

        /// <summary>
        /// If the game is currently paused (for development uses).
        /// </summary>
        private bool Paused;

        /// <summary>
        /// The time that the game was paused (so that the time can be reset accurately).
        /// </summary>
        private float PausedTime;

        /// <summary>
        /// The time of the previous frame.
        /// </summary>
        private float PreviousFrameTime;

        /// <summary>
        /// The time elapsed since the previous frame.
        /// </summary>
        private float TimeElapsed;

        /// <summary>
        /// Whether the game is from the perspective of the map player.
        /// </summary>
        private bool MapPlayer;

        /// <summary>
        /// The mesh for the camera to follow when in map mode.
        /// </summary>
        private Mesh MapPositionMesh;

        /// <summary>
        /// The scale of the map's view.
        /// </summary>
        private float Mapscale;

        /// <summary>
        /// Whether the game is in a developer environment.
        /// </summary>
        private bool Developer;

        /// <summary>
        /// Whether the game has ended.
        /// </summary>
        private bool GameEnded;

        /// <summary>
        /// The IP address of the other player.
        /// </summary>
        private IPAddress RemoteIP;

        /// <summary>
        /// The game socket to communicate game information via.
        /// </summary>
        private TCP GameSocket;

        /// <summary>
        /// The previous frame's movement input.
        /// </summary>
        private MovementCodes PreviousMovement;

        /// <summary>
        /// Connects the game socket to a remote IP address.
        /// </summary>
        /// <param name="remoteIP">The remote IP to connect to.</param>
        public void Connect(IPAddress remoteIP)
        {
            RemoteIP = remoteIP;
        }

        /// <summary>
        /// Creates the game socket to transfer data.
        /// </summary>
        private void CreateSocket()
        {
            GameSocket = new TCP((int)Ports.GamePort);
            GameSocket.Connected += Connected;
            GameSocket.MessageReceived += ParseMessage;
            GameSocket.Disconnected += Disconnected;
            if (RemoteIP != null)
            {
                Connect(RemoteIP);
            }
        }

        /// <summary>
        /// Closes the game socket.
        /// </summary>
        public void CloseSocket()
        {
            if (GameSocket != null)
            {
                GameSocket.Close();
            }
        }

        /// <summary>
        /// Handles the disposing of the game window.
        /// </summary>
        /// <param name="e">The event arguments.</param>
        protected override void OnClosed(EventArgs e)
        {
            CloseSocket();
            base.OnClosed(e);
        }

        /// <summary>
        /// Initializes all program variables.
        /// </summary>
        protected override void InitializeVariables()
        {
            OptionLoader.Load(OptionsFilepath, out RotationSensitivity, out InputKeys);
            ClearColor = Color.White;
            Walls = new List<Wall>();
            Pickups = new List<Pickup>();
            Enemies = new List<Enemy>();
            Paused = false;
            PausedTime = 0;
            PreviousFrameTime = 0;
            Developer = false;
            GameEnded = false;
            if (GameSocket != null && !GameSocket.IsConnected)
            {
                Paused = true;
            }

            // Create correct camera perspective
            if (MapPlayer)
            {
                Mapscale = 40;
                Camera = new RenderingEngine.Camera(Matrix4.CreateOrthographic(Mapscale, Mapscale, -400, 400), new Vector3(0, 0, 0), new Vector3(0, 0, (float)(Math.PI)));
                MapPositionMesh = new Mesh();
                Camera.BeginLookat(MapPositionMesh);
                Camera.BeginFollowing(MapPositionMesh, new Vector3(0, 10, 0.01f));
                ClearColor = Color.Black;
                CenterMouse = false;
            }
            else
            {
                Camera = new RenderingEngine.Camera(Matrix4.CreatePerspectiveFieldOfView(1.3f, ClientSize.Width / ClientSize.Height, 0.001f, 40), new Vector3(0, 0, 3), new Vector3());
                CursorVisible = false;
            }

            Camera.RotationSensitivity = RotationSensitivity;
        }

        /// <summary>
        /// Initializes the openGL data and shaders.
        /// </summary>
        protected override void InitializeGL()
        {
            base.InitializeGL();
        }

        /// <summary>
        /// Initializes resources for the game.
        /// </summary>
        protected override void InitResoruces()
        {
            // Initialize game assets
            Directory.SetCurrentDirectory("Resources\\Assets\\");
            Player.InitPlayerMesh(ref Materials);
            Wall.InitWallMesh(ref Materials);
            Pickup.InitPickupMesh(ref Materials);
            Tripwire.InitTripwireMesh(ref Materials);
            ViewingEnemy.InitSightMesh(ref Materials);
            Items.Enemies.Camera.InitCameraMesh(ref Materials);
            Robot.InitRobotMesh(ref Materials);

            // Get textures
            LoadMaterialTextures();

            // Reset Current Working Directory to avoid loading errors in future
            Directory.SetCurrentDirectory("..\\..\\");

            // Alter game colours for player type
            if (MapPlayer)
            {
                Pickup.TurnOnYAxis();
            }
        }

        /// <summary>
        /// Sets up the scene for the game.
        /// </summary>
        protected override void SetupScene()
        {
            // Load level and bind camera to player if needed
            LevelLoader.Load(LevelFilepath, out Player, out Walls, out Pickups, out Enemies);
            if (!MapPlayer)
            {
                Player.BindCamera(ref Camera);
            }
            else
            {
                MapPositionMesh.Position = Player.Position;
            }

            // Apply to pickup events
            foreach (Pickup p in Pickups)
            {
                p.PickedUp += HandlePickedUp;
            }

            BindGameObjects();

            // Set up sunlight
            Light sun = new Light(new Vector3(0, 20, 0), new Vector3(0.7f, 0.7f, 0.7f));
            sun.Type = LightType.Directional;
            sun.Direction = (sun.Position).Normalized();
            Lights.Add(sun);
        }

        /// <summary>
        /// Finishes up the setup for the game.
        /// </summary>
        protected override void Finalizer()
        {
            if (RemoteIP != null)
            {
                GameSocket.Connect(new IPEndPoint(RemoteIP, (int)Ports.GamePort));
            }
        }

        /// <summary>
        /// Updates the game window.
        /// </summary>
        protected override void UpdateGame()
        {
            if (!Paused)
            {
                TimeElapsed = Time - PreviousFrameTime;
                PreviousFrameTime = Time;
                UpdateEnemies();
                Player.Move(TimeElapsed, ref Walls, ref Pickups, ref Enemies);
            }
        }

        /// <summary>
        /// Processes mouse movements.
        /// </summary>
        /// <param name="movement">A <see cref="Vector2"/> representing the mouse movement.</param>
        protected override void ProcessMouseMove(Vector2 movement)
        {
            // Only rotate camera if not on the map player
            if (!MapPlayer)
            {
                Camera.Rotate(movement.X, movement.Y);
                if (Camera.ObjectFollowing == Player)
                {
                    Player.Turn(movement.X);
                }

                SendTurnData();
            }
        }

        /// <summary>
        /// Processes the current keyboard state.
        /// </summary>
        protected override void ProcessKeyboard()
        {
            if (!Paused)
            {
                // Player move inputs
                float forward = 0;
                float right = 0;
                if (KeyboardState[InputKeys[Inputs.Forward]])
                {
                    forward++;
                }
                if (KeyboardState[InputKeys[Inputs.Back]])
                {
                    forward--;
                }
                if (KeyboardState[InputKeys[Inputs.Right]])
                {
                    right++;
                }
                if (KeyboardState[InputKeys[Inputs.Left]])
                {
                    right--;
                }
    
                if (MapPlayer)
                {
                    MapPositionMesh.Position += new Vector3(-right / 3, 0, forward / 3);

                    // Adjust zoom if needed
                    float zoom = 0;
                    if (KeyboardState[InputKeys[Inputs.ZoomIn]])
                    {
                        zoom++;
                    }
                    if (KeyboardState[InputKeys[Inputs.ZoomOut]])
                    {
                        zoom--;
                    }

                    Mapscale -= zoom / 2;
                    Mapscale = Math.Max(MinMapScale, Math.Min(MaxMapScale, Mapscale));

                    Camera.FieldOfViewMatrix = Matrix4.CreateOrthographic(Mapscale, Mapscale, -400, 400);
                }
                else
                {
                    Player.SetMoveDirection(forward, right);
                    SendMoveData(forward, right);
                }
            }

            // Developer options inputs
            if (Developer && (KeyboardState[OpenTK.Input.Key.RControl] || KeyboardState[OpenTK.Input.Key.LControl]))
            {
                // Pause game
                if (KeyboardState[OpenTK.Input.Key.P])
                {
                    if (!Paused)
                    {
                        PausedTime = Time;
                        Paused = true;
                    }
                }
                else if (KeyboardState[OpenTK.Input.Key.O])
                {
                    if (Paused)
                    {
                        BeginTime += new TimeSpan((int)((Time - PausedTime) * Math.Pow(10, 7)));
                        Paused = false;
                    }
                }

                // Binding and rebinding camera to player
                if (!MapPlayer)
                {
                    if (KeyboardState[OpenTK.Input.Key.BackSpace])
                    {
                        Camera.StopFollowing();
                    }
                    else if (KeyboardState[OpenTK.Input.Key.Enter])
                    {
                        Player.BindCamera(ref Camera);
                    }
                }
            }

            // Move unbound camera
            if (!MapPlayer && Camera.ObjectFollowing == null)
            {
                if (KeyboardState[OpenTK.Input.Key.Up])
                {
                    Camera.Move(0f, 0f, 0.1f);
                }
                if (KeyboardState[OpenTK.Input.Key.Left])
                {
                    Camera.Move(-0.1f, 0f, 0f);
                }
                if (KeyboardState[OpenTK.Input.Key.Down])
                {
                    Camera.Move(0f, 0f, -0.1f);
                }
                if (KeyboardState[OpenTK.Input.Key.Right])
                {
                    Camera.Move(0.1f, 0f, 0f);
                }
            }
        }

        /// <summary>
        /// Handles a pickup being used.
        /// </summary>
        /// <param name="sender">The origin of the event.</param>
        /// <param name="e">The event arguments.</param>
        private void HandlePickedUp(object sender, EventArgs e)
        {
            // Send position so map updates
            if (!MapPlayer)
            {
                SendPositionData();
                SendTurnData();

                // End game if exit picked up
                if (sender is Exit)
                {
                    GameWon();
                }
            }
        }

        /// <summary>
        /// Updates the enemies in the game and check if the player has been detected.
        /// </summary>
        private void UpdateEnemies()
        {
            foreach (Enemy e in Enemies)
            {
                // Move the enemy and update detection levels
                e.Move(Time);
                e.UpdatePlayerDetection(Player, Walls, TimeElapsed);

                if (e.PlayerDetected())
                {
                    // Player has been detected, end the game with a fail
                    if (!MapPlayer)
                    {
                        GameLost();
                    }
                }
            }
        }

        /// <summary>
        /// Binds all the game objects to ensure they are rendered.
        /// </summary>
        private void BindGameObjects()
        {
            // Bind render objects
            List<Mesh> player = new List<Mesh>() { Player };
            if (!MapPlayer)
            {
                // Don't bind the tripwires - makes them invisible to the player
                IEnumerable<Mesh> enemies = Enemies.Where(e => !(e is Tripwire));
                BindObjects(player, Walls, Pickups, enemies);
            }
            else
            {
                // Bind all enemies and the viewing enemies' sights
                List<Mesh> sights = new List<Mesh>();
                foreach (Enemy e in Enemies)
                {
                    if (e is ViewingEnemy)
                    {
                        sights.Add((e as ViewingEnemy).Sight);
                    }
                }

                BindObjects(player, Enemies, Walls, Pickups, sights);
            }
        }

        /// <summary>
        /// Parses a received message.
        /// </summary>
        /// <param name="sender">The origin of the event.</param>
        /// <param name="e">The event arguments.</param>
        private void ParseMessage(object sender, DataReceivedEventArgs e)
        {
            // Get code
            Codes c = (Codes)e.Message.Code;

            // Parse message according to code.
            if (c == Codes.Movement)
            {
                ParseMovementData(e.Message);
            }
            else if (c == Codes.Turn)
            {
                ParseTurnData(e.Message);
            }
            else if (c == Codes.Position)
            {
                ParsePositionData(e.Message);
            }
            else if (c == Codes.Won)
            {
                GameWon();
            }
            else if (c == Codes.Lost)
            {
                GameLost();
            }
        }

        /// <summary>
        /// Sends movement data.
        /// </summary>
        /// <param name="forward">The amount the player moves forward.</param>
        /// <param name="right">The amount the player moves right.</param>
        private void SendMoveData(float forward, float right)
        {
            if (GameSocket.IsConnected && !MapPlayer)
            {
                // Get updated movement code
                MovementCodes currentMove = MovementCodes.None;
                if (forward == 1)
                {
                    currentMove |= MovementCodes.Forward;
                }
                else if (forward == -1)
                {
                    currentMove |= MovementCodes.Back;
                }

                if (right == 1)
                {
                    currentMove |= MovementCodes.Right;
                }
                else if (right == -1)
                {
                    currentMove |= MovementCodes.Left;
                }

                // Check if movement has changed
                if (currentMove != PreviousMovement)
                {
                    Networking.Messages.Message m = new Networking.Messages.Message((int)Codes.Movement, new byte[] { (byte)currentMove });
                    GameSocket.Send(m);
                    SendPositionData(); // Keep map's position up to date
                    SendTurnData();
                }

                PreviousMovement = currentMove;
            }
        }

        /// <summary>
        /// Parses a received movement command.
        /// </summary>
        /// <param name="receivedData">The received data.</param>
        private void ParseMovementData(Networking.Messages.Message receivedData)
        {
            // Get movement
            MovementCodes move = (MovementCodes)receivedData.Data[0];

            // Evaluate right/forward length
            float forward = 0;
            float right = 0;
            if ((move & MovementCodes.Forward) == MovementCodes.Forward)
            {
                forward++;
            }
            else if ((move & MovementCodes.Back) == MovementCodes.Back)
            {
                forward--;
            }

            if ((move & MovementCodes.Right) == MovementCodes.Right)
            {
                right++;
            }
            else if ((move & MovementCodes.Left) == MovementCodes.Left)
            {
                right--;
            }

            Player.SetMoveDirection(forward, right);
        }

        /// <summary>
        /// Sends current turn data.
        /// </summary>
        private void SendTurnData()
        {
            if (GameSocket.IsConnected && !MapPlayer)
            {
                // Create a message with the turn data
                byte[] data = BitConverter.GetBytes(Player.Rotation.Y);
                Networking.Messages.Message m = new Networking.Messages.Message((int)Codes.Turn, data);

                // Send data
                GameSocket.Send(m);
            }
        }

        /// <summary>
        /// Parses a received turn command.
        /// </summary>
        /// <param name="receivedData">The received data.</param>
        private void ParseTurnData(Networking.Messages.Message receivedData)
        {
            float turn = BitConverter.ToSingle(receivedData.Data, 0);
            Player.Rotation = new Vector3(Player.Rotation.X, turn, Player.Rotation.Z);
        }

        /// <summary>
        /// Sends the current position data.
        /// </summary>
        /// <remarks>https://stackoverflow.com/questions/4635769/how-do-i-convert-an-array-of-floats-to-a-byte-and-back</remarks>
        private void SendPositionData()
        {
            if (GameSocket.IsConnected && !MapPlayer)
            {
                // Create a message with the position data
                byte[] x = BitConverter.GetBytes(Player.Position.X);
                byte[] z = BitConverter.GetBytes(Player.Position.Z);
                byte[] data = x.Concat(z).ToArray();
                Networking.Messages.Message m = new Networking.Messages.Message((int)Codes.Position, data);

                // Send data
                GameSocket.Send(m);
            }
        }

        /// <summary>
        /// Parses a received position command.
        /// </summary>
        /// <param name="receivedData">The received data.</param>
        private void ParsePositionData(Networking.Messages.Message receivedData)
        {
            // Copy xz coordinate
            float x = BitConverter.ToSingle(receivedData.Data, 0);
            float z = BitConverter.ToSingle(receivedData.Data, 4);

            // Reset position
            Player.Position = new Vector3(x, Player.Position.Y, z);
        }

        /// <summary>
        /// Runs code for when the game is lost.
        /// </summary>
        private void GameLost()
        {
            if (!MapPlayer)
            {
                // Send lost message if in control
                if (!MapPlayer)
                {
                    SendMoveData(0, 0);
                }
                Networking.Messages.Message m = new Networking.Messages.Message((int)Codes.Lost, new byte[] { 0 });
                GameSocket.Send(m);
            }
            GameEnded = true;

            if (!IsExiting)
            {
                MessageBox.Show("Player has been detected. Game over!");
                Close();
            }
        }

        /// <summary>
        /// Runs code for when the game is won.
        /// </summary>
        private void GameWon()
        {
            if (!MapPlayer)
            {
                // Send won message if in control
                if (!MapPlayer)
                {
                    SendMoveData(0, 0);
                }
                Networking.Messages.Message m = new Networking.Messages.Message((int)Codes.Won, new byte[] { 0 });
                GameSocket.Send(m);
            }
            GameEnded = true;

            if (!IsExiting)
            {
                MessageBox.Show("Player reached the exit. You win!");
                Close();
            }
        }

        /// <summary>
        /// Handles a connection being made.
        /// </summary>
        /// <param name="sender">The origin of the event.</param>
        /// <param name="e">The event arugments.</param>
        private void Connected(object sender, ConnectedEventArgs e)
        {
            RemoteIP = e.RemoteIP;
            Paused = false;
            BeginTime = DateTime.Now;
        }

        /// <summary>
        /// Handles a disconnection.
        /// </summary>
        /// <param name="sender">The origin of the event.</param>
        /// <param name="e">The event arugments.</param>
        private void Disconnected(object sender, DisconnectedEventArgs e)
        {
            if (!GameEnded)
            {
                MessageBox.Show("Player disconnected!");
                Close();
            }
        }
    }
}
