﻿using System;
using System.Collections.Generic;
using OpenTK;
using RenderingEngine.Objects;
using RenderingEngine.Objects.Collisions;
using Game.Items.Enemies;
using Game.Items.Pickups;
using Game.Items.Walls;

namespace Game.Items
{
    /// <summary>
    /// A class representing the player.
    /// </summary>
    public class Player : Mesh
    {
        /// <summary>
        /// Gets or sets the default player mesh.
        /// </summary>
        /// <remarks>This is so that a player can be loaded without having to read the player mesh multiple times.</remarks>
        private static Mesh PlayerMesh { get; set; }

        /// <summary>
        /// Gets or sets the offset to place the camera from the player.
        /// </summary>
        private static Vector3 CameraOffset { get; set; }

        /// <summary>
        /// The height the player position should be set to.
        /// </summary>
        private const float Height = 1;

        /// <summary>
        /// The movement speed of the player.
        /// </summary>
        private const float MoveSpeed = 3;

        /// <summary>
        /// Initializes the default player mesh to be used when creating a new player.
        /// </summary>
        public static void InitPlayerMesh(ref Dictionary<string, Material> materials)
        {
            PlayerMesh = ObjectLoader.LoadMeshFromFile("player.obj", ref materials);
            CameraOffset = new Vector3(0, 0, 0);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Player"/> class.
        /// </summary>
        public Player()
            : base()
        {
            // Clone mesh data
            CloneMeshData(PlayerMesh);
            Name = "Player";

            // Set up the bounding box data
            GenerateBoundingBox();

            Forward = 0;
            Right = 0;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Player"/> class.
        /// </summary>
        /// <param name="position">The starting position of the player.</param>
        /// <param name="orientation">The starting orientation of the player in raidans.</param>
        public Player(Vector2 position, float orientation)
            : this()
        {
            Position = new Vector3(position.X, Height, position.Y);
            Rotation = new Vector3(0, orientation, 0);
        }

        /// <summary>
        /// The rotation sensitivity of the player.
        /// </summary>
        public float RotationSensitivity;

        /// <summary>
        /// The forward component to the player's movement.
        /// </summary>
        private float Forward;

        /// <summary>
        /// The right component to the player's movement.
        /// </summary>
        private float Right;

        /// <summary>
        /// Move the player.
        /// </summary>
        /// <param name="timeElapsed">The time elapsed since the last movement command.</param>
        /// <param name="walls">The walls that cannot be walked though.</param>
        /// <param name="pickups">The pickups in the game to be collected.</param>
        /// <param name="enemies">The enemies in the game.</param>
        public void Move(float timeElapsed, ref List<Wall> walls, ref List<Pickup> pickups, ref List<Enemy> enemies)
        {
            // Run the move loop at least 50 times a second
            if (timeElapsed > 0.02f)
            {
                while (timeElapsed > 0.02f)
                {
                    Move(0.02f, ref walls, ref pickups, ref enemies);
                    timeElapsed -= 0.02f;
                }
                Move(timeElapsed, ref walls, ref pickups, ref enemies);
            }
            else
            {
                // Calculate forward and right movement for time elapsed
                float forward = Forward * timeElapsed * MoveSpeed;
                float right = Right * timeElapsed * MoveSpeed;

                // Get forward and right vectors to calculate the offset
                Vector3 forwardV = new Vector3((float)Math.Sin(Rotation.Y), 0, (float)Math.Cos(Rotation.Y));
                Vector3 rightV = new Vector3((float)-Math.Cos(Rotation.Y), 0, (float)Math.Sin(Rotation.Y));

                // Calculate offset and move
                Vector3 offset = new Vector3();
                offset += right * rightV;
                offset += forward * forwardV;
                Position += offset;

                // Create new bounding box representing the move
                OrientedBoundingBox adjusted = new OrientedBoundingBox(new Mesh());
                adjusted.Orientation = OrientedBoundingBox.Orientation;
                adjusted.Extents = new Vector3(OrientedBoundingBox.Extents.X + right, OrientedBoundingBox.Extents.Y, OrientedBoundingBox.Extents.Z + forward);
                adjusted.Center = OrientedBoundingBox.Center + offset / 2;

                // Check collision of Adj-OBB against walls
                foreach (Wall w in walls)
                {
                    if (adjusted.Collision(w))
                    {
                        // Collided with wall, generate a collision contact
                        Contact c = new Contact(this, w.OrientedBoundingBox);
                        if (c.Depth > 0)
                        {
                            Vector3 posAdjustment = c.Normal.Normalized() * c.Depth;
                            Position -= posAdjustment;
                            adjusted.Center -= posAdjustment;
                        }
                    }
                }

                CheckPickups(ref walls, ref pickups, ref enemies);
            }
        }

        /// <summary>
        /// Turn the player.
        /// </summary>
        /// <param name="movement">How much the mouse was moved.</param>
        public void Turn(float movement)
        {
            // Adjust rotation to the sensitivity
            movement *= RotationSensitivity;
            Rotation = new Vector3(Rotation.X, (float)((Rotation.Y + movement) % (Math.PI * 2)), Rotation.Z);
        }

        /// <summary>
        /// Check if the player had collided (picked up) a pickup.
        /// </summary>
        /// <param name="walls">The walls in the game.</param>
        /// <param name="pickups">The pickups in the game.</param>
        /// <param name="enemies">The enemies in the game.</param>
        public void CheckPickups(ref List<Wall> walls, ref List<Pickup> pickups, ref List<Enemy> enemies)
        {
            for (int i = 0; i < pickups.Count; i++)
            {
                if (pickups[i].OrientedBoundingBox.Collision(OrientedBoundingBox))
                {
                    // Reset iteration if the pickup gets destroyed
                    int pickupIndex = i;
                    if (pickups[i].GetsDestroyed)
                    {
                        i--;
                    }

                    // Pick up the item
                    Player clone = new Player(new Vector2(Position.X, Position.Z), Rotation.Y);
                    pickups[pickupIndex].PickUp(ref clone, ref walls, ref pickups, ref enemies);

                    // Copy clone data back incase player has been adjusted by pickup
                    Position = clone.Position;
                    Rotation = clone.Rotation;
                }
            }
        }

        /// <summary>
        /// Binds the camera to the player. This will make sure the camera is at the correct viewing hight of the player.
        /// </summary>
        /// <param name="camera">The camera to bind to the player.</param>
        public void BindCamera(ref RenderingEngine.Camera camera)
        {
            camera.BeginFollowing(this, CameraOffset);
            camera.Orientation.Y = Rotation.Y;
            RotationSensitivity = camera.RotationSensitivity;
        }

        /// <summary>
        /// Sets the player's movement direction.
        /// </summary>
        /// <param name="forward">The forward component to the movement.</param>
        /// <param name="right">The right component to the movement.</param>
        public void SetMoveDirection(float forward, float right)
        {
            Forward = forward;
            Right = right;
        }
    }
}
