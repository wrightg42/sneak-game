﻿using System.Collections.Generic;
using OpenTK;
using Game.Items.Enemies;
using Game.Items.Walls;

namespace Game.Items.Pickups
{
    /// <summary>
    /// A class represeting the exit level pickup.
    /// </summary>
    public class Exit : Pickup
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Exit"/> class.
        /// </summary>
        public Exit()   
            : base()
        {
            Name = "Exit Pickup";
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Exit"/> class.
        /// </summary>
        /// <param name="position">The position of the exit pickup.</param>
        public Exit(Vector2 position)
            : this()
        {
            Position = new Vector3(position.X, Height, position.Y);
        }

        /// <summary>
        /// A procedure called to pick up the item.
        /// </summary>        
        /// <param name="walls">The walls in the game.</param>
        /// <param name="pickups">The pickups in the game.</param>
        /// <param name="enemies">The enemies in the game.</param>
        public override void PickUp(ref Player player, ref List<Wall> walls, ref List<Pickup> pickups, ref List<Enemy> enemies)
        {
            base.PickUp(ref player, ref walls, ref pickups, ref enemies);
        }
    }
}
