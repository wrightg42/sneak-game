﻿using System.Collections.Generic;
using OpenTK;
using Game.Items.Enemies;
using Game.Items.Walls;

namespace Game.Items.Pickups
{
    /// <summary>
    /// A class represeting the open door pickup.
    /// </summary>
    public class Key : Pickup
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Key"/> class.
        /// </summary>
        public Key()   
            : base()
        {
            Name = "Key";
            Color = new Vector3(-1, -1, -1);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Key"/> class.
        /// </summary>
        /// <param name="color">The colour of the door to open.</param>
        public Key(Vector3 color)
            : base()
        {
            Name = "Key " + color.ToString();
            SetMeshColor(color);
            Color = color;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Key"/> class.
        /// </summary>
        /// <param name="position">The position of the key pickup.</param>
        /// <param name="color">The colour of the door to open.</param>
        public Key(Vector2 position, Vector3 color)
            : this(color)
        {
            Position = new Vector3(position.X, Height, position.Y);
        }

        /// <summary>
        /// Gets or sets the colour of the key.
        /// </summary>
        public Vector3 Color { get; set; }

        /// <summary>
        /// A procedure called to pick up the item.
        /// </summary>
        /// <param name="walls">The walls in the game.</param>
        /// <param name="pickups">The pickups in the game.</param>
        /// <param name="enemies">The enemies in the game.</param>
        public override void PickUp(ref Player player, ref List<Wall> walls, ref List<Pickup> pickups, ref List<Enemy> enemies)
        {
            base.PickUp(ref player, ref walls, ref pickups, ref enemies);

            // Make sure there are no more keys for the door before opening
            bool otherKeys = false;
            for (int i = 0; i < pickups.Count; i++)
            {
                if (pickups[i] is Key && (pickups[i] as Key).Color == Color)
                {
                    otherKeys = true;
                    break;
                }
            }

            // Open door if key picked up by searching for the corrosponding door and deleting it
            if (!otherKeys)
            {
                for (int i = 0; i < walls.Count; i++)
                {
                    // Check it is a door, and the right colour
                    if (walls[i] is Door && (walls[i] as Door).Color == Color)
                    {
                        // Make unrenderable (0 scale) and then remove from walls
                        walls[i].Scale = new Vector3();
                        walls.RemoveAt(i);
                        i--;
                    }
                }
            }
        }
    }
}
