﻿using System;
using System.Collections.Generic;
using OpenTK;
using RenderingEngine.Objects;
using Game.Items.Enemies;
using Game.Items.Walls;

namespace Game.Items.Pickups
{
    /// <summary>
    /// An abstact class defining basic methods to be included by a pickup.
    /// </summary>
    public abstract class Pickup : Mesh
    {
        /// <summary>
        /// Gets or sets the default exit pickup mesh.
        /// </summary>
        /// <remarks>This is so that a exit pickup can be loaded without having to read the exit pickup mesh multiple times.</remarks>
        protected static Mesh PickupMesh { get; set; }

        /// <summary>
        /// The height the player position should be set to.
        /// </summary>
        protected const float Height = 0.1f;

        /// <summary>
        /// Gets or sets the scale of the pickup.
        /// </summary>
        protected const float PickupScale = 0.5f;

        /// <summary>
        /// Initializes the default exit mesh to be used when creating a new exit pickup.
        /// </summary>
        public static void InitPickupMesh(ref Dictionary<string, Material> materials)
        {
            PickupMesh = ObjectLoader.LoadMeshFromFile("pickup.obj", ref materials);

            // Set the angle to it appears like a diamond
            PickupMesh.Rotation = new Vector3((float)(Math.PI / 4), 0, (float)(Math.PI / 4));
        }

        /// <summary>
        /// Turns the pickup on the Y-Axis to render as a square for the map player.
        /// </summary>
        public static void TurnOnYAxis()
        {
            PickupMesh.Rotation = new Vector3(0, (float)(Math.PI / 4), 0);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Pickup"/> class.
        /// </summary>
        public Pickup()
            : base()
        {
            // Clone mesh data
            CloneMeshData(PickupMesh);
            Name = "Pickup";

            // Set up the bounding box data
            GenerateBoundingBox();

            // Set rotation to make it a diamond shape and set colour to default
            Position = new Vector3(0, Height, 0);
            Rotation = PickupMesh.Rotation;
            Scale = new Vector3(PickupScale, PickupScale, PickupScale);
            SetMeshColor(new Vector3(1, 1, 1));
            GetsDestroyed = true;
        }

        /// <summary>
        /// Fires when a pickup is picked up.
        /// </summary>
        public event EventHandler PickedUp;

        /// <summary>
        /// Gets or sets a value indicating whether the pickup gets destroyed.
        /// </summary>
        public bool GetsDestroyed;

        /// <summary>
        /// A procedure called to pick up the item.
        /// </summary>
        /// <param name="player">The player in the game.</param>
        /// <param name="walls">The walls in the game.</param>
        /// <param name="pickups">The pickups in the game.</param>
        /// <param name="enemies">The enemies in the game.</param>
        public virtual void PickUp(ref Player player, ref List<Wall> walls, ref List<Pickup> pickups, ref List<Enemy> enemies)
        {
            PickedUp?.Invoke(this, new EventArgs());
            if (GetsDestroyed)
            {
                Destroy(ref pickups);
            }
        }

        /// <summary>
        /// Destroys the pick up.
        /// </summary>
        /// <param name="pickups">The pickups in the game.</param>
        private void Destroy(ref List<Pickup> pickups)
        {
            Scale = new Vector3();
            pickups.Remove(this);
        }
    }
}
