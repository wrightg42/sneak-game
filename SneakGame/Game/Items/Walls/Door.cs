﻿using System.Collections.Generic;
using OpenTK;
using RenderingEngine.Objects;

namespace Game.Items.Walls
{
    /// <summary>
    /// A class representing a door.
    /// </summary>
    public class Door : Wall
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Door"/> class.
        /// </summary>
        public Door()
            : base()
        {
            // Clone mesh data
            CloneMeshData(WallMesh);
            Name = "Door";

            // Set up the bounding box data
            GenerateBoundingBox();

            // Create the needed height of a wall
            Scale = new Vector3(1, Height / 2, Tickness / 2);
            Position = new Vector3(0, (Height - 2) / 2, 0);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Door"/> class.
        /// </summary>
        /// <param name="colour">The colour of the door.</param>
        public Door(Vector3 color)
            : this()
        {
            Name = "Door " + color.ToString();
            Color = color;
            SetMeshColor(color);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Door"/> class.
        /// </summary>
        /// <param name="position">The position of the door.</param>
        /// <param name="orientation">The orientation of the door in raidans.</param>
        /// <param name="length">The length of the door.</param>
        /// <param name="color">The colour of the door.</param>
        public Door(Vector2 position, float orientation, float length, Vector3 color)
            : this(color)
        {
            Position = new Vector3(position.X, (Height - 2) / 2, position.Y);
            Rotation = new Vector3(0, orientation, 0);
            Scale = new Vector3((length + 0.98f) / 2, Height / 2, Tickness / 2); // Extends wall to correct length
        }

        /// <summary>
        /// Gets or sets the colour of the door.
        /// </summary>
        public Vector3 Color { get; set; }
    }
}
