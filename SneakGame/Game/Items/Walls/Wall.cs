﻿using System;
using System.Collections.Generic;
using OpenTK;
using RenderingEngine.Objects;

namespace Game.Items.Walls
{
    /// <summary>
    /// A class representing a wall.
    /// </summary>
    public class Wall : Mesh
    {
        /// <summary>
        /// Gets or sets the default wall mesh.
        /// </summary>
        /// <remarks>This is so that a wall can be loaded without having to read the wall mesh multiple times.</remarks>
        protected static Mesh WallMesh { get; set; }

        /// <summary>
        /// The thickness of walls.
        /// </summary>
        protected const float Tickness = 1;

        /// <summary>
        /// The height of walls.
        /// </summary>
        public const float Height = 4;

        /// <summary>
        /// Initializes the default wall mesh to be used when creating a new wall.
        /// </summary>
        public static void InitWallMesh(ref Dictionary<string, Material> materials)
        {
            WallMesh = ObjectLoader.LoadMeshFromFile("wall.obj", ref materials);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Wall"/> class.
        /// </summary>
        public Wall()
            : base()
        {
            // Clone mesh data
            CloneMeshData(WallMesh);
            Name = "Wall";

            // Set up the bounding box data
            GenerateBoundingBox();

            // Create the needed height of a wall
            Scale = new Vector3(1, Height / 2, Tickness / 2);
            Position = new Vector3(0, (Height - 2) / 2, 0);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Wall"/> class.
        /// </summary>
        /// <param name="position">The position of the wall.</param>
        /// <param name="orientation">The orientation of the wall in raidans.</param>
        /// <param name="length">The length of the wall.</param>
        public Wall(Vector2 position, float orientation, float length)
            : this()
        {
            Position = new Vector3(position.X, (Height - 2) / 2, position.Y);
            Rotation = new Vector3(0, orientation, 0);
            Scale = new Vector3((length + 0.98f) / 2, Height / 2, Tickness / 2); // Extends wall to correct length
        }

        /// <summary>
        /// A private store of the scale of the mesh.
        /// </summary>
        private Vector3 _scale;

        /// <summary>
        /// Gets or sets the scale of the mesh.
        /// </summary>
        public override Vector3 Scale
        {
            get
            {
                return _scale;
            }
            set
            {
                // Store value
                _scale = value;
                base.Scale = value;

                // Scale border of the mesh
                if (Faces != null)
                {
                    ScaleBorder();
                }
            }
        }

        /// <summary>
        /// Scales the border on a wall mesh.
        /// </summary>
        private void ScaleBorder()
        {
            // Calculate the position of the point
            float x = 1 - 0.02f / Scale.X;
            float y = 1 - 0.02f / Scale.Y;
            float z = 1 - 0.02f / Scale.Z;

            // Iterate over every point in the mesh
            foreach (Face f in Faces)
            {
                foreach (Vertex v in f.Verticies)
                {
                    // Alter vertex if it is a border point
                    // Line tests if it is a border, if it is test if it is negative then set position to correct point
                    float newX = Math.Abs(v.Position.X) == 1 ? v.Position.X : v.Position.X > 0 ? x : -x;
                    float newY = Math.Abs(v.Position.Y) == 1 ? v.Position.Y : v.Position.Y > 0 ? y : -y;
                    float newZ = Math.Abs(v.Position.Z) == 1 ? v.Position.Z : v.Position.Z > 0 ? z : -z;

                    // Set new position
                    v.Position = new Vector3(newX, newY, newZ);
                }
            }
        }
    }
}
