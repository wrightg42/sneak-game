﻿using System.Collections.Generic;
using System.Linq;
using OpenTK;
using RenderingEngine.Objects;

namespace Game.Items.Enemies
{
    /// <summary>
    /// A class representing a robot enemy unit.
    /// </summary>
    public class Robot : ViewingEnemy
    {
        /// <summary>
        /// Gets or sets the default robot mesh.
        /// </summary>
        /// <remarks>This is so that a robot can be loaded without having to read the robot mesh multiple times.</remarks>
        private static Mesh RobotMesh { get; set; }

        /// <summary>
        /// Initializes the default robot mesh to be used when creating a new robot.
        /// </summary>
        public static void InitRobotMesh(ref Dictionary<string, Material> materials)
        {
            RobotMesh = ObjectLoader.LoadMeshFromFile("robot.obj", ref materials);
        }

        /// <summary>
        /// The default movement speed for robots.
        /// </summary>
        public const float DefaultMoveSpeed = 2;

        /// <summary>
        /// Initializes a new instance of the <see cref="Robot"/> class.
        /// </summary>
        public Robot()
            : base()
        {
            // Clone mesh data
            CloneMeshData(RobotMesh);
            Name = "Robot";

            // Set up the bounding box data
            GenerateBoundingBox();

            MoveSpeed = DefaultMoveSpeed;
            TurnSpeed = DefaultTurnSpeed;
            Movements = new List<Movement>();
            TotalMoveTime = 0;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Robot"/> class.
        /// </summary>
        /// <param name="beginPosition">The initial position of the robot.</param>
        /// <param name="beginTurn">The initial turn angle of the robot.</param>
        /// <param name="path">The path the robot should take.</param>
        /// <param name="viewRange">The viewing range of the robot.</param>
        /// <param name="viewAngle">The viewing angle of the robot.</param>
        /// <param name="moveSpeed">The movement speed of the robot.</param>
        /// <param name="turnSpeed">The turning speed of the robot.</param>
        /// <param name="autoTurn">Whether the turns between movements should be automatically generated.</param>
        /// <remarks>See <see cref="Movement.ParseMoves(string, float, float, OpenTK.Vector2, float, bool, bool)"/> for documentation on how to generate <paramref name="path"/> string.</remarks>
        public Robot(Vector2 beginPosition, float beginTurn, string path, float viewRange = DefaultViewRange, float viewAngle = DefaultViewAngle, float moveSpeed = DefaultMoveSpeed, float turnSpeed = DefaultTurnSpeed, bool autoTurn = true)
            : this()
        {
            ViewRange = viewRange;
            ViewAngle = viewAngle;
            MoveSpeed = moveSpeed;
            TurnSpeed = turnSpeed;
            Movements = Movement.ParseMoves(path, MoveSpeed, TurnSpeed, beginPosition, beginTurn, autoTurn).ToList();

            // Calculate the total move time
            TotalMoveTime = Movements[Movements.Count - 1].FinishTime;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Robot"/> class.
        /// </summary>
        /// <param name="beginPosition">The initial position of the robot.</param>
        /// <param name="beginTurn">The initial turn angle of the robot.</param>
        /// <param name="points">The point the robot should move to.</param>
        /// <param name="viewRange">The viewing range of the robot.</param>
        /// <param name="viewAngle">The viewing angle of the robot.</param>
        /// <param name="moveSpeed">The movement speed of the robot.</param>
        /// <param name="turnSpeed">The turning speed of the robot.</param>
        /// <param name="autoTurn">Whether the turns between movements should be automatically generated.</param>
        /// <remarks>See <see cref="Movement.ParseMoves(string, float, float, OpenTK.Vector2, float, bool, bool)"/> for documentation on how to generate <paramref name="path"/> string.</remarks>
        public Robot(Vector2 beginPosition, float beginTurn, Vector2[] points, float viewRange = DefaultViewRange, float viewAngle = DefaultViewAngle, float moveSpeed = DefaultMoveSpeed, float turnSpeed = DefaultTurnSpeed, bool autoTurn = true)
            : this()
        {
            // Convert points into a string equivalent
            string path = string.Empty;
            foreach (Vector2 p in points)
            {
                path += string.Format("m {0},{1};", p.X, p.Y);
            }

            // Remove final semi-colon
            path = path.Substring(0, path.Length - 1);

            // Generate robot data
            ViewRange = viewRange;
            ViewAngle = viewAngle;
            MoveSpeed = moveSpeed;
            TurnSpeed = turnSpeed;
            Movements = Movement.ParseMoves(path, MoveSpeed, TurnSpeed, beginPosition, beginTurn, autoTurn).ToList();

            // Calculate the total move time
            TotalMoveTime = Movements[Movements.Count - 1].FinishTime;
        }

        /// <summary>
        /// The list of movements for the robot to follow.
        /// </summary>
        public List<Movement> Movements { get; private set; }

        /// <summary>
        /// Gets or sets the total time it takes for the robot to traverse it's movement path.
        /// </summary>
        public float TotalMoveTime { get; private set; }

        /// <summary>
        /// Gets or sets the movement speed of the robot.
        /// </summary>
        private float MoveSpeed { get; set; }

        /// <summary>
        /// Gets or sets the turning speed of the robot.
        /// </summary>
        private float TurnSpeed { get; set; }

        /// <summary>
        /// Moves the enemy.
        /// </summary>
        /// <param name="time">The time elapsed since the start of the game.</param>
        public override void Move(float time)
        {
            // Adjust time to be over a duration to cover movements
            float adjTime = time % TotalMoveTime;

            // Find the movement occuring at the given time.
            for (int i = 0; i < Movements.Count; i++)
            {
                if (adjTime < Movements[i].FinishTime)
                {
                    // Set position and turn based off movement
                    Position = Movements[i].CurrentPosition(adjTime, 1);
                    Turn = Movements[i].CurrentTurn(adjTime);

                    // Movement parsed, break loop
                    break;
                }
            }
        }
    }
}
