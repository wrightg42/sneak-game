﻿using System;
using System.Collections.Generic;
using OpenTK;
using RenderingEngine.Objects;
using RenderingEngine.Objects.Collisions;
using Game.Items.Walls;

namespace Game.Items.Enemies
{
    /// <summary>
    /// An abstract class defining basic methods to be included by an enemy unit that using a viewing cone.
    /// </summary>
    public abstract class ViewingEnemy : Enemy
    {
        /// <summary>
        /// Gets or sets the default sights mesh.
        /// </summary>
        /// <remarks>This is so that a sight can be loaded without having to read the sight mesh multiple times.</remarks>
        private static Mesh SightMesh { get; set; }

        /// <summary>
        /// The default turning speed of viewing enemies.
        /// </summary>
        public const float DefaultTurnSpeed = 0.5f;

        /// <summary>
        /// The default viewing angle of viewing enemies.
        /// </summary>
        public const float DefaultViewAngle = (float)(Math.PI / 4);

        /// <summary>
        /// The default viewing range of viewing enemies.
        /// </summary>
        public const float DefaultViewRange = 20;

        /// <summary>
        /// The decay rate of detection levels.
        /// </summary>
        private const float DetectionDecay = 0.2f;

        /// <summary>
        /// The increase rate of detection levels.
        /// </summary>
        private const float DetectionIncreaseRate = (1f / 8f);

        /// <summary>
        /// Initializes the default player mesh to be used when creating a new player.
        /// </summary>
        public static void InitSightMesh(ref Dictionary<string, Material> materials)
        {
            SightMesh = ObjectLoader.LoadMeshFromFile("sight.obj", ref materials);
        }

        /// <summary>
        /// The default constructor for a viewing enemy.
        /// </summary>
        /// <remarks>Initializes base variables for a viewing enemy.</remarks>
        public ViewingEnemy()
        {
            // Set up the camera's vision cone
            Sight = new Mesh();
            Sight.CloneMeshData(SightMesh);

            // Set default variable values
            Position = new Vector3(0, 0, 0);
            DetectionLevel = 0;
            Turn = 0;
            ViewAngle = DefaultViewAngle;
            ViewRange = DefaultViewRange;
        }

        /// <summary>
        /// A private store of the position of the enemy.
        /// </summary>
        private Vector3 _position;

        /// <summary>
        /// Gets or sets the position of the enemy.
        /// </summary>
        public new Vector3 Position
        {
            get
            {
                return _position;
            }
            set
            {
                // Run base position code - keep bounding boxes up to date
                base.Position = value;
                _position = value;

                SetSightPosition();
            }
        }

        /// <summary>
        /// Gets or sets the detection percentage of the player to the enemy unit.
        /// </summary>
        public new float DetectionLevel
        {
            get
            {
                return base.DetectionLevel;
            }
            set
            {
                // Run base detection level code (cap value)
                base.DetectionLevel = value;

                // Set the sight colour based off the detection level
                Sight.SetMeshColor(DetectionToColor(base.DetectionLevel));
            }
        }


        /// <summary>
        /// A private store of the current turn angle.
        /// </summary>
        private float _turn;

        /// <summary>
        /// Gets or sets the current turn on the camera.
        /// </summary>
        public float Turn
        {
            get
            {
                return _turn;
            }
            set
            {
                // Set the view direction and rotation of the base mesh
                Rotation = new Vector3(0, value, 0);
                Sight.Rotation = Rotation;

                _turn = value;

                // Reset position of sights
                SetSightPosition();
            }
        }

        /// <summary>
        /// Gets or sets the cone representing the enemy's cone of vision.
        /// </summary>
        public Mesh Sight { get; set; }

        /// <summary>
        /// A private store of the view angle.
        /// </summary>
        private float _viewAngle;

        /// <summary>
        /// The viewing angle of the enemy.
        /// </summary>
        public float ViewAngle
        {
            get
            {
                return _viewAngle;
            }
            set
            {
                // Set view cone's view angle
                float width = ViewRange * (float)Math.Tan(value / 2);
                Sight.Scale = new Vector3(width, Sight.Scale.Y, Sight.Scale.Z);

                _viewAngle = value;
            }
        }

        /// <summary>
        /// A private store of the view range.
        /// </summary>
        private float _viewRange;

        /// <summary>
        /// The viewing range of the enemy.
        /// </summary>
        public float ViewRange
        {
            get
            {
                return _viewRange;
            }
            set
            {
                // Set the cone's view range
                Sight.Scale = new Vector3(Sight.Scale.X, Sight.Scale.Y, value / 2);

                _viewRange = value;
            }
        }

        /// <summary>
        /// Updates the detection levels of the player to the enemy unit.
        /// </summary>
        /// <param name="p">The player to be detected.</param>
        /// <param name="walls">The walls in the level - these may hide the player</param>
        /// <param name="timeElapsed">The time elapsed over since the last detection check.</param>
        public override sealed void UpdatePlayerDetection(Player p, List<Wall> walls, float timeElapsed)
        {
            // Calculate relative player position
            Vector2 adjPos = new Vector2(p.Position.X - Position.X, p.Position.Z - Position.Z);

            // Calculate distance and set up store for player angle
            float dist = adjPos.Length;
            float playerAngle = 0;

            bool inView = false; // If the player is in view

            // Check player is in range before angle (as angle checks are more expensive)
            if (dist < ViewRange)
            {
                // Get unit cirlce vector of the direction the camera is facing
                Vector2 currentAnglePos = new Vector2((float)Math.Sin(Turn), (float)Math.Cos(Turn));

                // Get player angle using dot product
                playerAngle = (float)Math.Acos(Vector2.Dot(adjPos, currentAnglePos) / dist);

                // If the player angle is within the viewing angle state the player is within view
                if (Math.Abs(playerAngle) < ViewAngle / 2)
                {
                    inView = true;
                }
            }

            // Get the change in detection level
            float detectionChange = -DetectionDecay;
            if (inView)
            {
                detectionChange += CalulateDetectionIncrease(p, dist, walls, timeElapsed);
                if (detectionChange != -DetectionDecay)
                {
                    detectionChange += DetectionDecay;
                }
            }

            // Update detection level
            DetectionLevel += detectionChange;
        }

        /// <summary>
        /// Calcualtes the increase in detection from a player distance and angle.
        /// </summary>
        /// <param name="p">The player to be detected.</param>
        /// <param name="dist">The distance of the player from the camera.</param>
        /// <param name="walls">The walls in the level - these may hide the player</param>
        /// <param name="timeElapsed">The time elapsed over since the last detection check.</param>
        /// <returns>The increase in detection levels.</returns>
        protected float CalulateDetectionIncrease(Player p, float dist, List<Wall> walls, float timeElapsed)
        {
            // Check how many player verticies are visible from the camera
            int visibleCorners = 0;
            foreach (Vector3 v in p.OrientedBoundingBox.Verticies)
            {
                Raycast r = new Raycast(Position.Xz, v.Xz);
                if (r.Intersections(walls).Count == 0)
                {
                    // No intersections so add visible corner
                    visibleCorners++;
                }
            }

            // Get detection increase from corners, distance, and time since last check
            return visibleCorners * ((float)Math.Pow((ViewRange - dist + 1), 2) / ViewRange) * DetectionIncreaseRate * (timeElapsed / 0.01f);
        }

        /// <summary>
        /// Converts a dectection level to a colour representing the danger level.
        /// </summary>
        /// <param name="detection">The detection level</param>
        /// <returns>The colour representing the current detection level.</returns>
        protected Vector3 DetectionToColor(float detection)
        {
            // Calcualte red/green values based off detection levels
            float red = Math.Min(Math.Max(detection / 50, 0), 1);
            float green = Math.Min(Math.Max((100 - detection) / 50, 0), 1);

            // Return the generated colour
            return new Vector3(red, green, 0);
        }

        /// <summary>
        /// Sets the sight position relative to the current position and turn.
        /// </summary>
        private void SetSightPosition()
        {
            // Move view cone
            float X = Position.X + ViewRange * (float)(Math.Sin(Turn) / 2);
            float Z = Position.Z + ViewRange * (float)(Math.Cos(Turn) / 2);

            Sight.Position = new Vector3(X, Position.Y, Z);
        }
    }
}
