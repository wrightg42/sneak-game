﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenTK;

namespace Game.Items.Enemies
{
    /// <summary>
    /// A class representing a movement for enemy units.
    /// </summary>
    public class Movement
    {
        /// <summary>
        /// Parses a series of movements from a string.
        /// </summary>
        /// <param name="s">The string to be parsed as movements.</param>
        /// <param name="turnSpeed">The turn speed of movements.</param>
        /// <param name="moveSpeed">The move speed of movements.</param>
        /// <param name="beginPosition">The inital position before movements.</param>
        /// <param name="beginTurn">The initial amount of turn before the movements.</param>
        /// <param name="autoTurn">Whether turns should be automatically genereted.</param>
        /// <returns>A <see cref="List{T}"/> of <see cref="Movement"/>s.</returns>
        /// <remarks>Moves are defined in the usual format, found in the <see cref="ParseMove(string, float, float, float, Vector2, float)"/> documentation, with a semi-colon (;) to seperate elements.
        /// With <paramref name="autoTurn"/> set to false, turns are automatically calcualted so that to robot always faces it's movement direction.</remarks>
        public static IEnumerable<Movement> ParseMoves(string s, float moveSpeed, float turnSpeed, Vector2 beginPosition, float beginTurn, bool autoTurn = true)
        {
            // Create move list and split string to parse into data chunks
            List<Movement> moves = new List<Movement>();
            string[] sMoves = s.Split(';');

            // Keep store of the current time/position/turn after latest move
            float time = 0;
            Vector2 position = beginPosition;
            float turn = beginTurn;

            // Parse each move
            foreach (string cmd in sMoves)
            {
                // Parse movement command
                Movement move = ParseMove(cmd, time, moveSpeed, turnSpeed, position, turn);

                // Calculate turn before the move if it needs to be calculated
                if (autoTurn)
                {
                    Movement turnMovement = CreateTurnFix(move, turnSpeed);

                    // Update turn
                    turn += turnMovement.Turn;
                    move.BeginTurn = turn;

                    // Update time data and add turn, provided this is not the first move
                    if (moves.Count != 0)
                    {
                        time += turnMovement.ActionTime;
                        move.BeginTime = time;
                        move.FinishTime += turnMovement.ActionTime;
                        moves.Add(turnMovement);
                    }
                }

                // Update time/position/turn
                time += move.ActionTime;
                position += move.MoveVector;
                turn += move.Turn;

                // Add movement to list of moves
                moves.Add(move);
            }

            // Form loop if required
            if (position == beginPosition && turn != moves[0].BeginTurn)
            {
                // Turn not aligned so realign with begining
                float turnSize = (float)((2 * Math.PI + (moves[0].BeginTurn - turn)) % (2 * Math.PI));
                if (turnSize > Math.PI)
                {
                    turnSize = (float)(turnSize - 2 * Math.PI);
                }

                moves.Add(new Movement(time, turn, turnSize, turnSpeed, position));
            }
            else if (position != beginPosition)
            {
                // Position ended up elsewhere so reverse movement chain
                moves.AddRange(ReserveMovements(moves, turnSpeed));
            }

            return moves;
        }

        /// <summary>
        /// Parses a movement from a string.
        /// </summary>
        /// <param name="s">The string to be parsed as a movement.</param>
        /// <param name="beginTime">The starting time of the movement.</param>
        /// <param name="turnSpeed">The turn speed of the movement.</param>
        /// <param name="moveSpeed">The move speed of the movement.</param>
        /// <param name="beginPosition">The initial position of the movement.</param>
        /// <param name="beginTurn">The initial turn of the movement.</param>
        /// <returns>A <see cref="Movement"/> represeting that specified by the string <paramref name="s"/>.</returns>
        /// <remarks>Moves are defined in the format: 
        ///     m x,z   - Denotes move to the coordinate (x,z)
        ///     t n     - Denotes turn by n radians
        /// </remarks>
        public static Movement ParseMove(string s, float beginTime, float moveSpeed, float turnSpeed, Vector2 beginPosition, float beginTurn)
        {
            // Split string into move type and data
            char type = s.ToLower()[0];
            string data = s.Substring(2);

            // Create movement data based off of the movement type
            if (type == 'm')
            {
                // Parse final vector position and return movement structure
                string[] XYData = data.Split(',');
                Vector2 moveTo = new Vector2(float.Parse(XYData[0]), float.Parse(XYData[1]));
                return new Movement(beginTime, beginPosition, moveTo, moveSpeed, beginTurn);
            }
            else if (type == 't')
            {
                // Parse turn amount and return movement structure
                float turn = float.Parse(data);
                return new Movement(beginTime, beginTurn, turn, turnSpeed, beginPosition);
            }
            else
            {
                // Invalid data, return null
                return new Movement();
            }
        }

        /// <summary>
        /// Creates a turn to make sure a movement is at the correct angle.
        /// </summary>
        /// <param name="move">The move to have a turn to fix the angle.</param>
        /// <param name="turnSpeed">The turn speed of the turn.</param>
        /// <returns>A turn to correct the angle of a movement.</returns>
        private static Movement CreateTurnFix(Movement move, float turnSpeed)
        {
            // Get angle of current movement
            float ang = (float)Math.Acos(move.MoveVector.Y / move.MoveVector.Length);
            if (move.MoveVector.X < 0)
            {
                ang = (float)(2 * Math.PI - ang);
            }

            // Get min angle of movement
            float turnSize = (float)((2 * Math.PI + (ang - move.BeginTurn)) % (2 * Math.PI));
            if (turnSize > Math.PI)
            {
                turnSize = (float)(turnSize - 2 * Math.PI);
            }

            // Create turn
            return new Movement(move.BeginTime, move.BeginTurn, turnSize, turnSpeed, move.BeginPosition);
        }

        /// <summary>
        /// Reserves a list of movements.
        /// </summary>
        /// <param name="moves">The moves to reverse.</param>
        /// <param name="turnSpeed">The turn speed of the turn.</param>
        /// <returns>The reveresed list of movements.</returns>
        private static IEnumerable<Movement> ReserveMovements(IEnumerable<Movement> moves, float turnSpeed)
        {
            // Store for reverse move sequence
            List<Movement> reversedMoves = new List<Movement>();

            // Add in a change direction
            reversedMoves.Add(new Movement(moves.Last().FinishTime, moves.Last().BeginTurn + moves.Last().Turn, (float)Math.PI, turnSpeed, moves.Last().FinalPosition));

            // Get starting time
            float time = reversedMoves[0].FinishTime;

            // Reverse all moves
            moves = moves.Reverse();
            foreach (Movement m in moves)
            {
                // Copy data but reversed
                Movement reverse = new Movement();
                reverse.BeginTime = time;
                reverse.ActionTime = m.ActionTime;
                reverse.FinishTime = reverse.BeginTime + reverse.ActionTime;
                reverse.BeginTurn = (float)Math.PI + m.BeginTurn + m.Turn;
                reverse.Turn = -m.Turn;
                reverse.BeginPosition = m.FinalPosition;
                reverse.FinalPosition = m.BeginPosition;
                reverse.MoveVector = -m.MoveVector;

                // Adjust time for next item
                time += reverse.ActionTime;

                // Return reversed item
                reversedMoves.Add(reverse);
            }

            // Add another change in direction at the end
            reversedMoves.Add(new Movement(time, moves.Last().BeginTurn + (float)Math.PI, (float)Math.PI, turnSpeed, moves.Last().BeginPosition));

            return reversedMoves;
        }

        /// <summary>
        /// Initalizes a new instance of the <see cref="Movement"/> class.
        /// </summary>
        public Movement()
        {
            BeginTime = 0;
            ActionTime = 0;
            FinishTime = 0;
            BeginTurn = 0;
            Turn = 0;
            BeginPosition = new Vector2();
            FinalPosition = new Vector2();
            MoveVector = new Vector2();
        }

        /// <summary>
        /// Initalizes a new instance of the <see cref="Movement"/> class.
        /// </summary>
        /// <param name="beginTime">The starting time for the movement.</param>
        /// <param name="beginTurn">The initial turn before the movement.</param>
        /// <param name="turn">The turn size to be performed.</param>
        /// <param name="turnSpeed">The turn speed of the movement.</param>
        /// <param name="beginPosition">The position of the movement during the turn.</param>
        public Movement(float beginTime, float beginTurn, float turn, float turnSpeed, Vector2 beginPosition)
            : base()
        {
            BeginTime = beginTime;
            BeginTurn = beginTurn;
            Turn = turn;
            ActionTime = Math.Abs(Turn) / turnSpeed;
            FinishTime = BeginTime + ActionTime;
            BeginPosition = beginPosition;
            FinalPosition = beginPosition;
        }

        /// <summary>
        /// Initalizes a new instance of the <see cref="Movement"/> class.
        /// </summary>
        /// <param name="beginTime">The starting time for the movement.</param>
        /// <param name="beginPosition">The initial position before the movement.</param>
        /// <param name="finalPosition">The final position after the movement.</param>
        /// <param name="moveSpeed">The movement speed of the move.</param>
        /// <param name="beginTurn">The angle to face during the movement.</param>
        public Movement(float beginTime, Vector2 beginPosition, Vector2 finalPosition, float moveSpeed, float beginTurn)
        {
            BeginTime = beginTime;
            BeginPosition = beginPosition;
            FinalPosition = finalPosition;
            MoveVector = FinalPosition - BeginPosition;
            ActionTime = MoveVector.Length / moveSpeed;
            FinishTime = BeginTime + ActionTime;
            BeginTurn = beginTurn;
        }

        /// <summary>
        /// Gets or sets the start time of the movement to be processed at.
        /// </summary>
        public float BeginTime { get; set; }

        /// <summary>
        /// Gets or sets how long it takes to complete the movement.
        /// </summary>
        public float ActionTime { get; set; }

        /// <summary>
        /// Gets or sets the time that the action completes.
        /// </summary>
        public float FinishTime { get; set; }

        /// <summary>
        /// Gets or sets the initial amount of turn before the movement.
        /// </summary>
        public float BeginTurn { get; set; }

        /// <summary>
        /// Gets or sets the amount of turn during the movement.
        /// </summary>
        public float Turn { get; set; }

        /// <summary>
        /// Gets or sets the initial position before the movement.
        /// </summary>
        public Vector2 BeginPosition { get; set; }

        /// <summary>
        /// Gets or sets the final position after the movement.
        /// </summary>
        public Vector2 FinalPosition { get; set; }

        /// <summary>
        /// Gets or sets the vector representing the change in position..
        /// </summary>
        private Vector2 MoveVector { get; set; }

        /// <summary>
        /// Calculates the current turn at a given time.
        /// </summary>
        /// <param name="time">The time for the turn to be calculated at.</param>
        /// <returns>The amount of turn at the given point in time.</returns>
        public float CurrentTurn(float time)
        {
            return BeginTurn + Turn * TimePercentage(time);
        }

        /// <summary>
        /// Calculates the current position at a given time.
        /// </summary>
        /// <param name="time">The time for the position to be calculated at.</param>
        /// <returns>The position at the given point in time.</returns>
        public Vector2 CurrentPosition(float time)
        {
            return BeginPosition + MoveVector * TimePercentage(time);
        }

        /// <summary>
        /// Calculates the current position at a given time.
        /// </summary>
        /// <param name="time">The time for the position to be calculated at.</param>
        /// <param name="height">The height the movement is acting at.</param>
        /// <returns>The position, including height, at the given point in time.</returns>
        public Vector3 CurrentPosition(float time, float height)
        {
            // Get Vector2 positon and convert to Vector3
            Vector2 pos = CurrentPosition(time);
            return new Vector3(pos.X, height, pos.Y);
        }

        /// <summary>
        /// Calculates the percentage of the movement that has occured.
        /// </summary>
        /// <param name="time">The current time to calculate the movement for.</param>
        /// <returns>The percentage completed of the movement.</returns>
        private float TimePercentage(float time)
        {
            float relativeTime = time - BeginTime;
            return relativeTime / ActionTime;
        }
    }
}
