﻿using System;
using System.Collections.Generic;
using RenderingEngine.Objects;
using Game.Items.Walls;

namespace Game.Items.Enemies
{
    /// <summary>
    /// An abstract class defining basic methods to be included by an enemy unit.
    /// </summary>
    public abstract class Enemy : Mesh
    {
        /// <summary>
        /// A private store of the detection level.
        /// </summary>
        private float _detectionLevel;
         
        /// <summary>
        /// Gets or sets the detection percentage of the player to the enemy unit.
        /// </summary>
        public float DetectionLevel
        {
            get
            {
                return _detectionLevel;
            }
            set
            {
                // Cap the detection level to 0 -> 100
                float capVal = Math.Min(value, 100);
                capVal = Math.Max(capVal, 0);

                _detectionLevel = capVal;
            }
        }

        /// <summary>
        /// Moves the enemy.
        /// </summary>
        /// <param name="time">The time elapsed since the start of the game.</param>
        public abstract void Move(float time);

        /// <summary>
        /// Updates the detection levels of the player to the enemy unit.
        /// </summary>
        /// <param name="p">The player to be detected.</param>
        /// <param name="walls">The walls in the level - these may hide the plare</param>
        /// <param name="timeElapsed">The time elapsed over since the last detection check.</param>
        public abstract void UpdatePlayerDetection(Player p, List<Wall> walls, float timeElapsed);

        /// <summary>
        /// Checks if the player has been detected.
        /// </summary>
        /// <returns>Whether the player has been detected.</returns>
        public bool PlayerDetected()
        {
            // Player has been detected if the detection level is at 100%
            if (DetectionLevel == 100)
            {
                return true;
            }

            return false;
        }
    }
}
