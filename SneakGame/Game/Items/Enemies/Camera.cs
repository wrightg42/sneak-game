﻿using System;
using System.Collections.Generic;
using OpenTK;
using RenderingEngine.Objects;
using Game.Items.Walls;

namespace Game.Items.Enemies
{
    /// <summary>
    /// A class representing a camera enemy unit.
    /// </summary>
    public class Camera : ViewingEnemy
    {
        /// <summary>
        /// Gets or sets the default camera mesh.
        /// </summary>
        /// <remarks>This is so that a camera can be loaded without having to read the camera mesh multiple times.</remarks>
        private static Mesh CameraMesh { get; set; }

        /// <summary>
        /// Initializes the default camera mesh to be used when creating a new camera.
        /// </summary>
        public static void InitCameraMesh(ref Dictionary<string, Material> materials)
        {
            CameraMesh = ObjectLoader.LoadMeshFromFile("camera.obj", ref materials);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Camera"/> class.
        /// </summary>
        public Camera()
            : base()
        {
            // Clone mesh data
            CloneMeshData(CameraMesh);
            Name = "Camera";

            // Set up the bounding box data
            GenerateBoundingBox();

            Position = new Vector3(0, Wall.Height - 1, 0);
            Scale = new Vector3(0.5f, 0.5f, 0.5f);

            MaxTurn = 0;
            NeutralTurn = 0;
            TurnSpeed = DefaultTurnSpeed;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Camera"/> class.
        /// </summary>
        /// <param name="position">The position of the camrea.</param>
        public Camera(Vector2 position)
            : this()
        {
            Position = new Vector3(position.X, Wall.Height - 1, position.Y);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Camera"/> class.
        /// </summary>
        /// <param name="position">The position of the camrea.</param>
        /// <param name="maxTurn">The maximum turn angle of the camera.</param>
        /// <param name="neutralTurn">The nuteral turn value of the camera.</param>
        /// <param name="viewRange">The viewing range of the camera.</param>
        /// <param name="viewAngle">The viewing angle of the camera.</param>
        /// <param name="turnSpeed">How much the camera turns in one second.</param>
        public Camera(Vector2 position, float maxTurn, float neutralTurn, float viewRange = DefaultViewRange, float viewAngle = DefaultViewAngle, float turnSpeed = DefaultTurnSpeed)
            : this(position)
        {
            MaxTurn = maxTurn;
            TurnSpeed = turnSpeed;
            NeutralTurn = neutralTurn;
            Turn = neutralTurn;
            ViewRange = viewRange;
            ViewAngle = viewAngle;
        }

        /// <summary>
        /// Gets the maximum turn of the camera.
        /// </summary>
        public float MaxTurn { get; set; }

        /// <summary>
        /// Gets or sets the neutral turn position.
        /// </summary>
        public float NeutralTurn { get; set; }

        /// <summary>
        /// Gets or sets how many radians can be turned in a second.
        /// </summary>
        private float TurnSpeed { get; set; }

        /// <summary>
        /// Moves the enemy.
        /// </summary>
        /// <param name="time">The time elapsed since the start of the game.</param>
        public override void Move(float time)
        {
            // Calculate camera angle
            float turnPercent = CalculateTurnPercentage(time); // The percentge of the turn completed
            Turn = NeutralTurn + MaxTurn * turnPercent;
        }

        /// <summary>
        /// Calculates the turn percentage at any given time.
        /// </summary>
        /// <param name="time">The time to calcualte the turn percentage from.</param>
        /// <returns>The turn percentage at the specifided time.</returns>
        protected float CalculateTurnPercentage(float time)
        {
            if (Math.Abs(MaxTurn) >= Math.PI)
            {
                // Adjust time to be over a duration to cover a full circle turn and convert to percentage turn
                MaxTurn = (float)Math.PI; // Make sure max turn is PI to bugs
                float adjTime = time % (2 * MaxTurn / TurnSpeed);
                return adjTime / (MaxTurn / TurnSpeed);
            }
            else
            {
                // Adjust time to be over a duration to cover movements and convert to a percentage turn
                float adjTime = time % (4 * MaxTurn / TurnSpeed);
                float percent = adjTime / (MaxTurn / TurnSpeed);

                if (percent < 1)
                {
                    // Moving towards whole turn from neutral
                    return percent;
                }
                else if (percent < 3)
                {
                    // Moving toward negative turn from whole
                    return 2 - percent;
                }
                else
                {
                    // Moving towards neutral from negative
                    return percent - 4;
                }
            }
        }
    }
}
