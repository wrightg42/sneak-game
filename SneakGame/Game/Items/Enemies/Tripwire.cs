﻿using System;
using System.Collections.Generic;
using OpenTK;
using Game.Items.Walls;
using RenderingEngine.Objects;

namespace Game.Items.Enemies
{
    /// <summary>
    /// A class representing a tripwire enemy unit.
    /// </summary>
    public class Tripwire : Enemy
    {
        /// <summary>
        /// Gets or sets the default tripwire mesh.
        /// </summary>
        /// <remarks>This is so that a tripwire can be loaded without having to read the tripwire mesh multiple times.</remarks>
        private static Mesh TripwireMesh { get; set; }

        /// <summary>
        /// Initializes the default tripwire mesh to be used when creating a new tripwire.
        /// </summary>
        public static void InitTripwireMesh(ref Dictionary<string, Material> materials)
        {
            TripwireMesh = ObjectLoader.LoadMeshFromFile("tripwire.obj", ref materials);
            TripwireMesh.SetMeshColor(new Vector3(1, 0, 0));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Tripwire"/> class.
        /// </summary>
        public Tripwire()
            : base()
        {
            // Clone mesh data
            CloneMeshData(TripwireMesh);
            Name = "Tripwire";

            // Set up the bounding box data
            GenerateBoundingBox();

            DetectionLevel = 0;
            Pulse = 1;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Tripwire"/> class.
        /// </summary>
        /// <param name="start">The starting point of the tripwire.</param>
        /// <param name="end">The endpoint of the tripwire.</param>
        public Tripwire(Vector2 start, Vector2 end)
            : this()
        {
            // Calculate the direction vector from start to end
            Vector2 direction = end - start;

            // Set position and length to be accurate
            Vector2 midpoint = direction / 2;
            Position = new Vector3(start.X, 0, start.Y) + new Vector3(midpoint.X, 0, midpoint.Y);
            Scale = new Vector3(direction.Length / 2, 1, 1);
            StandardScale = Scale;

            // Calculate orientation
            float rotationY = (float)Math.Atan(direction.Y / direction.X);
            Rotation = new Vector3(0, rotationY, 0);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Tripwire"/> class.
        /// </summary>
        /// <param name="start">The starting point of the tripwire.</param>
        /// <param name="end">The endpoint of the tripwire.</param>
        /// <param name="pulse">The pulse duration of the tripwire.</param>
        public Tripwire(Vector2 start, Vector2 end, float pulse)
            : this(start, end)
        {
            Pulse = pulse;
        }

        /// <summary>
        /// Gets or sets a value indicating wheather the tripwire is activated.
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Gets or sets the pulse speed of the tripwire, i.e. how often it turns off and on.
        /// </summary>
        public float Pulse { get; set; }

        /// <summary>
        /// Gets or sets the used sacle of the tipewire.
        /// </summary>
        private Vector3 StandardScale { get; set; }

        /// <summary>
        /// Moves the enemy.
        /// </summary>
        /// <param name="time">The time elapsed since the start of the game.</param>
        public override void Move(float Time)
        {
            // Change the state of the wire after (Pulse) seconds have passed.
            Active = (Time % (Pulse * 2)) <= Pulse ? true : false;

            // Show/hide the tripwire depending on if it is active
            if (!Active)
            {
                if (Scale != new Vector3())
                {
                    Scale = new Vector3();
                }
            }
            else
            {
                if (Scale == new Vector3())
                {
                    Scale = StandardScale;
                    GenerateBoundingBox();
                }
            }
        }

        /// <summary>
        /// Updates the detection levels of the player to the enemy unit.
        /// </summary>
        /// <param name="p">The player to be detected.</param>
        /// <param name="walls">The walls in the level - these may hide the plare</param>
        /// <param name="timeElapsed">The time elapsed over since the last detection check.</param>
        public override void UpdatePlayerDetection(Player p, List<Wall> walls, float timeElapsed)
        {
            if (Active && OrientedBoundingBox.Collision(p.OrientedBoundingBox))
            {
                DetectionLevel = 100;
            }
            else
            {
                DetectionLevel = 0;
            }
        }
    }
}
