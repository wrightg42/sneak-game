﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using OpenTK.Input;
using Game.Loader.Intermediary;

namespace Game.Loader
{
    /// <summary>
    /// A static class used to load options in the game.
    /// </summary>
    public static class OptionLoader
    {
        /// <summary>
        /// Loads options form a options file.
        /// </summary>
        /// <param name="path">The filepath of the options to use.</param>
        /// <param name="rotationSensitivity">The rotation sensitivity of the player.</param>
        /// <param name="inputs">The inputs to be used in the game.</param>
        /// <returns>Whether the options were loaded successfully.</returns>
        public static bool Load(string path, out float rotationSensitivity, out Dictionary<Inputs, Key> inputs)
        {
            // Read and desearialize the xml data
            XmlSerializer serializer = new XmlSerializer(typeof(OptionData));
            OptionData data = new OptionData();
            bool parsed = true;
            try
            {
                using (StreamReader sr = File.OpenText(path))
                {
                    data = (OptionData)serializer.Deserialize(sr);
                }
            }
            catch (Exception e)
            {
                // Output accurate error message
                if (e is DirectoryNotFoundException || e is FileNotFoundException)
                {
                    Console.WriteLine("The file cannot be found!");
                }
                else
                {
                    Console.WriteLine("Corrupt options file!");
                }

                parsed = false;
            }

            // Load the level from the data if it was parsed
            if (parsed)
            {
                data.ToOptions(out rotationSensitivity, out inputs);

                return true;
            }
            else
            {
                // Save and return default settings
                OptionData o = new OptionData();
                o.RotationSensitivity = 0.01f;
                o.Forward = Key.W;
                o.Back = Key.S;
                o.Left = Key.A;
                o.Right = Key.D;
                o.ZoomIn = Key.Q;
                o.ZoomOut = Key.E;
                Save("/Resources/Settings/settings.xml", o);
                Load("Resources/Settings/settings.xml", out rotationSensitivity, out inputs);
                return false;
            }
        }

        /// <summary>
        /// Saves options to a options file.
        /// </summary>
        /// <param name="path">The filepath of the options to save to.</param>
        /// <param name="options">The option settings to save.</param>
        public static void Save(string path, OptionData options)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(OptionData));
            try
            {
                using (StreamWriter sw = new StreamWriter(Directory.GetCurrentDirectory() + path))
                {
                    serializer.Serialize(sw, options);
                }
            }
            catch (DirectoryNotFoundException)
            {
                Directory.CreateDirectory(Directory.GetCurrentDirectory() + path.Substring(0, path.LastIndexOf('/')));
                Save(path, options);
            }
        }
    }
}
