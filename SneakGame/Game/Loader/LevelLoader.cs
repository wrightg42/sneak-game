﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using Game.Items;
using Game.Items.Enemies;
using Game.Items.Pickups;
using Game.Items.Walls;
using Game.Loader.Intermediary;

namespace Game.Loader
{
    /// <summary>
    /// A static class used to load levels in the game.
    /// </summary>
    public static class LevelLoader
    {
        /// <summary>
        /// Loads a level form a level file.
        /// </summary>
        /// <param name="path">The filepath of the level.</param>
        /// <param name="player">The player to be used in the game.</param>
        /// <param name="walls">The walls to be used in the game.</param>
        /// <param name="pickups">The pickups to be used in the game.</param>
        /// <param name="enemies">The enemies to be used in the game.</param>
        /// <returns>Whether the level was loaded successfully.</returns>
        public static bool Load(string path, out Player player, out IEnumerable<Wall> walls, out IEnumerable<Pickup> pickups, out IEnumerable<Enemy> enemies)
        {
            // Read and desearialize the xml data
            XmlSerializer serializer = new XmlSerializer(typeof(LevelData));
            LevelData data = new LevelData();
            bool parsed = true;
            try
            {
                using (StreamReader sr = File.OpenText(path))
                {
                    data = (LevelData)serializer.Deserialize(sr);
                }
            }
            catch (Exception e)
            {
                // Output accurate error message
                if (e is DirectoryNotFoundException || e is FileNotFoundException)
                {
                    Console.WriteLine("The file cannot be found!");
                }
                else
                {
                    Console.WriteLine("Corrupt level file!");
                }

                parsed = false;
            }

            // Load the level from the data if it was parsed
            if (parsed)
            {
                data.ToLevel(out player, out walls, out pickups, out enemies);

                return true;
            }
            else
            {
                player = new Player();
                walls = new List<Wall>();
                pickups = new List<Pickup>();
                enemies = new List<Enemy>();
                return false;
            }
        }

        /// <summary>
        /// Loads a level form a level file.
        /// </summary>
        /// <param name="path">The filepath of the level.</param>
        /// <param name="player">The player to be used in the game.</param>
        /// <param name="walls">The walls to be used in the game.</param>
        /// <param name="pickups">The pickups to be used in the game.</param>
        /// <param name="enemies">The enemies to be used in the game.</param>
        /// <returns>Whether the level was loaded successfully.</returns>
        public static bool Load(string path, out Player player, out List<Wall> walls, out List<Pickup> pickups, out List<Enemy> enemies)
        {
            // Load with enumerables
            IEnumerable<Wall> enumWalls;
            IEnumerable<Pickup> enumPickups;
            IEnumerable<Enemy> enumEnemies;
            bool ret = Load(path, out player, out enumWalls, out enumPickups, out enumEnemies);

            // Copy data
            walls = enumWalls.ToList();
            pickups = enumPickups.ToList();
            enemies = enumEnemies.ToList();

            return ret;
        }

        /// <summary>
        /// Determines whehter a level can be loaded.
        /// </summary>
        /// <param name="path">The filepath of the level.</param>
        /// <returns>A string with the error message generated when loading the file.</returns>
        /// <remarks>If an empty string is returned the level can be loaded.</remarks>
        public static string CanLoad(string path)
        {
            // The error to return
            string error = string.Empty;

            // Read and desearialize the xml data
            XmlSerializer serializer = new XmlSerializer(typeof(LevelData));
            try
            {
                using (StreamReader sr = File.OpenText(path))
                {
                    object o = serializer.Deserialize(sr);
                }
            }
            catch (Exception e)
            {
                // Output error messages
                if (e is DirectoryNotFoundException || e is FileNotFoundException)
                {
                    error = "The file cannot be found";
                }
                else
                {
                    error = "Corrupt level file!";
                }
            }

            return error;
        }
    }
}
