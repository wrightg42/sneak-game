﻿using System.Collections.Generic;
using OpenTK.Input;

namespace Game.Loader.Intermediary
{
    /// <summary>
    /// A data structure to represent options loaded from an XML file.
    /// </summary>
    public class OptionData
    {
        /// <summary>
        /// The rotation sensitivity of the player.
        /// </summary>
        public float RotationSensitivity;

        /// <summary>
        /// The key that represents moving forward with the player.
        /// </summary>
        public Key Forward;

        /// <summary>
        /// The key that represents moving back with the player.
        /// </summary>
        public Key Back;

        /// <summary>
        /// The key that represents moving left with the player.
        /// </summary>
        public Key Left;

        /// <summary>
        /// The key that represents moving right with the player.
        /// </summary>
        public Key Right;

        /// <summary>
        /// The key that represents zooming in with the camera.
        /// </summary>
        public Key ZoomIn;

        /// <summary>
        /// The key that represents zooming out with the camera.
        /// </summary>
        public Key ZoomOut;

        /// <summary>
        /// Converts the current <see cref="OptionData"/> into usable controls.
        /// </summary>
        /// <param name="rotationSensitivity">The rotation sensitivity of the player.</param>
        /// <param name="inputs">The inputs to be used in the game.</param>
        public void ToOptions(out float rotationSensitivity, out Dictionary<Inputs, Key> inputs)
        {
            rotationSensitivity = RotationSensitivity;

            inputs = new Dictionary<Inputs, Key>();
            inputs.Add(Inputs.Forward, Forward);
            inputs.Add(Inputs.Back, Back);
            inputs.Add(Inputs.Left, Left);
            inputs.Add(Inputs.Right, Right);
            inputs.Add(Inputs.ZoomIn, ZoomIn);
            inputs.Add(Inputs.ZoomOut, ZoomOut);
        }
    }
}
