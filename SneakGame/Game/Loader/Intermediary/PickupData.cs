﻿using OpenTK;
using Game.Items.Pickups;

namespace Game.Loader.Intermediary
{
    /// <summary>
    /// An intermediary data structure to generate an pickup point in a level.
    /// </summary>
    public class PickupData
    {
        /// <summary>
        /// The position of the exit.
        /// </summary>
        public Vector2 Position;

        /// <summary>
        /// Exports the data structure to a usable exit object.
        /// </summary>
        /// <returns>The exit represeted by the data set provided.</returns>
        public Exit ToExit()
        {
            return new Exit(Position);
        }
    }
}
