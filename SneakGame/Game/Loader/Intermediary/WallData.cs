﻿using OpenTK;
using Game.Items.Walls;

namespace Game.Loader.Intermediary
{
    /// <summary>
    /// An intermediary data structure to generate a wall in a level.
    /// </summary>
    public class WallData
    {
        /// <summary>
        /// The position of the wall.
        /// </summary>
        public Vector2 Position;

        /// <summary>
        /// The orientation of the wall.
        /// </summary>
        public float Orientation;

        /// <summary>
        /// The length of the wall.
        /// </summary>
        public float Length;

        /// <summary>
        /// Exports the data structure to a usable wall object.
        /// </summary>
        /// <returns>The wall represeted by the data set provided.</returns>
        public Wall ToWall()
        {
            return new Wall(Position, Orientation, Length);
        }
    }
}
