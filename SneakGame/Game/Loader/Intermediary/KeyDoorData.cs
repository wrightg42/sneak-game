﻿using System.Linq;
using OpenTK;
using Game.Items.Walls;
using Game.Items.Pickups;

namespace Game.Loader.Intermediary
{
    /// <summary>
    /// An intermediary data structure to generate a key and door pair in a level.
    /// </summary>
    public class KeyDoorData
    {
        /// <summary>
        /// The colour of the key and door pair.
        /// </summary>
        public Vector3 Color;

        /// <summary>
        /// The key data.
        /// </summary>
        public PickupData[] Keys = new PickupData[0];

        /// <summary>
        /// The door data.
        /// </summary>
        public WallData Door = new WallData();

        /// <summary>
        /// Exports the data structure to a usable key object.
        /// </summary>
        /// <returns>The keys represeted by the data set provided.</returns>
        public Key[] ToKeys()
        {
            return Keys.Select(k => new Key(k.Position, Color)).ToArray();
        }

        /// <summary>
        /// Exports the data structure to a usable door object.
        /// </summary>
        /// <returns>The door represeted by the data set provided.</returns>
        public Door ToDoor()
        {
            return new Door(Door.Position, Door.Orientation, Door.Length, Color);
        }
    }
}
