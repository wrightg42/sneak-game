﻿using OpenTK;
using Game.Items.Enemies;

namespace Game.Loader.Intermediary
{
    /// <summary>
    /// An intermediary data structure to generate a camera in a level.
    /// </summary>
    public class CameraData
    {
        /// <summary>
        /// The position of the camera.
        /// </summary>
        public Vector2 Position;

        /// <summary>
        /// The maximum turning angle of the camera.
        /// </summary>
        public float MaxTurn;

        /// <summary>
        /// The neutral turn angle of the camera.
        /// </summary>
        public float NeutralTurn;

        /// <summary>
        /// The viewing range of the camera.
        /// </summary>
        /// <remarks>A negative value means use the default value.</remarks>
        public float ViewRange = -1;

        /// <summary>
        /// The viewing angle of the camera.
        /// </summary>
        /// <remarks>A negative value means use the default value.</remarks>
        public float ViewAngle = -1;

        /// <summary>
        /// The turning speed of the camera.
        /// </summary>
        /// <remarks>A negative value means use the default value.</remarks>
        public float TurnSpeed = -1;

        /// <summary>
        /// Exports the data structure to a usable camera object.
        /// </summary>
        /// <returns>The camera represeted by the data set provided.</returns>
        public Camera ToCamera()
        {
            if (ViewRange < 0)
            {
                ViewRange = ViewingEnemy.DefaultViewRange;
            }

            if (ViewAngle < 0)
            {
                ViewAngle = ViewingEnemy.DefaultViewAngle;
            }

            if (TurnSpeed < 0)
            {
                TurnSpeed = ViewingEnemy.DefaultTurnSpeed;
            }

            return new Camera(Position, MaxTurn, NeutralTurn, ViewRange, ViewAngle, TurnSpeed);
        }
    }
}
