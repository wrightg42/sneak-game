﻿namespace Game.Loader.Intermediary
{
    /// <summary>
    /// An enumerator of the different inputs used in the game.
    /// </summary>
    public enum Inputs
    {
        Forward = 0,
        Back = 1,
        Left = 2,
        Right = 3,
        ZoomIn = 4,
        ZoomOut = 5
    }
}
