﻿using OpenTK;
using Game.Items.Enemies;

namespace Game.Loader.Intermediary
{
    /// <summary>
    /// An intermediary data structure to generate a robot in a level.
    /// </summary>
    public class RobotData
    {
        /// <summary>
        /// The initial position of the player.
        /// </summary>
        public Vector2 Position;

        /// <summary>
        /// The initial orientation of the player.
        /// </summary>
        public float BeginOrientation;

        /// <summary>
        /// The path the robot should take.
        /// </summary>
        /// <remarks>Leave blank, i.e. as an empty string, to use the list of verticies that represent a tour.</remarks>
        public string Path = string.Empty;

        /// <summary>
        /// The points to be visited by the robot in it's tour.
        /// </summary>
        /// <remarks>The path as a string is harder to read but more functional, so it takes priority.
        /// If a string path exists this will be used over coordinates.</remarks>
        public Vector2[] PathPoints = new Vector2[0];

        /// <summary>
        /// The viewing range of the robot.
        /// </summary>
        /// <remarks>A negative value means use the default value.</remarks>
        public float ViewRange = -1;

        /// <summary>
        /// The viewing angle of the robot.
        /// </summary>
        /// <remarks>A negative value means use the default value.</remarks>
        public float ViewAngle = -1;

        /// <summary>
        /// The moveing speed of the robot.
        /// </summary>
        /// <remarks>A negative value means use the default value.</remarks>
        public float MoveSpeed = -1;

        /// <summary>
        /// The turning speed of the robot.
        /// </summary>
        /// <remarks>A negative value means use the default value.</remarks>
        public float TurnSpeed = -1;

        /// <summary>
        /// Exports the data structure to a usable robot object.
        /// </summary>
        /// <returns>The robot represeted by the data set provided.</returns>
        public Robot ToRobot()
        {
            if (ViewRange < 0)
            {
                ViewRange = ViewingEnemy.DefaultViewRange;
            }

            if (ViewAngle < 0)
            {
                ViewAngle = ViewingEnemy.DefaultViewAngle;
            }

            if (MoveSpeed < 0)
            {
                MoveSpeed = Robot.DefaultMoveSpeed;
            }

            if (TurnSpeed < 0)
            {
                TurnSpeed = ViewingEnemy.DefaultTurnSpeed;
            }

            // Use string path over the path points represented as vectors
            if (Path != string.Empty)
            {
                return new Robot(Position, BeginOrientation, Path, ViewRange, ViewAngle, MoveSpeed, TurnSpeed);
            }
            else
            {
                return new Robot(Position, BeginOrientation, PathPoints, ViewRange, ViewAngle, MoveSpeed, TurnSpeed);
            }
        }
    }
}
