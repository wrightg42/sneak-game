﻿using OpenTK;
using Game.Items;

namespace Game.Loader.Intermediary
{
    /// <summary>
    /// An intermediary data structure to generate a player in a level.
    /// </summary>
    public class PlayerData
    {
        /// <summary>
        /// The initial position of the player.
        /// </summary>
        public Vector2 Position;

        /// <summary>
        /// The initial orientation of the player.
        /// </summary>
        public float Orientation;

        /// <summary>
        /// Exports the data structure to a usable player object.
        /// </summary>
        /// <returns>The player represeted by the data set provided.</returns>
        public Player ToPlayer()
        {
            return new Player(Position, Orientation);
        }
    }
}
