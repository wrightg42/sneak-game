﻿using System.Collections.Generic;
using Game.Items;
using Game.Items.Enemies;
using Game.Items.Pickups;
using Game.Items.Walls;

namespace Game.Loader.Intermediary
{
    /// <summary>
    /// A data structure to represent the data loaded from an XML file.
    /// </summary>
    public class LevelData
    {
        /// <summary>
        /// The player data loaded from an XML file.
        /// </summary>
        public PlayerData Player;

        /// <summary>
        /// The wall data loaded from an XML file.
        /// </summary>
        public WallData[] Walls;

        /// <summary>
        /// The exit data loaded from an XML file.
        /// </summary>
        public PickupData Exit;

        /// <summary>
        /// The key and door pair data loaded from an XML file.
        /// </summary>
        public KeyDoorData[] KeyDoorPairs;

        /// <summary>
        /// The tripwires loaded from an XML file.
        /// </summary>
        public TripwireData[] Tripwires;

        /// <summary>
        /// The cameras loaded from an XML file.
        /// </summary>
        public CameraData[] Cameras;

        /// <summary>
        /// The robots loaded from an XML file.
        /// </summary>
        public RobotData[] Robots;

        /// <summary>
        /// Exports the loaded level data to a level.
        /// </summary>
        /// <param name="player">The player to be used in the game.</param>
        /// <param name="walls">The walls to be used in the game.</param>
        /// <param name="pickups">The pickups to be used in the game.</param>
        /// <param name="enemies">The enemies to be used in the game.</param>
        public void ToLevel(out Player player, out IEnumerable<Wall> walls, out IEnumerable<Pickup> pickups, out IEnumerable<Enemy> enemies)
        {
            // Initialize lists
            List<Wall> loadedWalls = new List<Wall>();
            List<Pickup> loadedPickups = new List<Pickup>();
            List<Enemy> loadedEnemies = new List<Enemy>();

            // Load player
            player = Player.ToPlayer();

            // Load walls
            foreach (WallData w in Walls)
            {
                loadedWalls.Add(w.ToWall());
            }

            // Load exit
            loadedPickups.Add(Exit.ToExit());

            // Load key and door pairs
            if (KeyDoorPairs != null)
            {
                foreach (KeyDoorData kd in KeyDoorPairs)
                {
                    loadedWalls.Add(kd.ToDoor());
                    loadedPickups.AddRange(kd.ToKeys());
                }
            }

            // Load tripwires
            if (Tripwires != null)
            {
                foreach (TripwireData tw in Tripwires)
                {
                    loadedEnemies.Add(tw.ToTripwire());
                }
            }

            // Load cameras
            if (Cameras != null)
            {
                foreach (CameraData c in Cameras)
                {
                    loadedEnemies.Add(c.ToCamera());
                }
            }

            // Load robots
            if (Robots != null)
            {
                foreach (RobotData r in Robots)
                {
                    loadedEnemies.Add(r.ToRobot());
                }
            }

            // Store loaded lists in paramiters
            walls = loadedWalls;
            pickups = loadedPickups;
            enemies = loadedEnemies;
        }
    }
}
