﻿using OpenTK;
using Game.Items.Enemies;

namespace Game.Loader.Intermediary
{
    /// <summary>
    /// An intermediary data structure to generate a tripwire in a level.
    /// </summary>
    public class TripwireData
    {
        /// <summary>
        /// The start coordinate of the tripwire.
        /// </summary>
        public Vector2 BeginCoordinate;

        /// <summary>
        /// The end coordinate of the tripwire.
        /// </summary>
        public Vector2 EndCoordinate;

        /// <summary>
        /// The pulse duration of the tripwire.
        /// </summary>
        public float Pulse;

        /// <summary>
        /// Exports the data structure to a usable tripwire object.
        /// </summary>
        /// <returns>The tripwire represeted by the data set provided.</returns>
        public Tripwire ToTripwire()
        {
            return new Tripwire(BeginCoordinate, EndCoordinate, Pulse);
        }
    }
}
