﻿using System;
using Game.Loader;

namespace Game
{
    /// <summary>
    /// An application entry point.
    /// </summary>
    public class Program
    {
        /// <summary>
        /// The main entry point for the program.
        /// </summary>
        /// <param name="args">Any arguments the program is run with.</param>
        public static void Main(string[] args)
        {
            // Get level name
            Console.Write("Enter level name: ");
            string levelName = Console.ReadLine();

            using (Game g = new Game("Sneak Game", 1024, 768, string.Format("Resources/Levels/{0}.xml", levelName), "Resources/Settings/settings.xml", true))
            {
                // Check game is supported by system
                if (g.Supported)
                {
                    // Check level can be loaded
                    string loadingError = LevelLoader.CanLoad(g.LevelFilepath);
                    if (loadingError == string.Empty)
                    {
                        g.Run();
                    }
                    else
                    {
                        Console.WriteLine(loadingError);
                        Console.ReadKey(true);
                        Environment.Exit(0);
                    }
                }
                else
                {
                    Console.Write("OpenGL version is not up to date enough to support this application.\nTry updating your graphics card.\nPress any key to continue...");
                    Console.ReadKey(true);
                }
            }
        }
    }
}