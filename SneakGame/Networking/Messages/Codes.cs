﻿namespace Networking.Messages
{
    /// <summary>
    /// An enumerator of the different codes availible to use.
    /// </summary>
    public enum Codes
    {
        Username = 0,
        LevelName = 1,
        MapPlayer = 2,
        LevelData = 3,
        Message = 4,
        Movement = 5,
        Turn = 6,
        Position = 7,
        Lost = 8, 
        Won = 9
    }
}
