﻿namespace Networking.Messages
{
    /// <summary>
    /// An enumerator of the different movement codes avalible.
    /// </summary>
    public enum MovementCodes
    {
        None = 0,
        Left = 1,
        Right = 2,
        Forward = 4,
        Back = 8
    }
}
