﻿using System;
using System.Text;

namespace Networking.Messages
{
    /// <summary>
    /// A class representing a message to be sent.
    /// </summary>
    public class Message
    {
        /// <summary>
        /// Initalizes a new instance of the <see cref="Message"/> class.
        /// </summary>
        /// <param name="code">The code identifier for the message.</param>
        public Message(int code)
        {
            Code = code;
            Data = new byte[0];
        }

        /// <summary>
        /// Initalizes a new instance of the <see cref="Message"/> class.
        /// </summary>
        /// <param name="code">The code identifier for the message.</param>
        /// <param name="data">The data contained in the message.</param>
        public Message(int code, byte[] data)
        {
            Code = code;
            Data = data;
        }

        /// <summary>
        /// Initalizes a new instance of the <see cref="Message"/> class.
        /// </summary>
        /// <param name="code">The code identifier for the message.</param>
        /// <param name="msg">The string to be sent in the message.</param>
        public Message(int code, string msg)
        {
            Code = code;
            Data = Encoding.UTF8.GetBytes(msg);
        }

        /// <summary>
        /// Gets or sets the code identifying what the message is.
        /// </summary>
        public int Code { get; set; }

        /// <summary>
        /// Gets or sest the data in the message.
        /// </summary>
        public byte[] Data { get; set; }

        /// <summary>
        /// Converts the message to a byte array to send.
        /// </summary>
        /// <param name="tcp">Whether the message is being sent via a TCP socket.</param>
        /// <returns>The sendable byte array.</returns>
        public byte[] ToByteArray(bool tcp = true)
        {
            // Create buffer with extra 8 bytes - used to store an integer for both length and code
            if (tcp)
            {
                // Get the length of the message
                int length = Data.Length;

                byte[] packet = new byte[8 + Data.Length];

                // Copy data
                BitConverter.GetBytes(length).CopyTo(packet, 0);
                BitConverter.GetBytes(Code).CopyTo(packet, 4);
                Data.CopyTo(packet, 8);

                return packet;
            }
            else
            {
                byte[] datagram = new byte[4 + Data.Length];

                // Copy data
                BitConverter.GetBytes(Code).CopyTo(datagram, 0);
                Data.CopyTo(datagram, 4);

                return datagram;
            }
        }
    }
}
