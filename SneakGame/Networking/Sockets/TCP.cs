﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using Networking.Messages;
using Networking.Sockets.Events;

namespace Networking.Sockets
{
    /// <summary>
    /// A class that handles a TCP connection.
    /// </summary>
    public class TCP
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TCP"/> class.
        /// </summary>
        public TCP()
        {
            // Initialize sockets 
            Port = (int)Ports.DefaultPort;
            ListenSocket = new TcpListener(new IPEndPoint(IPAddress.Any, Port));

            // Wait for connections
            Listen();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TCP"/> class.
        /// </summary>
        /// <param name="port">The port to create the listener socket on.</param>
        public TCP(int port)
        {
            // Initialize sockets 
            Port = port;
            ListenSocket = new TcpListener(new IPEndPoint(IPAddress.Any, Port));

            // Wait for connections
            Listen();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TCP"/> class and connects to a remote IP.
        /// </summary>
        /// <param name="ipep">The remote IP to connect to.</param>
        public TCP(IPEndPoint ipep)
        {
            // Initialize sockets 
            Port = (int)Ports.DefaultPort;
            ListenSocket = new TcpListener(new IPEndPoint(IPAddress.Any, Port));

            // Connect
            Connect(ipep);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TCP"/> class and connects to a remote IP.
        /// </summary>
        /// <param name="port">The port to create the listener socket on.</param>
        /// <param name="ipep">The remote IP to connect to.</param>
        public TCP(int port, IPEndPoint ipep)
        {
            // Initialize sockets 
            Port = port;
            ListenSocket = new TcpListener(new IPEndPoint(IPAddress.Any, Port));

            // Connect
            Connect(ipep);
        }

        /// <summary>
        /// Fires when a connection is made.
        /// </summary>
        public event EventHandler<ConnectedEventArgs> Connected;

        /// <summary>
        /// Fires when a message is recevied.
        /// </summary>
        public event EventHandler<DataReceivedEventArgs> MessageReceived;

        /// <summary>
        /// Fires when the remote disconnects.
        /// </summary>
        public event EventHandler<DisconnectedEventArgs> Disconnected;

        /// <summary>
        /// Gets the local end point of the connection.
        /// </summary>
        public IPEndPoint LocalIP
        {
            get
            {
                return ClientSocket.Client.LocalEndPoint as IPEndPoint;
            }
        }

        /// <summary>
        /// Gets the local end point of the listening socket.
        /// </summary>
        public IPEndPoint ListenIP
        {
            get
            {
                return ListenSocket.LocalEndpoint as IPEndPoint;
            }
        }

        /// <summary>
        /// Gets the remote end point of the connection.
        /// </summary>
        public IPEndPoint RemoteIP
        {
            get
            {
                try
                {
                    return ClientSocket.Client.RemoteEndPoint as IPEndPoint;
                }
                catch
                {
                    return new IPEndPoint(IPAddress.Any, 0);
                }
            }
        }

        /// <summary>
        /// Gets whether the socket is still connected.
        /// </summary>
        public bool IsConnected
        {
            get
            {
                return ClientSocket != null && ClientSocket.Connected;
            }
        }

        /// <summary>
        /// Gets or sets the port number for the listener socket.
        /// </summary>
        public virtual int Port { get; set; }

        /// <summary>
        /// Gets or sets the TCP socket to transfer packets with.
        /// </summary>
        private TcpClient ClientSocket { get; set; }

        /// <summary>
        /// Gets or sets the listener socket for incoming TCP connections.
        /// </summary>
        private TcpListener ListenSocket { get; set; }

        /// <summary>
        /// Gets or sets the network stream used to send/receive data.
        /// </summary>
        private NetworkStream NetStream { get; set; }

        /// <summary>
        /// Gets or sets the thread used to handle incomming connections without blocking the main thread.
        /// </summary>
        private Thread ConnectionThread { get; set; }

        /// <summary>
        /// Gets or sets the thread used to receive messages without blocking the main thread.
        /// </summary>
        private Thread ReceiveThread { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether socket connections are being accepted on the listen thread.
        /// </summary>
        private bool AcceptingConnections { get; set; }

        /// <summary>
        /// Closes a <see cref="TCP"/> object.
        /// </summary>
        public void Close()
        {
            Disconnect();
            StopListen();
            ListenSocket.Stop();
            ConnectionThread.Abort();
        }

        /// <summary>
        /// Connects the socket to a remote IP.
        /// </summary>
        /// <param name="ipep">The remote IP to connect to.</param>
        public void Connect(IPEndPoint ipep)
        {
            if (!IsConnected)
            {
                try
                {
                    ClientSocket = new TcpClient(new IPEndPoint(IPAddress.Any, 0));
                    ClientSocket.SendTimeout = 2000;
                    ClientSocket.Connect(ipep);
                    NetStream = ClientSocket.GetStream();
                    BeginReceiving();
                    OnConnected(new ConnectedEventArgs(RemoteIP.Address));
                }
                catch (Exception e)
                {
                    // Prevent connection if error occurs, i.e. refusing connection
                    Console.WriteLine(e.Message);
                }
            }
        }

        /// <summary>
        /// Listens for incomming connection requests.
        /// </summary>
        public void Listen()
        {
            AcceptingConnections = true;
            if (ConnectionThread == null || !ConnectionThread.IsAlive)
            {
                // Only start listen thread if it is not active
                ConnectionThread = new Thread(() => WaitForConnection());
                ConnectionThread.Start();
            }
        }

        /// <summary>
        /// Stops listening for incomming connection requests.
        /// </summary>
        public void StopListen()
        {
            AcceptingConnections = false;
        }

        /// <summary>
        /// Disconnects the ongoing connection.
        /// </summary>
        public void Disconnect()
        {
            // Only disconnect if already connected
            if (IsConnected)
            {
                OnDisconnected(new DisconnectedEventArgs(RemoteIP.Address));
            }
        }

        /// <summary>
        /// Sends a message to the remote.
        /// </summary>
        /// <param name="msg">The message to be sent.</param>
        public void Send(Message msg)
        {
            try
            {
                byte[] data = msg.ToByteArray();
                NetStream.Write(data, 0, data.Length);
            }
            catch
            {
                // Disconnect on errors
                Disconnect();
            }
        }

        /// <summary>
        /// Parses a received message.
        /// </summary>
        /// <param name="msg">The message received.</param>
        protected void ParseMessage(Message msg)
        {
            Console.WriteLine(msg.Data);
        }

        /// <summary>
        /// Manages and fires the <see cref="Connected"/> event.
        /// </summary>
        /// <param name="e">The event arguments to fire with.</param>
        private void OnConnected(ConnectedEventArgs e)
        {
            // Fire event
            Connected?.Invoke(this, e);
        }

        /// <summary>
        /// Manages and fires the <see cref="MessageReceived"/> event.
        /// </summary>
        /// <param name="e">The event arguments to fire with.</param>
        private void OnMessageRecevied(DataReceivedEventArgs e)
        {
            // Fire event
            MessageReceived?.Invoke(this, e);
        }

        /// <summary>
        /// Manages and fires the <see cref="Disconnected"/> event.
        /// </summary>
        /// <param name="e">The event arguments to fire with.</param>
        private void OnDisconnected(DisconnectedEventArgs e)
        {
            // Disconnect socket (incase event has blocking code)
            if (ClientSocket != null)
            {
                ClientSocket.Close();
                ClientSocket = null;
                NetStream.Close();
            }

            // Fire event
            Disconnected?.Invoke(this, e);

            // Start listening for connections again
            ReceiveThread.Abort();
            Listen();
        }

        /// <summary>
        /// Waits for a connection to bind to the socket.
        /// </summary>
        private void WaitForConnection()
        {
            // Start listening
            ListenSocket.Start();

            while (AcceptingConnections && !IsConnected)
            {
                // Block until connection created
                try
                {
                    ClientSocket = ListenSocket.AcceptTcpClient();
                }
                catch
                {
                    continue;
                }

                // Create netstream if accepting connections
                if (AcceptingConnections)
                {
                    NetStream = ClientSocket.GetStream();
                    BeginReceiving();
                    OnConnected(new ConnectedEventArgs(RemoteIP.Address));
                    ListenSocket.Stop();
                }
                else
                {
                    Disconnect();
                    AcceptingConnections = false;
                }
            }
        }

        /// <summary>
        /// Begins receiving data after a connection has been created.
        /// </summary>
        private void BeginReceiving()
        {
            ReceiveThread = new Thread(() => Receive());
            ReceiveThread.Start();
        }

        /// <summary>
        /// Receives messages from the remote.
        /// </summary>
        private void Receive()
        {
            while (true)
            {
                int code = 0;
                byte[] message;

                // Receive a payload
                try
                {
                    int length = ReceiveInt();  // Get payload length
                    code = ReceiveInt();        // Get message code
                    message = ReceiveData(length);    // Get payload
                }
                catch
                {
                    // Close when network errors occur
                    break;
                }

                // Check if disconnect was sent
                if (message.Length == 0)
                {
                    break;
                }

                // Fire message received event 
                OnMessageRecevied(new DataReceivedEventArgs(new Message(code, message), RemoteIP.Address));
            }

            // Error occured or disconnected, disconnect socket
            OnDisconnected(new DisconnectedEventArgs(RemoteIP.Address));
        }

        /// <summary>
        /// Receives an incomming integer.
        /// </summary>
        /// <returns>The integer received.</returns>
        private int ReceiveInt()
        {
            // Recieve the data
            byte[] data = ReceiveData(4);
            
            // Convert data to integer
            return BitConverter.ToInt32(data, 0);
        }

        /// <summary>
        /// Receives incomming data.
        /// </summary>
        /// <param name="size">The size of the incomming data.</param>
        /// <returns>The data received.</returns>
        private byte[] ReceiveData(int size)
        {
            // Assign the payload buffer array
            byte[] data = new byte[size];

            // Create message income tracking variables.
            int totalRead = 0;      // How much data has been received
            int currentRead = 0;    // How much data was read in last receive

            // Receive the payload
            do
            {
                // Receive remaining data from stream
                currentRead = NetStream.Read(data, totalRead, data.Length - totalRead);

                // Update total read data
                totalRead += currentRead;
            }
            while (totalRead < data.Length && currentRead > 0); // Run until all data received, and still data incomming

            return data;
        }
    }
}
