﻿namespace Networking.Sockets
{
    /// <summary>
    /// An enumerator of the default ports to use in the game.
    /// </summary>
    public enum Ports
    {
        DefaultPort = 27763,
        ConnectionPort = 27765,
        ChatPort = 27767,
        GamePort = 27769
    }
}
