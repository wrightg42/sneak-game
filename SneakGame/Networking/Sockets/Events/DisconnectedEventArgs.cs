﻿using System;
using System.Net;

namespace Networking.Sockets.Events
{
    /// <summary>
    /// Contains event arguments for when the remote disconnects.
    /// </summary>
    public class DisconnectedEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DisconnectedEventArgs"/> class.
        /// </summary>
        /// <param name="remote">The remote IP address which became disconnected.</param>
        public DisconnectedEventArgs(IPAddress remote)
        {
            RemoteIP = remote;
        }

        /// <summary>
        /// Gets or sets the remote IP.
        /// </summary>
        public IPAddress RemoteIP { get; set; }
    }
}
