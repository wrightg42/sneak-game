﻿using System;
using System.Net;

namespace Networking.Sockets.Events
{
    /// <summary>
    /// Contains event arguments for when the remote connects.
    /// </summary>
    public class ConnectedEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ConnectedEventArgs"/> class.
        /// </summary>
        /// <param name="remote">The remote IP address which became connected.</param>
        public ConnectedEventArgs(IPAddress remote)
        {
            RemoteIP = remote;
        }

        /// <summary>
        /// Gets or sets the remote IP.
        /// </summary>
        public IPAddress RemoteIP { get; set; }
    }
}
