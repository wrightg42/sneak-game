﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using Networking.Sockets;
using Networking.Messages;
using Networking.Sockets.Events;

namespace Networking
{
    class Program
    {
        static TCP c;

        static void Main(string[] args)
        {
            c = new TCP();
            c.Connected += SendMessages;
            c.MessageReceived += RecevMsg;
            c.Disconnected += Discon;

            Console.WriteLine(c.ListenIP);

            string msg = Console.ReadLine();
            if (!c.IsConnected)
            {
                string ip = msg.Split(':')[0];
                string port = msg.Split(':')[1];
                c.Connect(new IPEndPoint(IPAddress.Parse(ip), int.Parse(port)));
            }
            else
            {
                c.Send(new Message(0, msg));
            }
        }

        static void SendMessages(object sender, ConnectedEventArgs e)
        {
            Console.WriteLine("Connected!");
            while (c.IsConnected)
            {
                Console.Write(">>> ");
                string m = Console.ReadLine();
                if (m == "d")
                {
                    c.Disconnect();
                }
                else
                {
                    c.Send(new Message(0, m));
                }
            }
        }

        static void RecevMsg(object sender, DataReceivedEventArgs e)
        {
            Console.WriteLine(Encoding.UTF8.GetString(e.Message.Data));
        }

        static void Discon(object sender, DisconnectedEventArgs e)
        {
            Console.WriteLine("Disconnected! Press any key to continue...");
            Console.ReadKey(true);
            Environment.Exit(0);
        }
    }
}
